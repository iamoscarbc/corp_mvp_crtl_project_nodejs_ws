import { Controller, UseFilters } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { TcpExceptionFilter } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { AppTcpService } from "./app.tcp.service";

@Controller()
export class AppTcpController {
  constructor(private readonly appService: AppTcpService) {}

  @UseFilters(new TcpExceptionFilter())
  @MessagePattern({ action: "validate-exist-token", type: "validate" })
  validateExistToken({ token }) {
    return this.appService.validateExistToken(token);
  }

  @UseFilters(new TcpExceptionFilter())
  @MessagePattern({
    action: "generate-token-for-internal-user",
    type: "insert-update",
  })
  generateTokenForInternalUser({
    idInternalUser,
    profileCode,
    roles,
    fullName,
    firstName,
    lastName,
    email,
    isOnline,
  }) {
    return this.appService.generateTokenForInternalUser(
      idInternalUser,
      profileCode,
      roles,
      fullName,
      firstName,
      lastName,
      email,
      isOnline
    );
  }

  @UseFilters(new TcpExceptionFilter())
  @MessagePattern({
    action: "generate-token-for-external-user",
    type: "insert-update",
  })
  generateTokenForExternalUser({
    idExternalUser,
    profile,
    roles,
    fullName,
    firstName,
    lastName,
    email,
    isOnline,
    idEnterprise,
  }) {
    return this.appService.generateTokenForExternalUser(
      idExternalUser,
      profile,
      roles,
      fullName,
      firstName,
      lastName,
      email,
      isOnline,
      idEnterprise
    );
  }

  @UseFilters(new TcpExceptionFilter())
  @MessagePattern({ action: "logout", type: "update" })
  logout({ token }) {
    return this.appService.logout(token);
  }
}
