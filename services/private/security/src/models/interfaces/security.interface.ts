import { Document, ObjectId } from 'mongoose'

declare global {
    export interface Security extends Document {
        idUser: ObjectId
        token: string
        createdAt: Date
        updatedAt: Date
    }
}