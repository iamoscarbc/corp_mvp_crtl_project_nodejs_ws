import { ObjectId } from 'mongodb';
import * as mongoose from 'mongoose';
import * as moment from 'moment';

export const SecuritySchema = new mongoose.Schema(
    {
        idUser: ObjectId,
        token: String,
        createdAt: {
            type: Date,
            default: () => moment.utc().clone(),
        },
        updatedAt: {
            type: Date,
            default: null,
        }
    },
    {
        collection: "Securities"
    }
);