export interface JwtPayload {
  data: string;
}
export interface JwtPayloadInternalUserData {
  idInternalUser: string;
  profileCode: string;
  roles: string[];
  fullName: string;
  firstName: string;
  lastName: string;
  email: string;
  isOnline: boolean;
  isInternal: boolean;
}

export interface JwtPayloadExternalUserData {
  idExternalUser: string;
  profile: any;
  roles: string[];
  fullName: string;
  firstName: string;
  lastName: string;
  email: string;
  isOnline: boolean;
  isInternal: boolean;
  idEnterprise: string;
}
