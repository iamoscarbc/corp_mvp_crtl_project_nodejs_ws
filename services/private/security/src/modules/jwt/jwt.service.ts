/// <reference path="interfaces/jwt-payload.d.ts" />

import { Injectable } from "@nestjs/common";
import { ConfigService } from "@corp_mvp_crtl_project_nodejs_ws/config";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { sign } from "jsonwebtoken";
import {
  JwtPayload,
  JwtPayloadExternalUserData,
  JwtPayloadInternalUserData,
} from "./interfaces/jwt-payload";

@Injectable()
export class JwtService {
  private readonly secretKey = ConfigService.get("jwt.secretKey");
  private readonly signOptions = ConfigService.get<string>("jwt.signOptions");

  constructor(private readonly loggerService: LoggerService) {}

  createTokenForInternalUser(payload: JwtPayloadInternalUserData) {
    const result = sign(
      <JwtPayload>{
        data: Buffer.from(JSON.stringify(payload)).toString("base64"),
      },
      this.secretKey,
      {
        expiresIn: this.signOptions,
      }
    );
    return result;
  }

  createTokenForExternalUser(payload: JwtPayloadExternalUserData) {
    const result = sign(
      <JwtPayload>{
        data: Buffer.from(JSON.stringify(payload)).toString("base64"),
      },
      this.secretKey,
      {
        expiresIn: this.signOptions,
      }
    );
    return result;
  }
}
