import { Module } from '@nestjs/common';
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { AppTcpController } from './app.tcp.controller';
import { AppTcpService } from './app.tcp.service';
import { CONST_MODULE } from './app.constants';
import { SecuritySchema } from './models/schemas/security.schema';
import { JwtModule } from './modules/jwt/jwt.module';
import { LoggerModule } from '@corp_mvp_crtl_project_nodejs_ws/logger';

@Module({
    imports: [
        LoggerModule,
        JwtModule,
        MongoDbModule,
        MongoDbModule.forFeature([{ name: CONST_MODULE.SECURITY, schema: SecuritySchema }])
    ],
    controllers: [AppTcpController],
    providers: [AppTcpService]
})
export class AppModule { }