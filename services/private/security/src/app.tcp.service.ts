/// <reference path="./models/interfaces/security.interface.ts" />

import { Injectable, ConflictException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { from, Observable, of } from "rxjs";
import { tap, concatMap, map } from "rxjs/operators";
import { Model } from "mongoose";
import { CONST_MODULE } from "./app.constants";
import { JwtService } from "./modules/jwt/jwt.service";
import { ObjectId } from "mongodb";
import * as moment from "moment";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";

@Injectable()
export class AppTcpService {
  constructor(
    @InjectModel(CONST_MODULE.SECURITY)
    private readonly securityModel: Model<Security>,
    private readonly jwtService: JwtService,
    private readonly loggerService: LoggerService
  ) {}

  validateExistToken(token: string): Observable<boolean> {
    return from(this.securityModel.findOne({ token: token })).pipe(
      map((security) => {
        return !security;
      })
    );
  }

  generateTokenForInternalUser(
    idInternalUser: string,
    profileCode: string,
    roles: any,
    fullName: string,
    firstName: string,
    lastName: string,
    email: string,
    isOnline: boolean
  ) {
    const onlyRolesCode = (roles as any[]).map((x) => String(x.code));
    const token = this.jwtService.createTokenForInternalUser({
      idInternalUser,
      profileCode,
      roles: onlyRolesCode,
      fullName,
      firstName,
      lastName,
      email,
      isOnline,
      isInternal: true,
    });
    return from(this.createOrUpdateToken(idInternalUser, token)).pipe(
      map(() => {
        return { token };
      })
    );
  }

  generateTokenForExternalUser(
    idExternalUser: string,
    profile: any,
    roles: any,
    fullName: string,
    firstName: string,
    lastName: string,
    email: string,
    isOnline: boolean,
    idEnterprise: string
  ) {
    const onlyRolesCode = (roles as any[]).map((x) => String(x.code));
    const token = this.jwtService.createTokenForExternalUser({
      idExternalUser,
      profile,
      roles: onlyRolesCode,
      fullName,
      firstName,
      lastName,
      email,
      isOnline,
      isInternal: false,
      idEnterprise,
    });
    return from(this.createOrUpdateToken(idExternalUser, token)).pipe(
      map(() => {
        return { token };
      })
    );
  }

  logout(token: string) {
    return from(
      this.securityModel.findOneAndUpdate(
        { token: token },
        { $set: { token: null } }
      )
    );
  }

  private createOrUpdateToken(idUser: any, token: string) {
    return from(
      this.securityModel.findOne({ idUser: idUser }, { _id: 1 })
    ).pipe(
      concatMap((security) => {
        if (!security) return this.createToken(idUser, token);
        return this.updateToken(security._id, token);
      })
    );
  }

  private createToken(idUser: string, token: string) {
    let security = {
      idUser: new ObjectId(idUser),
      token: token,
    };
    return from(new this.securityModel(security).save());
  }

  private updateToken(id: any, token: string) {
    return from(
      this.securityModel.findByIdAndUpdate(
        {
          _id: id,
        },
        {
          $set: {
            token: token,
            updatedAt: moment.utc().toDate(),
          },
        }
      )
    );
  }
}
