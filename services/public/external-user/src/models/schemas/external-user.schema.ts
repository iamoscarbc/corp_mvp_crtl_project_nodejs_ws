import { ObjectId } from "mongodb";
import * as mongoose from "mongoose";
import * as moment from "moment";

export const ExternalUserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      unique: true,
      index: true,
    },
    enterprise: {
      idEnterprise: ObjectId,
      businessName: String,
      ruc: String,
      isSuscribe: Boolean,
    },
    currentProjects: [
      {
        idProject: ObjectId,
        name: String,
        status: {
          value: Number,
          description: String,
        },
      },
    ],
    historyProjects: [
      {
        idProject: ObjectId,
        name: String,
      },
    ],
    profile: {
      idProfile: ObjectId,
      name: String,
      code: String,
    },
    roles: [
      {
        idRole: ObjectId,
        code: String,
      },
    ],
    password: String,
    fullName: String,
    firstName: String,
    lastName: String,
    document: {
      code: String,
      value: {
        type: String,
        unique: true,
        index: true,
      },
    },
    cost: Number,
    active: {
      type: Boolean,
      default: true,
    },
    isSubscribe: {
      type: Boolean,
      default: true,
    },
    isOnline: {
      type: Boolean,
      default: () => false,
      index: true,
    },
    createdAt: {
      type: Date,
      default: () => moment.utc().clone(),
    },
    updatedAt: {
      type: Date,
      default: null,
    },
    isInternal: {
      type: Boolean,
      default: () => true,
      index: true,
    },
  },
  {
    collection: "ExternalUsers",
  }
);

ExternalUserSchema.pre("save", function (this: any, next) {
  this.fullName = `${this.firstName} ${this.lastName}`;
  next();
});
