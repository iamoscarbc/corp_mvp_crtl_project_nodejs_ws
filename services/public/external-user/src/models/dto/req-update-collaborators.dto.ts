import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "@corp_mvp_crtl_project_nodejs_ws/setup";
export class RequestUpdateCollaboratorDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idExternalUser: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  cost: number;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idProfile: string;
}
