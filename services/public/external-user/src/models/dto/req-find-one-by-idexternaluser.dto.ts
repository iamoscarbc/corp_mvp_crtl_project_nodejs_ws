import { ApiModelProperty } from "@nestjs/swagger";
import { IsString } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsNotEmpty } from "class-validator";

export class RequestFindOneByIdExternalUserDto { 
    
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    idExternalUser: string
}