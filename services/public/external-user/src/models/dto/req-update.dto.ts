import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsEnum } from "class-validator";
import {
  CONST_PROFILE_ADMINISTRATOR,
  CONST_PROFILE_COLLABORATORS,
  CONST_PROFILE_HUMAN_RESOURCES,
  CONST_PROFILE_PROJECT_MANAGER,
} from "../../app.constants";

export class RequestUpdateDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idExternalUser: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  @IsEnum([
    CONST_PROFILE_ADMINISTRATOR.ID,
    CONST_PROFILE_COLLABORATORS.ID,
    CONST_PROFILE_HUMAN_RESOURCES.ID,
    CONST_PROFILE_PROJECT_MANAGER.ID,
  ])
  @ApiModelProperty()
  idProfile: string;
}
