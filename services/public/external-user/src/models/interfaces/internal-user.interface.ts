import { ObjectId } from 'mongodb';
import { Document } from 'mongoose';

declare global {
    export interface InternalUser extends Document {
        idInternalUser: ObjectId
        email: string
        profile: {
            idProfile: ObjectId
            name: string
            code: string
        }
        roles: [{
            idRole: ObjectId
            code: string
        }],
        fullName: string
        firstName: string
        lastName: string
        document: {
            code: string
            value: string
        }
        active: boolean
        isSubscribe: boolean
        password: string
        createdAt: Date
        updatedAt: Date
        isOnline: boolean
    }
}