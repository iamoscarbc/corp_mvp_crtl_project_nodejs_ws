import { ObjectId } from "mongodb";
import { Document } from "mongoose";

declare global {
  export interface Enterprise extends Document {
    idEnterprise: ObjectId;
    ruc: string;
    businessName: string;

    estado_del_contribuyente: string;
    condicion_de_domicilio: string;
    ubigeo: string;
    tipo_de_via: string;
    nombre_de_via: string;
    codigo_de_zona: string;
    tipo_de_zona: string;
    numero: string;
    interior: string;
    lote: string;
    dpto: string;
    manzana: string;
    kilometro: string;
    departamento: string;
    provincia: string;
    distrito: string;
    direccion: string;
    direccion_completa: string;
    ultima_actualizacion: string;
    materialResources: [
      {
        idMaterialResource: ObjectId;
        type: string;
        name: string;
        description: string;
        singleCost: number;
        monthCost: number;
        yearCost: number;
        createdAt: Date;
      }
    ];
    humanResources: [
      {
        idHumanResource: ObjectId;
        firstName: string;
        lastName: string;
        email: string;
        createdAt: Date;
      }
    ];
    managers: [
      {
        idManager: ObjectId;
        firstName: string;
        lastName: string;
        email: string;
        createdAt: Date;
      }
    ];
    projects: [
      {
        idProject: ObjectId;
        name: string;
        boss: {
          idBoss: ObjectId;
          fullName: string;
          email: string;
        };
        createdAt: Date;
      }
    ];
    collaborators: [
      {
        idCollaborator: ObjectId;
        firstName: string;
        lastName: string;
        createdAt: Date;
        isOnline: boolean;
      }
    ];
    createdBy: {
      idUserCreator: ObjectId;
      firstName: String;
      lastName: string;
      profileCode: String;
    };
    createdAt: Date;
    updatedAt: Date;
    isSubscribe: boolean;
  }
}
