import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  Get,
  Put,
  UseGuards,
} from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";
import {
  ENUM_Roles,
  Roles,
  RolesGuard,
  SecurityGuard,
  Token,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { AppHttpService } from "./app.http.service";
import { RequestCreateDto } from "./models/dto/req-create.dto";
import { RequestUpdateDto } from "./models/dto/req-update.dto";
import { Param } from "@nestjs/common";
import { RequestFindOneByIdExternalUserDto } from "./models/dto/req-find-one-by-idexternaluser.dto";
import { RequestCreateCollaboratorsDto } from "./models/dto/req-create-collaborators.dto";
import { RequestUpdateCollaboratorDto } from "./models/dto/req-update-collaborators.dto";

@ApiBearerAuth()
@UseGuards(SecurityGuard, RolesGuard)
@ApiUseTags("Responsible service for all external user in project")
@Controller("ExternalUser")
export class AppHttpController {
  constructor(private readonly appService: AppHttpService) {}

  @ApiOperation({ title: "Profiles", description: "Find all profiles" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_PROFILE)
  @Get("profiles")
  profiles() {
    return this.appService.profiles();
  }

  @ApiOperation({ title: "Create", description: "Create all external user" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.CREATE_EXTERNAL_USER)
  @Post("create")
  create(@Body() createDto: RequestCreateDto) {
    return this.appService.create(createDto);
  }

  @ApiOperation({
    title: "Create collaborator",
    description: "Create all collaboratos",
  })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.CREATE_COLABORATOR)
  @Post("create-collaborator")
  createCollaborator(
    @Body() createDto: RequestCreateCollaboratorsDto,
    @Token() token: string
  ) {
    return this.appService.createCollaborator(token, createDto);
  }

  @ApiOperation({ title: "Update", description: "Update all external user" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.UPDATE_EXTERNAL_USER)
  @Put("update")
  update(@Body() updateDto: RequestUpdateDto) {
    return this.appService.update(updateDto);
  }

  @ApiOperation({
    title: "Update collaborator",
    description: "Update all collaborators",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.UPDATE_COLABORATOR)
  @Put("update-collaborator")
  updateCollaborator(@Body() updateDto: RequestUpdateCollaboratorDto) {
    return this.appService.updateCollaborator(updateDto);
  }

  @ApiOperation({ title: "Find-All", description: "Find all external user" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_EXTERNAL_USER)
  @Get("find-all")
  findAll(@Token() token: string) {
    return this.appService.findAll(token);
  }

  @ApiOperation({
    title: "Find-one-by-idExternalUser",
    description: "Find one external user for idExternalUser",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_EXTERNAL_USER)
  @Get("find-one-by-idExternalUser/:idExternalUser")
  findOneByIdExternalUser(
    @Param() { idExternalUser }: RequestFindOneByIdExternalUserDto
  ) {
    return this.appService.findOneByIdExternalUser(idExternalUser);
  }
}
