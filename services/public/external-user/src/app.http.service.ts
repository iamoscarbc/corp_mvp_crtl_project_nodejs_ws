/// <reference path="./models/interfaces/external-user.interface.ts" />
/// <reference path="./models/interfaces/internal-user.interface.ts" />
/// <reference path="./models/interfaces/enterprise.interface.ts" />

import { Injectable, ConflictException, HttpException } from "@nestjs/common";
import { RequestCreateDto } from "./models/dto/req-create.dto";
import { from, throwError, Observable, of, forkJoin } from "rxjs";
import { concatMap, catchError, tap, map, toArray, filter } from "rxjs/operators";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import {
  CONST_MODULE,
  CONST_PROFILE,
  CONST_PROFILE_ADMINISTRATOR,
  CONST_PROFILE_COLLABORATORS,
  CONST_PROFILE_HUMAN_RESOURCES,
  CONST_PROFILE_PROJECT_MANAGER,
  CONST_ROLE,
} from "./app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import moment = require("moment");
import { RequestUpdateDto } from "./models/dto/req-update.dto";
import { ObjectId } from "mongodb";
import { RequestCreateCollaboratorsDto } from "./models/dto/req-create-collaborators.dto";
import { RequestUpdateCollaboratorDto } from "./models/dto/req-update-collaborators.dto";
import {
  decodeTokenExternalUser,
  decodeTokenInternalUser,
} from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { decode } from "querystring";

@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.EXTERNAL_USER) private readonly externalUserModel: Model<ExternalUser>,
    @InjectModel(CONST_MODULE.INTERNAL_USER) private readonly internalUserModel: Model<InternalUser>,
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseModel: Model<Enterprise>,
    private readonly loggerService: LoggerService
  ) {}

  profiles() {
    return from(CONST_PROFILE).pipe(
      map((profiles) => {
        return {
          idProfile: profiles.ID_PROFILE,
          name: profiles.NAME,
          code: profiles.CODE,
          isViewAdm: profiles.IS_VIEW_ADM,
        };
      }),
      toArray()
    );
  }

  create(createDto: RequestCreateDto) {
    this.loggerService.log(
      `### init process create with parameter : ${JSON.stringify(createDto)}`
    );

    const validateEmailExternalObservable = from(this.externalUserModel.findOne({ email: createDto.email }));
    const validateEmailInternalObservable = from(this.externalUserModel.findOne({ email: createDto.email }));
    const validateDocumentObservable = from(this.externalUserModel.findOne({"document.value": createDto.documentNumber }));
    const validateEnterpriseObservable = from(this.enterpriseModel.findOne({_id: new ObjectId(createDto.idEnterprise), isSubscribe: true }));

    return forkJoin(
      validateEmailExternalObservable,
      validateEmailInternalObservable,
      validateDocumentObservable,
      validateEnterpriseObservable
    ).pipe(
      tap((response) => {
        if (response[0] || response[1])
          throw new ConflictException("El email ya esta registrado");
      }),
      tap((response) => {
        if (response[2])
          throw new ConflictException("El documento ya esta registrado");
      }),
      tap((response) => {
        if (!response[3])
          throw new ConflictException("La empresa no existe y/o fue dado de baja");
      }),
      concatMap((response) => {
        let validEnterprise = response[3];

        let enterprise = {
          idEnterprise: validEnterprise._id,
          businessName: validEnterprise.businessName,
          ruc: validEnterprise.ruc,
          isSubscribe: validEnterprise.isSubscribe,
        };

        return this.createUser(createDto, enterprise);
      }),
      catchError((error) => {
        this.loggerService.error(`### error in create message : `, error.message + " - " + error.status);
        throw new HttpException(error.message, error.status);
      })
    );
  }

  createCollaborator(token: string, createDto: RequestCreateCollaboratorsDto) {
    this.loggerService.log(
      `### init process create with parameter : ${JSON.stringify(createDto)}`
    );

    let externalUser = decodeTokenExternalUser(token);
    const validateEmailExternalObservable = from(
      this.externalUserModel.findOne({ email: createDto.email })
    );
    const validateEmailInternalObservable = from(
      this.externalUserModel.findOne({ email: createDto.email })
    );
    const validateDocumentObservable = from(
      this.externalUserModel.findOne({
        "document.value": createDto.documentNumber,
      })
    );
    const validateEnterpriseObservable = from(
      this.enterpriseModel.findOne({
        _id: new ObjectId(externalUser.idEnterprise),
        isSubscribe: true,
      })
    );

    return forkJoin(
      validateEmailExternalObservable,
      validateEmailInternalObservable,
      validateDocumentObservable,
      validateEnterpriseObservable
    ).pipe(
      tap((response) => {
        if (response[0] || response[1])
          throw new ConflictException("El email ya esta registrado");
      }),
      tap((response) => {
        if (response[2])
          throw new ConflictException("El documento ya esta registrado");
      }),
      tap((response) => {
        if (!response[3])
          throw new ConflictException(
            "La empresa no existe y/o fue dado de baja"
          );
      }),
      concatMap((response) => {
        let validEnterprise = response[3];

        let enterprise = {
          idEnterprise: validEnterprise._id,
          businessName: validEnterprise.businessName,
          ruc: validEnterprise.ruc,
          isSubscribe: validEnterprise.isSubscribe,
        };

        return this.createUserCollaborator(createDto, enterprise);
      }),
      catchError((error) => {
        this.loggerService.error(
          `### error in create message : `,
          error.message + " - " + error.status
        );
        throw new HttpException(error.message, error.status);
      })
    );
  }

  update(updateDto: RequestUpdateDto) {
    this.loggerService.log(
      `### init process update with parameter : ${JSON.stringify(updateDto)}`
    );
    return this.updateUser(updateDto).pipe(
      catchError((error) => {
        this.loggerService.error(
          `### error in update message : `,
          error.message + " - " + error.status
        );
        throw new HttpException(error.message, error.status);
      })
    );
  }

  updateCollaborator(updateDto: RequestUpdateCollaboratorDto) {
    this.loggerService.log(
      `### init process update with parameter : ${JSON.stringify(updateDto)}`
    );
    return this.updateUserCollaborator(updateDto).pipe(
      catchError((error) => {
        this.loggerService.error(
          `### error in update message : `,
          error.message + " - " + error.status
        );
        throw new HttpException(error.message, error.status);
      })
    );
  }

  findAll(token: string) {
    this.loggerService.log(`### init process find all external user`);

    let decodeToken = decodeTokenExternalUser(token);
    
    let query = !decodeToken.isInternal
      ? { "enterprise.idEnterprise": new ObjectId(decodeToken.idEnterprise) }
      : { };

    if(Object.entries(query).length == 0)
      return  this.findAllForAdministrator(query)
    
    if(decodeToken.profile.code == CONST_PROFILE_ADMINISTRATOR.CODE || decodeToken.profile.code == CONST_PROFILE_HUMAN_RESOURCES.CODE)
      return this.findAllForAdministrator(query)

    return this.findAllForProjectManager(query)
  }

  findOneByIdExternalUser(idExternalUser: string) {
    return from(this.externalUserModel.findById(idExternalUser));
  }

  private createUser(createDto: RequestCreateDto, enterprise: any): Observable<any> {
    const profile = CONST_PROFILE.find((x) => x.ID_PROFILE == createDto.idProfile);
    const profileRoles = CONST_ROLE.find((x) => x.ID_PROFILE == profile.ID_PROFILE).ROLES;
    
    const newCreateUser: any = {
      enterprise: enterprise,
      firstName: createDto.firstName,
      lastName: createDto.lastName,
      email: createDto.email,
      password: createDto.password,
      document: {
        code: createDto.documentType,
        value: createDto.documentNumber,
      },
      profile: {
        idProfile: new ObjectId(profile.ID_PROFILE),
        code: profile.CODE,
        name: profile.NAME,
      },
      roles: profileRoles.map((x) => {
        return {
          idRole: new ObjectId(x.ID_ROLE),
          code: x.CODE,
        };
      }),
    };
    return from(new this.externalUserModel(newCreateUser).save()).pipe(
      tap((externalUser) => this.loggerService.log(`### end process create : ${JSON.stringify(externalUser._id)}`)),
      tap((externalUser) => {
        if (profile.NAME == CONST_PROFILE_ADMINISTRATOR.NAME)
          this.pushManagersInEnterprise(enterprise.idEnterprise, externalUser);
        else
          this.loggerService.warn(`### not insert manager :`, JSON.stringify(profile));
      }),
      tap((externalUser) => {
        if (profile.NAME == CONST_PROFILE_HUMAN_RESOURCES.NAME)
          this.pushHumanResourcesInEnterprise(enterprise.idEnterprise, externalUser);
        else
          this.loggerService.warn(`### not insert human resources :`, JSON.stringify(profile));
      }),
      tap((externalUser) => {
        this.pushCollaboratorsInEnterprise(enterprise.idEnterprise, externalUser);
      })
    );
  }

  private createUserCollaborator(createDto: RequestCreateCollaboratorsDto, enterprise: any): Observable<any> {
    const profile = CONST_PROFILE.find((x) => x.ID_PROFILE == createDto.idProfile);
    const profileRoles = CONST_ROLE.find((x) => x.ID_PROFILE == profile.ID_PROFILE).ROLES;
    
    const newCreateUser: any = {
      enterprise: enterprise,
      firstName: createDto.firstName,
      lastName: createDto.lastName,
      email: createDto.email,
      password: createDto.password,
      document: {
        code: createDto.documentType,
        value: createDto.documentNumber,
      },
      cost: createDto.cost,
      profile: {
        idProfile: new ObjectId(profile.ID_PROFILE),
        code: profile.CODE,
        name: profile.NAME,
      },
      roles: profileRoles.map((x) => {
        return {
          idRole: new ObjectId(x.ID_ROLE),
          code: x.CODE,
        };
      }),
    };
    return from(new this.externalUserModel(newCreateUser).save()).pipe(
      tap((externalUser) =>
        this.loggerService.log(`### end process create : ${JSON.stringify(externalUser._id)}`)
      ),
      tap((externalUser) => {
        this.pushCollaboratorsInEnterprise(enterprise.idEnterprise,externalUser);
      })
    );
  }

  private updateUser(updateDto: RequestUpdateDto) {
    const profile = CONST_PROFILE.find((x) => x.ID_PROFILE == updateDto.idProfile);
    const profileRoles = CONST_ROLE.find((x) => x.ID_PROFILE == profile.ID_PROFILE).ROLES;
    
    const newUpdateUser: any = {
      firstName: updateDto.firstName,
      lastName: updateDto.lastName,
      profile: {
        idProfile: new ObjectId(profile.ID_PROFILE),
        code: profile.CODE,
        name: profile.NAME,
      },
      roles: profileRoles.map((x) => {
        return {
          idRole: new ObjectId(x.ID_ROLE),
          code: x.CODE,
        };
      }),
      updatedAt: moment.utc().clone(),
      isOnline: false,
    };
    return from(
      this.externalUserModel.findByIdAndUpdate(updateDto.idExternalUser, newUpdateUser)
    ).pipe(
      tap(() =>
        this.loggerService.log(`### end process update : ${JSON.stringify(updateDto.idExternalUser)}`)
      )
    );
  }

  private updateUserCollaborator(updateDto: RequestUpdateCollaboratorDto) {
    const profile = CONST_PROFILE.find((x) => x.ID_PROFILE == updateDto.idProfile);
    const profileRoles = CONST_ROLE.find((x) => x.ID_PROFILE == profile.ID_PROFILE).ROLES;
    
    const newUpdateUser: any = {
      cost: updateDto.cost,
      profile: {
        idProfile: new ObjectId(profile.ID_PROFILE),
        code: profile.CODE,
        name: profile.NAME,
      },
      roles: profileRoles.map((x) => {
        return {
          idRole: new ObjectId(x.ID_ROLE),
          code: x.CODE,
        };
      }),
      updatedAt: moment.utc().clone(),
      isOnline: false,
    };
    return from(
      this.externalUserModel.findByIdAndUpdate(updateDto.idExternalUser, newUpdateUser)
    ).pipe(
      tap(() =>
        this.loggerService.log(`### end process update : ${JSON.stringify(updateDto.idExternalUser)}`)
      )
    );
  }

  private pushManagersInEnterprise(idEnterprise: any, externalUser: any) {
    const { _id, firstName, lastName, email } = externalUser;
    from(
      this.enterpriseModel.updateOne(
        { _id: new ObjectId(idEnterprise) },
        {
          $push: {
            managers: {
              idManager: new ObjectId(_id),
              firstName: firstName,
              lastName: lastName,
              email: email,
              createdAt: moment.utc().toDate(),
            },
          },
        }
      )
    ).subscribe(
      () => {},
      (error) =>
        this.loggerService.log(`### error process push manager in enterprise`, error)
    );
  }

  private pushHumanResourcesInEnterprise(idEnterprise: any, externalUser: any) {
    const { _id, firstName, lastName, email } = externalUser;
    from(
      this.enterpriseModel.updateOne(
        { _id: new ObjectId(idEnterprise) },
        {
          $push: {
            humanResources: {
              idHumanResource: new ObjectId(_id),
              firstName: firstName,
              lastName: lastName,
              email: email,
              createdAt: moment.utc().toDate(),
            },
          },
        }
      )
    ).subscribe(
      () => {},
      (error) =>
        this.loggerService.log(
          `### error process push manager in enterprise`,
          error
        )
    );
  }

  private pushCollaboratorsInEnterprise(idEnterprise: any, externalUser: any) {
    const { _id, firstName, lastName, isOnline } = externalUser;
    from(
      this.enterpriseModel.updateOne(
        { _id: new ObjectId(idEnterprise) },
        {
          $push: {
            collaborators: {
              idCollaborator: new ObjectId(_id),
              firstName: firstName,
              lastName: lastName,
              isOnline: isOnline,
              createdAt: moment.utc().toDate(),
            },
          },
        }
      )
    ).subscribe(
      () => {},
      (error) =>
        this.loggerService.log(
          `### error process push collaborators in enterprise`,
          error
        )
    );
  }

  private findAllForAdministrator(query) {
    return from(this.externalUserModel.find(query)).pipe(
      tap((externalUser) => this.loggerService.log(`### end process find all external user: ${JSON.stringify(externalUser.length)}`)),
      concatMap((externalUser) => externalUser),
      map((externalUser) => {
        return {
          idexternalUser: externalUser._id,
          firstName: externalUser.firstName,
          lastName: externalUser.lastName,
          fullName: externalUser.fullName,
          document: externalUser.document,
          email: externalUser.email,
          enterprise: externalUser.enterprise,
          profile: externalUser.profile,
          roles: externalUser.roles,
          isInternal: externalUser.isInternal,
        };
      }),
      toArray()
    );
  }

  private findAllForProjectManager(query) {
    return from(this.externalUserModel.find(query)).pipe(
      tap((externalUser) => this.loggerService.log(`### end process find all external user: ${JSON.stringify(externalUser.length)}`)),
      concatMap((externalUser) => externalUser),
      filter(externalUser => externalUser.profile.code == CONST_PROFILE_COLLABORATORS.CODE),
      map((externalUser) => {
        return {
          idexternalUser: externalUser._id,
          firstName: externalUser.firstName,
          lastName: externalUser.lastName,
          fullName: externalUser.fullName,
          document: externalUser.document,
          email: externalUser.email,
          enterprise: externalUser.enterprise,
          profile: externalUser.profile,
          roles: externalUser.roles,
          isInternal: externalUser.isInternal,
        };
      }),
      toArray()
    );
  }
}
