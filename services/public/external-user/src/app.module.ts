import { Module } from '@nestjs/common';
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { AppHttpController } from "./app.http.controller";
import { AppHttpService } from "./app.http.service";
import { ExternalUserSchema } from './models/schemas/external-user.schema';
import { SecurityModule } from '@corp_mvp_crtl_project_nodejs_ws/client-security';
import { EnterpriseSchema } from './models/schemas/enterprise.schema';
import { InternalUserSchema } from './models/schemas/internal-user.schema';
import { CONST_MODULE } from './app.constants';
@Module({
    imports: [
        SecurityModule,
        MongoDbModule,
        MongoDbModule.forFeature([
            { name: CONST_MODULE.EXTERNAL_USER, schema: ExternalUserSchema },
            { name: CONST_MODULE.INTERNAL_USER, schema: InternalUserSchema },
            { name: CONST_MODULE.ENTERPRISE, schema: EnterpriseSchema }
        ])
    ],
    controllers: [AppHttpController],
    providers: [AppHttpService]
})
export class AppModule { }