/// <reference path="./models/interfaces/project.interface.ts" />
/// <reference path="./models/interfaces/external-user.interface.ts" />
/// <reference path="./models/interfaces/enterprise.interface.ts" />

import { ConflictException, HttpException, Injectable } from "@nestjs/common";
import { RequestCreateDto } from "./models/dto/req-project.dto";
import { from, throwError, Observable, of, forkJoin, concat } from "rxjs";
import {
  concatMap,
  catchError,
  tap,
  map,
  switchMap,
  toArray,
  pluck,
  filter,
  defaultIfEmpty,
} from "rxjs/operators";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { CONST_DAYS_IN_MONTH, CONST_EVENT, CONST_EVENT_BEFORE_INITIALIZING, CONST_MODULE, CONST_PROFILE_ADMINISTRATOR } from "./app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { ObjectId } from "mongodb";
import moment = require("moment");
import { decodeTokenExternalUser } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { RequestAssignedUnassignedCollaboratorDto } from "./models/dto/req-assigned-unassigned-collaborator.dto";
import { RequestAssignedUnassignedProjectManagerDto } from "./models/dto/req-assigned-unassigned-project-manager.dto";
import { RequestAssignedUnassignedMaterialResourcesDto } from "./models/dto/req-assigned-unassigned-material-resources.dto";
import { ENUM_STATUS_DESCRIPTION, ENUM_STATUS_VALUE } from "./app.enums";
import { RequestAssignedEndDateDurationSprintDto } from "./models/dto/req-assigned-end-date-duration-sprint.dto";
import { RequestLearnedLessonsDto } from "./models/dto/req-learned-lessons.dto";
import { Cron } from '@nestjs/schedule';
import { CONST_ORDERING } from './app.constants'

@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.PROJECT) private readonly projectModel: Model<Project>,
    @InjectModel(CONST_MODULE.EXTERNAL_USER) private readonly externalUserModel: Model<ExternalUser>,
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseMode: Model<Enterprise>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService
  ) {}

  create(createDto: RequestCreateDto, token: string) {
    this.loggerService.log(`### init process create with parameter : ${JSON.stringify(createDto)}`);

    const decodeTokenForExternalUser = decodeTokenExternalUser(token);

    const createdBy: any = {
      idUserCreator: decodeTokenForExternalUser.idExternalUser,
      firstName: decodeTokenForExternalUser.firstName,
      lastName: decodeTokenForExternalUser.lastName,
      email: decodeTokenForExternalUser.email,
      profile: {
        idProfile: new ObjectId(decodeTokenForExternalUser.profile.idProfile),
        name: decodeTokenForExternalUser.profile.name,
      },
    };

    const existEnterpriseObservable = from(this.enterpriseMode.findById(decodeTokenForExternalUser.idEnterprise));
    const existProjectManagerObservable = from(this.externalUserModel.findById(createDto.idProjectManager));
    const existProjectManagerInOtherProjectObservable = from(this.projectModel.findOne({ "status.value": { $ne: 3 }, "projectManager.idProjectManager": new ObjectId(createDto.idProjectManager) }));

    return forkJoin(
      existEnterpriseObservable,
      existProjectManagerObservable,
      existProjectManagerInOtherProjectObservable
    ).pipe(
      concatMap((response) => {
        if (!response[0])
          throw new ConflictException("La empresa no existe");

        if (!response[1])
          throw new ConflictException("El jefe de proyecto asignado no existe");

        if (response[2])
          throw new ConflictException("El jefe de proyecto no puede ser asignado en mas de 2 proyectos a la vez");

        if (String(response[1]._id) == String(decodeTokenForExternalUser.idExternalUser))
          throw new ConflictException("No puedes crear proyectos y ser responsable");

        let existEnterprise = response[0];
        let existProjectManager = response[1];
        let enterprise = {
          idEnterprise: existEnterprise._id,
          businessName: existEnterprise.businessName,
          ruc: existEnterprise.ruc,
          isSubscribe: existEnterprise.isSubscribe,
        };

        return this.createProject(
          createDto,
          existProjectManager,
          createdBy,
          enterprise
        );

      }),
      catchError((error) => {
        this.loggerService.error(`### error in create project message : `,error.message + " - " + error.status);
        throw new HttpException(error.message, error.status);
      })
    );
  }

  assignedUnassignedCollaborators(
    assignedUnassignedDto: RequestAssignedUnassignedCollaboratorDto,
    token: string
  ) {

    const existProjectObservable = from(this.projectModel.findById(assignedUnassignedDto.idProject));
    const existCollaboratorObservable = from(this.externalUserModel.findById(assignedUnassignedDto.idCollaborator));
    const existCollaboratorInOtherProjectObservable = from(this.projectModel.findOne({ "status.value": { $ne: 3 }, "collaborators.idCollaborator": new ObjectId(assignedUnassignedDto.idCollaborator) }))

    return forkJoin(existProjectObservable, existCollaboratorObservable, existCollaboratorInOtherProjectObservable).pipe(
      concatMap((response) => {
        if (!response[0]) 
          throw new ConflictException("El proyecto no existe");

        //if (response[0].status.value !== ENUM_STATUS_VALUE.PROJECT_CREATE) 
        //  throw new ConflictException("Solo se puede asignar colaboradores cuando el proyecto esta creado");

        if (!response[1])
          throw new ConflictException("El colaborador no existe");
        
        if(response[2]) {
          let isActive = response[2].collaborators.find(x => String(x.idCollaborator) == assignedUnassignedDto.idCollaborator).isActive  
          if(assignedUnassignedDto.isAssigned && isActive)
            throw new ConflictException("El colaborador ya se encuentra asignado en otro proyecto - " + response[2].name);  
        }
      
        let existCollaborator = response[1];

        let collaborators = {
          idCollaborator: existCollaborator._id,
          firstName: existCollaborator.firstName,
          lastName: existCollaborator.lastName,
          email: existCollaborator.email,
          cost: existCollaborator.cost,
          isOnline: existCollaborator.isOnline,
          createdAt: moment.utc().toDate(),
          updatedAt: null,
          isActive: true
        };
        
        let event = null
        if(assignedUnassignedDto.isAssigned) {
          event = {
            idEvent: new ObjectId,
            description: CONST_EVENT.PROJECT_ASIGNED_COLLABORATOR
            .replace('REPLACE_EMAIL', existCollaborator.email)
            .replace('REPLEACE_PROFILE', existCollaborator.profile.name),
            createdAt: moment.utc().toDate()
          }
        } else {
          event = {
            idEvent: new ObjectId,
            description: CONST_EVENT.PROJECT_UNASIGNED_COLLABORATOR
            .replace('REPLACE_EMAIL', existCollaborator.email)
            .replace('REPLEACE_PROFILE', existCollaborator.profile.name),
            createdAt: moment.utc().toDate()
          }
        }
        let costProject = (response[0].status.value == ENUM_STATUS_VALUE.PROJECT_CREATE) ? 0 : response[0].cost

        return this.createAssignedUnassignedCollaborator(
          response[0]._id,
          collaborators,
          assignedUnassignedDto.isAssigned,
          costProject,
          event
        );
      }),
      tap(() => this.calculateRealCosts(assignedUnassignedDto.idProject)),
      catchError((error) => {
        this.loggerService.error(`### error in assigned unnasigned collaborators message : `,error.message + " - " + error.status);
        throw new HttpException(error.message, error.status);
      })
    );
  }

  assignedUnassignedProjectManager(assignedUnassignedDto: RequestAssignedUnassignedProjectManagerDto, token: string) {
    const existProjectObservable = from(this.projectModel.findById(assignedUnassignedDto.idProject));
    const existProjectManagerObservable = from(this.externalUserModel.findById(assignedUnassignedDto.idProjectManager));

    return forkJoin(existProjectObservable, existProjectManagerObservable).pipe(
      concatMap((response) => {
        if (!response[0])
          throw new ConflictException("El proyecto no existe");

        if (!response[1])
          throw new ConflictException("El jefe de proyecto no existe");

        let existProjectManager = response[1];

        let projectManager = {
          idProjectManager: existProjectManager._id,
          firstName: existProjectManager.firstName,
          lastName: existProjectManager.lastName,
          isOnline: existProjectManager.isOnline,
        };

        return this.createAssignedUnassignedProjectManager(
          assignedUnassignedDto.idProject,
          projectManager
        );
      })
    );
  }

  assignedUnassignedMaterialResources(assignedUnassignedDto: RequestAssignedUnassignedMaterialResourcesDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)

    let existProjectObservable = from(this.projectModel.findById(assignedUnassignedDto.idProject))
    let existMaterialResourceObservable = from(this.enterpriseMode.findById(tokenExternalUser.idEnterprise)).pipe(
      pluck('materialResources'),
      concatMap(materialResources => materialResources),
      filter(materialResource => String(materialResource.idMaterialResource) == String(assignedUnassignedDto.idMaterialResource)),
      defaultIfEmpty()
    )

    return forkJoin(existProjectObservable, existMaterialResourceObservable).pipe(
      concatMap(response => {
        if (!response[0])
          throw new ConflictException("El proyecto no existe");

        //if (response[0].status.value !== ENUM_STATUS_VALUE.PROJECT_CREATE) 
        //  throw new ConflictException("Solo se puede asignar recursos materiales cuando el proyecto esta creado");
        if (!response[1] && assignedUnassignedDto.isAssigned)
          throw new ConflictException("El recurso material no existe");

        let materialResource = {
          idMaterialResource : String(assignedUnassignedDto.idMaterialResource),
          code: null,
          name: null,
          description: null,
          quantity: null,
          singleCost: null,
          monthCost: null,
          yearCost: null,
          createdAt: null,
          updatedAt: null,
          isActive: null
        }

        let event = null;

        if(assignedUnassignedDto.isAssigned) {
          materialResource = {
            idMaterialResource: String(response[1].idMaterialResource),
            code: response[1].code,
            name: response[1].name,
            description: response[1].description,
            quantity: response[1].quantity,
            singleCost: response[1].singleCost,
            monthCost: response[1].monthCost,
            yearCost: response[1].yearCost,
            createdAt: moment.utc().toDate(),
            updatedAt: null,
            isActive: true
          }
          event = {
            idEvent: new ObjectId(),
            description: CONST_EVENT.PROJECT_ASIGNED_MATERIAL_RESOURCES.replace('REPLEACE_MATERIAL', response[1].name),
            createdAt: moment.utc().toDate()
          }
        }
        else { 
          event = {
            idEvent: new ObjectId(),
            description: CONST_EVENT.PROJECT_UNASIGNED_MATERIAL_RESOURCES.replace('REPLEACE_MATERIAL', response[1].name),
            createdAt: moment.utc().toDate()
          }
        }

        return this.createAssignedUnassignedMaterialResource(
          assignedUnassignedDto.idProject,
          materialResource,
          assignedUnassignedDto.isAssigned,
          response[0].status.value,
          event
        );
      }),
      tap(() => this.calculateRealCosts(assignedUnassignedDto.idProject)),
      catchError((error) => {
        this.loggerService.error(`### error in assigned unnasigned material resources message : `,error.message + " - " + error.status);
        throw new HttpException(error.message, error.status);
      })
    )
  }

  assignedEndDateDurationSprint(assignedDto: RequestAssignedEndDateDurationSprintDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)

    return from(this.projectModel.findOne({ "status.value": { $ne: 3 }, "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        if(String(project._id) !== String(assignedDto.idProject))
          throw new ConflictException("No puedes asignar datos a un projecto que no eres el jefe del proyecto")
      }),
      concatMap(project => {
        let endDate =  moment(assignedDto.endDate).clone().toDate()
        let endDateString =  moment(assignedDto.endDate).format('YYYY-MM-DD')
        let eventBeforeInitializing = project.eventBeforeInitializing
        eventBeforeInitializing.push(CONST_EVENT_BEFORE_INITIALIZING.READY_TO_ASSIGNED_MATERIAL_RESOURCE)
        eventBeforeInitializing = [...new Set(eventBeforeInitializing)];
        let event = {
          idEvent: new ObjectId(),
          description: CONST_EVENT.PROJECT_ASSIGNED_DURATION_SPRINT.replace('REPLEACE_DURATION_SPRINT', assignedDto.durationSprint.toString()),
          createdAt: moment.utc().toDate()
        }
        return from(this.projectModel.bulkWrite(
          [
            {
              updateOne: {
                filter: { _id: project._id },
                update: {   
                  $set: {
                    endDate: endDate,
                    endDateString: endDateString,
                    sprint: {
                      quantityDays: assignedDto.durationSprint,
                      percentageProgress: 0,
                      dates: []
                    },
                    eventBeforeInitializing: eventBeforeInitializing,
                  },
                  $addToSet: {
                    event: event
                  }
                }
              } 
            }
          ])
        )
      })
    )
  }

  initializationOfTheProject(idProject: string, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.projectModel.findOne({ "status.value": { $ne: 3}, "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        if(String(project._id) !== String(idProject))
          throw new ConflictException("No puedes inicializar un projecto que no eres el jefe del proyecto")
      }),
      concatMap(project => {
        let utcToday = moment.utc().hours(-5).startOf()
        let diffYearDate = moment(project.endDate).get('year') - utcToday.get('year');
        let diffMonthDate = moment(project.endDate).get('month') - utcToday.get('month');
        let diffDaysDate = moment(project.endDate).diff(utcToday, 'days');
        let quantityMonth = ( diffYearDate * 12 ) + (diffMonthDate) + 1;
        let costInProject = this.calculateTotalCostForProject(project, diffYearDate, quantityMonth);
        let advanceForSprintInProject = this.calculateAdvanceForSprints(utcToday, diffDaysDate, project.sprint.quantityDays, project.endDate);
        let detailsCostRealsPerMonth = this.calculateDetailsCostRealsPerMonth(utcToday.toDate(), project.endDate, quantityMonth);
        let percentageProgressInProject = this.calculatePercentageOfProgress(advanceForSprintInProject.length)

        let startDateAndEndDate = moment(project.startDate).format('DD/MM/YYYY') + 'hasta' + moment(utcToday).format('DD/NN/YYYY')
       
        let event = { 
          idEvent: new ObjectId(),
          description: CONST_EVENT.PROJECT_INITIATED.replace('REPLEACE_EMAIL', tokenExternalUser.email),
          createdAt: moment.utc().toDate()
        }
        return from(this.projectModel.findByIdAndUpdate(project._id, {
          $set: {
            startDate: utcToday.toDate(),
            startString: utcToday.format('YYYY-MM-DD'),
            cost: costInProject,
            sprint: {
              quantityMonth: quantityMonth,
              quantityDays: project.sprint.quantityDays,
              percentageProgress: percentageProgressInProject,
              advance: advanceForSprintInProject
            },
            detailsCostRealsPerMonth: detailsCostRealsPerMonth,
            status: {
              value: ENUM_STATUS_VALUE.PROJECT_INITIATED,
              description: ENUM_STATUS_DESCRIPTION.PROJECT_INITIATED
            }
          },
          $addToSet: {
            event: event
          }
        }))
      }),
      tap(() => this.calculateRealCosts(idProject)),
      catchError((error) => { 
        this.loggerService.error(`### error in initialization of the project message : `, error.message + " - " + error.status);
        throw new HttpException(error.message, error.status);
      })
    )
  }

  learnedLessons(learnedLessonDto: RequestLearnedLessonsDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.projectModel.findOne({ "status.value": 3, "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        if(String(project._id) !== String(learnedLessonDto.idProject))
          throw new ConflictException("No puedes registrar lecciones aprendidas a un projecto que no eres el jefe del proyecto")
      }),
      concatMap(project => {
        let event = {
          idEvent: new ObjectId(),
          description: CONST_EVENT.PROJECT_LEARDNED_LESSON.replace('REPLACE_EMAIL', tokenExternalUser.email),
          createdAt: moment.utc().toDate()
        }
        return from(this.projectModel.updateOne(
          { _id: project._id }, 
          { $set: { learnedLessons: learnedLessonDto.commentary }, $addToSet: { event: event } }))
      })
    )
  }

  findAll(token: string) {
    this.loggerService.log(`### init process find all projects`);
    let decodeToken = decodeTokenExternalUser(token);
    let query = !decodeToken.isInternal
    ? (decodeToken.profile.idProfile == CONST_PROFILE_ADMINISTRATOR.ID) ? {  "enterprise.idEnterprise": new ObjectId(decodeToken.idEnterprise) } : { "projectManager.idProjectManager": new ObjectId(decodeToken.idExternalUser) }
      : {};
    return from(this.projectModel.find(query).sort({ createdAt: CONST_ORDERING.DESCENDING })).pipe(
      tap((project) =>
        this.loggerService.log(
          `### end process find all project: ${JSON.stringify(project.length)}`
        )
      ),
      concatMap((project) => project),
      map((project) => {
        return {
          idProject: project._id,
          enterprise: project.enterprise,
          name: project.name,
          startDate: project.startDate,
          endDate: project.endDate,
          projectManager: project.projectManager,
          collaborators: project.collaborators.filter(x => x.isActive),
          materialResources: project.materialResources.filter(x => x.isActive),
          createdAt: project.createdAt,
          updatedAt: project.updatedAt,
          isSubscribe: project.isSubscribe,
          sprint: project.sprint,
          eventBeforeInitializing: project.eventBeforeInitializing,
          cost: project.cost,
          costReal: project.costReal,
          budget: project.budget,
          status: project.status,
          event: project.event
        };
      }),
      toArray()
    );
  }

  findOneByIdProject(idProject: string) {
    return from(this.projectModel.findById(idProject)).pipe(
      map(project => {
        return {
          _id: project._id,
          enterprise: project.enterprise,
          name: project.name,
          startDate: project.startDate,
          startString: project.startString,
          endDate: project.endDate,
          endDateString: project.endDateString,
          projectManager: project.projectManager,
          collaborators: project.collaborators.filter(x => x.isActive),
          materialResources: project.materialResources.filter(x => x.isActive),
          createdAt: project.createdAt,
          updatedAt: project.updatedAt,
          isSubscribe: project.isSubscribe,
          sprint: project.sprint,
          eventBeforeInitializing: project.eventBeforeInitializing,
          cost: project.cost,
          costReal: project.costReal,
          learnedLessons: project.learnedLessons,
          budget: project.budget,
          status: project.status,
          progress: project.progress,
          event: project.event,
          indicatorRealCost: 'BLUE' //this.calculatedIndicatorCostReal(project.budget, project.detailsCostRealsPerMonth)
        }
      })
    );
  }

  terminationOfTheProject(idProject: string, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    let event = null
    return from(this.projectModel.findOne({ "status.value": { $ne: 3}, "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        if(String(project._id) !== String(idProject))
          throw new ConflictException("No puedes finalizar un projecto porque tu no eres el jefe del proyecto")

        event = {
          idEvent: new ObjectId(),
          description: CONST_EVENT.PROJECT_TERMINATION.replace('REPLACE_EMAIL', tokenExternalUser.email),
          createdAt: moment.utc().toDate()
        }

      }),
      concatMap( () => {
        return from(this.projectModel.findByIdAndUpdate(idProject, { $set: { 
          "status.value": 3,
          "status.description": "Terminado"
        }, $addToSet: {
          event: event
        }}))
      }),
      catchError((error) => { 
        this.loggerService.error(`### error in termination of the project message : `, error.message + " - " + error.status);
        throw new HttpException(error.message, error.status);
      })
    )
  }

  @Cron('59 23 1 * *')
  async handleCron() {
    let utcToday = moment.utc()

    let month = this.calculateMonth(utcToday.get('month'))
    let year = utcToday.get('year')
    
    let projects = await this.projectModel.find({ "status.value": { $ne: ENUM_STATUS_VALUE.PROJECT_CLOSED } })
    projects.forEach(project => {
      this.loggerService.log(`### init process for calculated cost for project ${project.name} month [${month}] - year [${year}]`);
      this.calculateRealCosts(project._id)
    });

  }

  private createProject(
    createDto: RequestCreateDto,
    projectManager: any,
    createdBy: any,
    enterprise: any
  ) {
    let newCreateProject = {
      enterprise: {
        idEnterprise: new ObjectId(enterprise.idEnterprise),
        businessName: enterprise.businessName,
        ruc: enterprise.ruc,
        isSubscribe: enterprise.isSubscribe,
      },
      name: createDto.name,
      budget: createDto.budget,
      projectManager: {
        idProjectManager: new ObjectId(projectManager._id),
        firstName: projectManager.firstName,
        lastName: projectManager.lastName,
        createdAt: moment.utc().clone(),
        cost: projectManager.cost,
        isOnline: projectManager.isOnline,
      },
      progress: 0,
      cost: 0,
      costReal: 0,
      collaborators: [],
      materialResources: [],
      documents: [],
      event: [ {
        idEvent: new ObjectId(),
        description: CONST_EVENT.PROJECT_CREATE.replace('REPLACE_EMAIL', createdBy.email),
        createdAt: moment.utc().toDate()
      }],
      eventBeforeInitializing: [
        CONST_EVENT_BEFORE_INITIALIZING.READY_TO_ASSIGNED_DURATION_SPRINT
      ],
      sprint: {
        quantityDays: 0,
        percentageProgress: 0,
        advance: []
      },
      createdBy: createdBy,
    };

    return from(new this.projectModel(newCreateProject).save()).pipe(
      tap((project) =>
        this.loggerService.log(`### end process create : ${JSON.stringify(project._id)}`)
      ),
      tap((project) => {
        this.pushProjectsInEnterprise(enterprise.idEnterprise, project);
      })
    );
  }

  private createAssignedUnassignedCollaborator(
    idProject: string,
    collaborators: any,
    isAssigned: boolean,
    costProject: number,
    event: any
  ) {

    let query = isAssigned
      ? [{
          updateOne: {
            filter: { _id: new ObjectId(idProject) },
            update: {
              $set: { cost: costProject },
              $push: { collaborators: collaborators },
              $addToSet: { event: event, eventBeforeInitializing: CONST_EVENT_BEFORE_INITIALIZING.READY_TO_INITIALIZING_OF_THE_PROJECT } 
            } 
          },
        }]
      : [{
          updateOne: {
            filter: { _id: new ObjectId(idProject), "collaborators.idCollaborator": new ObjectId(collaborators.idCollaborator) },
            update: {   
              //$pull: { collaborators: { idCollaborator: new ObjectId(collaborators.idCollaborator) } },
              $set: { 
                "collaborators.$[itemCollaborator].isActive": false,
                "collaborators.$[itemCollaborator].updatedAt": moment.utc().toDate()
              },
              $addToSet: { event: event, eventBeforeInitializing: CONST_EVENT_BEFORE_INITIALIZING.READY_TO_INITIALIZING_OF_THE_PROJECT }

            },
            arrayFilters: [ {"itemCollaborator.idCollaborator": new ObjectId(collaborators.idCollaborator)} ]
          }
        }];

    return from(this.projectModel.bulkWrite(query)).pipe(
      tap(() => this.loggerService.log(`### end process assigned unassigned collaborators : ${JSON.stringify(idProject)}`)),
    );
  }

  private createAssignedUnassignedProjectManager(
    idProject: string,
    projectManager: any
  ) {
    return from(
      this.projectModel.findByIdAndUpdate(idProject, {
        $set: {
          idProjectManager: new ObjectId(projectManager.idProjectManager),
          firstName: projectManager.firstName,
          lastName: projectManager.lastName,
          isOnline: projectManager.isOnline,
        },
      })
    ).pipe(
      tap(() =>
        this.loggerService.log(
          `### end process assigned unassigned collaborators : ${JSON.stringify(
            idProject
          )}`
        )
      )
    );
  }

  private createAssignedUnassignedMaterialResource(idProject: string, materialResource: any, isAssigned: boolean, projectStatus: number, event: any) {
    let updateOne = {}
    if(projectStatus == ENUM_STATUS_VALUE.PROJECT_CREATE) {
      updateOne = {
        filter: { _id: new ObjectId(idProject), "materialResources._id": new ObjectId(materialResource.idMaterialResource) },
        update: {  
          $set: { updatedAt: moment.utc().toDate() }, 
          $pull: { materialResources: { _id: new ObjectId(materialResource.idMaterialResource) } },
          $addToSet: { event: event, eventBeforeInitializing: CONST_EVENT_BEFORE_INITIALIZING.READY_TO_ASSIGNED_COLLABORATOR } 
        }
      }
    } else {
      updateOne = {
        filter: { _id: new ObjectId(idProject), "materialResources._id": new ObjectId(materialResource.idMaterialResource) },
        update: {
          $set: { 
            updatedAt: moment.utc().toDate(),
            "materialResources.$[itemMaterialResource].isActive": false,
            "materialResources.$[itemMaterialResource].updatedAt": moment.utc().toDate()
          },
          $addToSet: { event: event, eventBeforeInitializing: CONST_EVENT_BEFORE_INITIALIZING.READY_TO_ASSIGNED_COLLABORATOR } 
        },
        arrayFilters: [ { "itemMaterialResource._id": new ObjectId(materialResource.idMaterialResource) } ]
      }
    }

    let query = isAssigned
      ? [{
          updateOne: {
            filter: { _id: new ObjectId(idProject) },
            update: {    
              $set: { updatedAt: moment.utc().toDate() },
              $push: { 
                materialResources: materialResource,
              },
              $addToSet: { event: event, eventBeforeInitializing: CONST_EVENT_BEFORE_INITIALIZING.READY_TO_ASSIGNED_COLLABORATOR } 
            }
          },
        }]
      : [{
          updateOne: updateOne
        }];
      
    return from(this.projectModel.bulkWrite(query)).pipe(
      tap(() => this.loggerService.log(`### end process assigned unassigned material resources : ${JSON.stringify(idProject)}`))
    );
  }

  private calculateTotalCostForProject(project: any, diffYearDate: number, quantityMonth: number) {
    let projectCost = 0;
    let collaboratorsCost = 0;
    let materialCost = 0;

    if (project.collaborators)
      collaboratorsCost = project.collaborators.map((a: { cost: any; }) => a.cost).reduce((a: any, b: any) => a + b, 0);

    if(project.materialResources) {
      let singleCosts = project.materialResources.map((a: { singleCost: any; }) => a.singleCost).reduce(function (a: any, b: any) { return a + b; });
      let monthCosts = project.materialResources.map((a: { monthCost: any; }) => a.monthCost).reduce(function (a: any, b: any) { return a + b; });
      let yearCosts = project.materialResources.map((a: { yearCost: any; }) => a.yearCost).reduce(function (a: any, b: any) { return a + b; });
      if(singleCosts > 0)
        materialCost = singleCosts
      
      if(monthCosts > 0)
        materialCost = materialCost + (monthCosts * quantityMonth)

      materialCost =  (diffYearDate == 0) ? materialCost + yearCosts : materialCost + (yearCosts * diffYearDate)
    }
    projectCost = project.projectManager.cost

    return ( projectCost * quantityMonth ) + ( collaboratorsCost * quantityMonth ) + materialCost

  }

  private calculatePercentageOfProgress(quantitySprintInProject){
    let quantityPercentageProgress = 100 / quantitySprintInProject
    return quantityPercentageProgress
  }

  private calculateAdvanceForSprints(date: any, quantityDaysInProject: number, quantityDaysInSprint: number, limitDate: any) : ProjectAdvance[] {
    let advanceForSprints = []
    let  quantitySprintsInProject = (quantityDaysInProject/quantityDaysInSprint)
    let dateString = moment(date).format('YYYY-MM-DD')
    let endDateSum = 0
    let initDaysSum = 0
    for (let index = 0; index < quantitySprintsInProject; index++) {
      initDaysSum = index == 0 ? 0 : (endDateSum + 1)
      endDateSum = quantityDaysInSprint + endDateSum

      let startDate = (initDaysSum == 0) ? dateString : moment(dateString).days(initDaysSum).format('YYYY-MM-DD')
      let endDate = moment(dateString).days(endDateSum).format('YYYY-MM-DD')

      let diff = moment(limitDate).diff(endDate, 'days');

      if(diff >= 0) {
        advanceForSprints.push(this.generateAdvanceForSprint(index, startDate, endDate))
      } else {
        limitDate = moment(limitDate).format('YYYY-MM-DD')
        advanceForSprints.push(this.generateAdvanceForSprint(index, startDate, limitDate));
        break;
      }
    }
    return advanceForSprints
  }

  private calculateDetailsCostRealsPerMonth(startDate: Date, endDate: Date, quantityMonth: number) {
    let detailsCostRealsPerMonth = []
    for (let index = 0; index < quantityMonth; index++) {
      const startOfMonth = moment(startDate).startOf('month').format('YYYY-MM-DD');
      const endOfMonth   = moment(startDate).endOf('month').format('YYYY-MM-DD');
      detailsCostRealsPerMonth.push({
        idDetailCostRealPerMonth: new ObjectId(),
        year: moment(startDate).year(),
        month:  this.calculateMonth(moment(startDate).month()),
        startDate: startDate,
        startString: startOfMonth,
        endDate: moment(endOfMonth).format('YYYY-MM-DD'),
        endString: endOfMonth,
        createdAt: moment.utc().toDate(),
        updatedAt: null,
        costs: []
      })
      startDate = moment(startDate).add(1, 'months').toDate()
    }
    return detailsCostRealsPerMonth;
  }

  private calculateRealCosts(idProject: string){
    from(this.projectModel.findById(idProject)).pipe(
      concatMap(project => {
        let utcToday = moment.utc().clone();
        let year = utcToday.year().toString();
        let month =  this.calculateMonth(utcToday.month());
        this.loggerService.warn(' ### calculated cost year per month' , year + month);

        let detailsCostReals = []
        let quantityMonth = project.sprint.quantityMonth

        this.pushAssignedCostReal('MATERIAL_RESOURCES', project.materialResources, detailsCostReals, quantityMonth);
        this.pushAssignedCostReal('COLLABORATORS', project.collaborators, detailsCostReals, quantityMonth);
        this.pushAssignedCostReal('PROJECTMANAGER', project.projectManager, detailsCostReals, quantityMonth);
        
        this.loggerService.warn(' ### calculated cost year per month details' , JSON.stringify(detailsCostReals));
        
        let costReal = detailsCostReals.map(x => x.cost).reduce(function (a: any, b: any) { return a + b; });
        let detailCostRealPerMonth = project.detailsCostRealsPerMonth.find(x => x.year == year && x.month == month)
        if(!detailCostRealPerMonth)
          throw new ConflictException(`No existe un detalle de costo por mes para las siguientes fechas ${year} - ${month}`)

        return from(this.projectModel.bulkWrite([{
          updateOne: {
            filter: { _id: new ObjectId(project._id), "detailsCostRealsPerMonth.idDetailCostRealPerMonth": new ObjectId(detailCostRealPerMonth.idDetailCostRealPerMonth) },
            update: {
              $set: {
                costReal: costReal,
                'detailsCostRealsPerMonth.$[itemCostDetailPerMonth].costs': detailsCostReals,
                'detailsCostRealsPerMonth.$[itemCostDetailPerMonth].updatedAt': moment().utc().toDate()
               }
            },
            arrayFilters: [ { 'itemCostDetailPerMonth.idDetailCostRealPerMonth': new ObjectId(detailCostRealPerMonth.idDetailCostRealPerMonth)  } ]
          }
        }]))
      })
    ).subscribe(
      () => {},
      (error) => this.loggerService.log(`### error process detail real cost`,error)
    );
  }

  private calculateMonth(numberMonth: number) {
    let months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    return months[numberMonth]
  }

  private calculatedProrrateoCost(type: string, startDate: Date, endDate: Date, costMonth: number) : number {
    this.loggerService.warn(' ### prorrateo cost' , type, startDate, endDate);
    let diffDays = moment(endDate).diff(moment(startDate), 'days');
    this.loggerService.warn(' ### prorrateo cost diff' , diffDays);
    let days = diffDays == 0 ? 1 : diffDays
    return (costMonth  / 30 ) * days
  }

  private calculatedIndicatorCostReal(budget: number, detailsCostRealsPerMonth: any[]) {
    let utcToday = moment.utc().clone();
    let year = utcToday.year().toString();
    let month =  this.calculateMonth(utcToday.month());
    let budgetPerMonth = budget / detailsCostRealsPerMonth.length
    let detailCostRealPerMonth = detailsCostRealsPerMonth.find(x => x.year == year && x.month == month)
    if(detailCostRealPerMonth.length) {

      if(detailCostRealPerMonth.costs) {
        let costRealPerMonth = detailCostRealPerMonth.costs.map(x => x.cost).reduce((a: any, b: any) => a + b, 0);
        if( costRealPerMonth == budget ) {
          return 'RED'
        }
        if( costRealPerMonth > budgetPerMonth && costRealPerMonth < budget ) {
          return 'YELLOW'
        }
        if( budgetPerMonth > costRealPerMonth ) {
          return 'BLUE'
        }
      }
    }
    return 'BLUE'
  }

  private generateAdvanceForSprint(index, startDate, endDate) {
    return {
      idAdvance: new ObjectId(),
      name: `SPRINT NRO - ${index + 1}`,
      startDate: startDate,
      endDate: endDate,
      description: '',
      observations: '',
      status: {
        value: ENUM_STATUS_VALUE.PROJECT_SPRINT_PENDING,
        description: ENUM_STATUS_DESCRIPTION.PROJECT_SPRINT_PENDING
      },
      progress: 0,
      userStories: [],
      indicators: {
        notCompleted: false,
        completed: false,
        isMedium: false,
        isLow: false,
        isHigh: false
      },
      documents: [],
      createdAt: moment.utc().toDate(),
      updatedAt: null
    }
  }

  private pushProjectsInEnterprise(idEnterprise: any, project: any) {
    const { _id, name, projectManager } = project;
    from(
      this.enterpriseMode.updateOne(
        { _id: new ObjectId(idEnterprise) },
        {
          $push: {
            projects: {
              idProject: new ObjectId(_id),
              name: name,
              boss: {
                idBoss: new ObjectId(projectManager.idProjectManager),
                fullName:
                  projectManager.firstName + " " + projectManager.lastName,
                email: projectManager.email,
              },
              createdAt: moment.utc().toDate(),
            },
          },
        }
      )
    ).subscribe(
      () => {},
      (error) => this.loggerService.log(`### error process push project in enterprise`,error)
    );
  }

  private pushAssignedCostReal(typeCharge: string, charge: any[] | any, detailsCostReal: any[], quantityMonth) : void {
    switch (typeCharge) {
      case 'MATERIAL_RESOURCES':
        let singleCosts = charge.filter(x => x.singleCost > 0);
        this.pushAssignedCostRealSingleCost(singleCosts, detailsCostReal);

        let yearCosts = charge.filter(x => x.yearCost > 0);
        this.pushAssignedCostRealYearCost(yearCosts, detailsCostReal, quantityMonth);

        let monthCosts = charge.filter(x => x.monthCost > 0);
        this.pushAssignedCostRealMonthCost(monthCosts, detailsCostReal);

        break;
    
      case 'COLLABORATORS':
        this.pushAssignedCostRealCollaborators(charge, detailsCostReal)
        break;

      case 'PROJECTMANAGER':
        this.pushAssignedCostRealProjectManager(charge, detailsCostReal)
        break;
    }
  }

  private pushAssignedCostRealSingleCost(singleCosts: any[], detailsCostReal: any[]): void {
    this.loggerService.warn(' ### material resources: assigned cost real single cost' , JSON.stringify(singleCosts));
    singleCosts.forEach(x => {
      detailsCostReal.push({
        idCost: new ObjectId(),
        cost: x.singleCost,
        charge: x.name,
        createdAt: moment.utc().toDate(),
        updatedAt: null
      })
    })
  }

  private pushAssignedCostRealMonthCost(monthCosts: any[], detailsCostReal: any[]) {
    this.loggerService.warn(' ### material resources: assigned cost real month cost' , JSON.stringify(monthCosts));
    monthCosts.forEach(x => {
      detailsCostReal.push({
        idCost: new ObjectId(),
        cost: (x.monthCost > 0 && x.updatedAt == null) ? x.cost : this.calculatedProrrateoCost('material resource', x.createdAt, x.updatedAt, x.cost),
        charge: 'Desarrollador',
        createdAt: moment.utc().toDate(),
        updatedAt: null
      })
    })
  }

  private pushAssignedCostRealYearCost(yearCosts: any[], detailsCostReal: any[], quantityMonth: number) : void {
    this.loggerService.warn(' ### material resources: assigned cost real year cost' , JSON.stringify(yearCosts));
    yearCosts.forEach(x => {
      detailsCostReal.push({
        idCost: new ObjectId(),
        cost: x.yearCost / quantityMonth,
        charge: x.name,
        createdAt: moment.utc().toDate(),
        updatedAt: null
      })
    })
  }

  private pushAssignedCostRealCollaborators(collaborators: any[], detailsCostReal: any[]) {
    this.loggerService.warn(' ### collaborators: assigned cost real month cost' , JSON.stringify(collaborators));
    collaborators.forEach(x => {
      detailsCostReal.push({
        idCost: new ObjectId(),
        cost: (x.cost > 0 && x.updatedAt == null) ? x.cost : this.calculatedProrrateoCost('developer', x.createdAt, x.updatedAt, x.cost),
        charge: 'Desarrollador',
        createdAt: moment.utc().toDate(),
        updatedAt: null
      })
    })
  }

  private pushAssignedCostRealProjectManager (projectManager: any, detailsCostReal: any[]) {
    this.loggerService.warn(' ### project manager: assigned cost real month cost' , JSON.stringify(projectManager));
    detailsCostReal.push({
      idCost: new ObjectId(),
      cost: projectManager.cost,
      charge: 'Jefe Proyecto',
      createdAt: moment.utc().toDate(),
      updatedAt: null
    })
  }

}