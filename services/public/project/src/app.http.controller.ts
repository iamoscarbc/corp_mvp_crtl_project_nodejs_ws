import {
  ENUM_Roles,
  Roles,
  RolesGuard,
  SecurityGuard,
  Token,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  UseGuards,
  Get,
  Param,
  Put,
} from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";
import { AppHttpService } from "./app.http.service";
import { RequestAssignedEndDateDurationSprintDto } from "./models/dto/req-assigned-end-date-duration-sprint.dto";
import { RequestAssignedUnassignedCollaboratorDto } from "./models/dto/req-assigned-unassigned-collaborator.dto";
import { RequestAssignedUnassignedMaterialResourcesDto } from "./models/dto/req-assigned-unassigned-material-resources.dto";
import { RequestAssignedUnassignedProjectManagerDto } from "./models/dto/req-assigned-unassigned-project-manager.dto";
import { RequestFindOneByIdProjectDto } from "./models/dto/req-find-one-by-idproject.dto";
import { RequestLearnedLessonsDto } from "./models/dto/req-learned-lessons.dto";
import { RequestCreateDto } from "./models/dto/req-project.dto";

@ApiBearerAuth()
@UseGuards(SecurityGuard, RolesGuard)
@ApiUseTags("Responsible service for all projects in project")
@Controller("Project")
export class AppHttpController {
  constructor(private readonly appService: AppHttpService) {}

  @ApiOperation({ title: "Create", description: "Create all projet" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.CREATE_PROJECT)
  @Post("create")
  create(@Body() createDto: RequestCreateDto, @Token() token: string) {
    return this.appService.create(createDto, token);
  }

  @ApiOperation({
    title: "Assigned - Unassigned project manager",
    description: "Assigned unassigned project manager in the projet",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.ASSIGNED_RESPONSIBLE_PROJECT,ENUM_Roles.UNASSIGNED_RESPONSIBLE_PROJECT)
  @Post("assigned-unassigned-project-manager")
  assignedUnassignedProjectManager(@Body() assignedUnassignedDto: RequestAssignedUnassignedProjectManagerDto,@Token() token: string) {
    return this.appService.assignedUnassignedProjectManager(
      assignedUnassignedDto,
      token
    );
  }

  @ApiOperation({
    title: "Assigned - Unassigned collaborators",
    description: "Assigned unassigned collaborators in the projet",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.ASSIGNED_COLABORATOR_PROJECT,ENUM_Roles.UNASSIGNED_COLABORATOR_PROJECT)
  @Put("assigned-unassigned-collaborators")
  assignedUnassignedCollaborators(@Body() assignedUnassignedDto: RequestAssignedUnassignedCollaboratorDto,@Token() token: string
  ) {
    return this.appService.assignedUnassignedCollaborators(assignedUnassignedDto, token);
  }

  @ApiOperation({
    title: "Assigned - Unassigned material resources",
    description: "Assigned unassigned material resources in the projet",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.ASSIGNED_MATERIAL_RESOURCES,ENUM_Roles.UNASSIGNED_MATERIAL_RESOURCES)
  @Put("assigned-unassigned-material-resources")
  assignedUnassignedMaterialResources(@Body() assignedUnassignedDto: RequestAssignedUnassignedMaterialResourcesDto,@Token() token: string) {
    return this.appService.assignedUnassignedMaterialResources(
      assignedUnassignedDto,
      token
    );
  }

  @ApiOperation({
    title: "Assigned - duration sprint",
    description: "Assigned duration sprint in the projet",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.ASSIGNED_DURATION_SPRINT_PROJECT)
  @Put("assigned-end-date-duration-sprint")
  assignedEndDateDurationSprint(
    @Body() assignedDto: RequestAssignedEndDateDurationSprintDto,
    @Token() token: string
  ) {
    return this.appService.assignedEndDateDurationSprint(
      assignedDto,
      token
    );
  }

  @ApiOperation({title: "Initialization of the project", description: "Initialization of the project in the projets" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.INITIALIZATION_OF_THE_PROJECT)
  @Post("initialization-of-the-project")
  initializationOfTheProject(@Body() { idProject }: RequestFindOneByIdProjectDto, @Token() token: string) {
    return this.appService.initializationOfTheProject(idProject,token);
  }

  @ApiOperation({ title: "Learned lessons sprint", description: "Learned lessons by sprint in the projet" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Post("learned-lessons")
  learnedLessons(
      @Body() learnedLessonDto: RequestLearnedLessonsDto,
      @Token() token: string
  ) {
      return this.appService.learnedLessons(learnedLessonDto, token);
  }

  @ApiOperation({ title: "Find-All", description: "FInd all projects" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("find-all")
  findAll(@Token() token: string) {
    return this.appService.findAll(token);
  }

  @ApiOperation({
    title: "Find-one-by-idProject",
    description: "Find one project in all projects",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("find-one-by-idProject/:idProject")
  findOneByIdProject(@Param() { idProject }: RequestFindOneByIdProjectDto) {
    return this.appService.findOneByIdProject(idProject);
  }

  @ApiOperation({title: "Termination of the project", description: "Termination of the project in the projets" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.INITIALIZATION_OF_THE_PROJECT)
  @Post("termination-of-the-project")
  terminationOfTheProject(@Body() { idProject }: RequestFindOneByIdProjectDto, @Token() token: string) {
    return this.appService.terminationOfTheProject(idProject,token);
  }

}
