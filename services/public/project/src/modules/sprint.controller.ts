import {
    ENUM_Roles,
    Roles,
    RolesGuard,
    SecurityGuard,
    Token,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import {
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    Body,
    UseGuards,
    Get,
    Param,
    Put,
} from "@nestjs/common";
import { ApiUseTags, ApiBearerAuth, ApiOperation } from "@nestjs/swagger";
import { RequestAssignedUnassignedUserStoriesDto } from "../models/dto/req-assigned-unassigned-user-stories.dto";
import { RequestDoneReportSprintDto } from "../models/dto/req-done-report-sprint.dto";
import { RequestDoneUserStoriesDto } from "../models/dto/req-done-user-story.dto";
import { SprintService } from "./sprint.service";
  
@ApiBearerAuth()
@UseGuards(SecurityGuard, RolesGuard)
@ApiUseTags("Responsible service for all sprints in project")
@Controller("Sprint")
export class SprintController {
    constructor(private readonly sprintService: SprintService) {}

    @ApiOperation({ title: "Report - sprint", description: "Report sprint in the projet" })
    @HttpCode(HttpStatus.OK)
    @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
    @Put("done-report")
    doneReport(
        @Body() reportDto: RequestDoneReportSprintDto,
        @Token() token: string
    ) {
        return this.sprintService.doneReport(reportDto,token);
    }

    @ApiOperation({ title: "Report - sprint", description: "Report sprint in the projet" })
    @HttpCode(HttpStatus.OK)
    @Roles(ENUM_Roles.ASSIGNED_USER_HISTORY_SPRINT, ENUM_Roles.UNASSIGNED_USER_HISTORY_SPRINT)
    @Put("assigned-unassigned-user-stories")
    assignedUnassignedUserStories(
        @Body() assignedUnassignedDto: RequestAssignedUnassignedUserStoriesDto,
        @Token() token: string
    ) {
        return this.sprintService.assignedUnassignedUserStories(assignedUnassignedDto, token);
    }

    @ApiOperation({ title: "Done - sprint user history", description: "Done sprint user history in the projet" })
    @HttpCode(HttpStatus.OK)
    @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT, ENUM_Roles.ASSIGNED_USER_HISTORY_SPRINT, ENUM_Roles.UNASSIGNED_USER_HISTORY_SPRINT)
    @Put("done-user-stories")
    doneUserStories(
        @Body() doneUserStoryDto: RequestDoneUserStoriesDto,
        @Token() token: string
    ) {
        return this.sprintService.doneUserStories(doneUserStoryDto, token);
    }

}
  