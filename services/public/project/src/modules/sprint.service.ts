import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { decodeTokenExternalUser } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { ConflictException, HttpException, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ObjectId } from "mongodb";
import { from, of } from "rxjs";
import { filter, map } from 'rxjs/operators';
import { CONST_MODULE } from "../app.constants";
import { RequestDoneReportSprintDto } from "../models/dto/req-done-report-sprint.dto";
import { catchError, concatMap, switchMap, tap } from "rxjs/operators";
import { ENUM_STATUS_DESCRIPTION, ENUM_STATUS_VALUE } from "../app.enums";
import moment = require("moment");
import { RequestAssignedUnassignedUserStoriesDto } from "../models/dto/req-assigned-unassigned-user-stories.dto";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { RequestDoneUserStoriesDto } from "../models/dto/req-done-user-story.dto";

@Injectable()
export class SprintService {
  constructor(
    @InjectModel(CONST_MODULE.PROJECT) private readonly projectModel: Model<Project>,
    @InjectModel(CONST_MODULE.EXTERNAL_USER) private readonly externalUserModel: Model<ExternalUser>,
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseMode: Model<Enterprise>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService
  ) {}

  doneReport(doneReportDto: RequestDoneReportSprintDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)

    return from(this.projectModel.findOne({  "status.value": { $ne: 3 }, "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
        concatMap(project => {
            if(!project)
                throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

            let sprint = project.sprint.advance.find(x => String(x.idAdvance) == String(doneReportDto.idAdvance))
            if(!sprint)
              throw new ConflictException("No existe el sprint en este proyecto : ", project.name)

            let userStoriesCompleted = sprint.userStories.filter(x => x.isCompleted)
            let progressUserStoriesCompleted = userStoriesCompleted.map(y => y.percentageOfSprint).reduce((a: any, b: any) => a + b, 0);
            this.loggerService.warn('progress user stories completed :', progressUserStoriesCompleted)

            let transforProgress = this.calculateTransforProfress(project.sprint.percentageProgress, progressUserStoriesCompleted);
            let indicators = this.calculateIndicators(project.sprint.percentageProgress, transforProgress);
            let progress = this.calculateProgressForProject(project, transforProgress, doneReportDto.idAdvance);

            let operation = [{
              updateOne: {
                filter: { _id: new ObjectId(project._id), "sprint.advance.idAdvance": new ObjectId(doneReportDto.idAdvance) },
                update: {   
                  $set: {
                    progress: progress,
                    "sprint.advance.$[item].description": '',
                    "sprint.advance.$[item].observations": '',
                    "sprint.advance.$[item].progress": progressUserStoriesCompleted,
                    "sprint.advance.$[item].status": {
                        value: ENUM_STATUS_VALUE.PROJECT_SPRINT_CONCLUDED,
                        description: ENUM_STATUS_DESCRIPTION.PROJECT_SPRINT_CONCLUDED
                    },
                    "sprint.advance.$[item].updatedAt": moment.utc().toDate(),
                    "sprint.advance.$[item].indicators": {
                      completed: indicators.completed,
                      notCompleted: indicators.notCompleted,
                      isMedium: indicators.isMedium,
                      isLow: indicators.isLow,
                      isHigh: indicators.isHigh
                    }
                   }
                },
                arrayFilters: [ { 'item.idAdvance': new ObjectId(doneReportDto.idAdvance) } ]
              }
            }]
            return from(this.projectModel.bulkWrite(operation))
        }),
        catchError((error) => {
          this.loggerService.error(`### error done report [ message : `, error.message + " ] [ satatus: " + error.status + " ]");
          throw new HttpException(error.message, error.status);
        })
    )
  }

  assignedUnassignedUserStories(assignedUnassignedDto: RequestAssignedUnassignedUserStoriesDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    let idProject = null
    return from(this.projectModel.findOne({ "status.value": { $ne: 3 },  "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        let sprintInProject = project.sprint.advance.find(x => String(x.idAdvance) == String(assignedUnassignedDto.idAdvance))
        if(!sprintInProject)
          throw new ConflictException("No existe el sprint en este proyecto : ", project.name)

        idProject = project._id
      }),
      concatMap(project => {
        return this.createAssignedUnassignedUserStories(project._id, assignedUnassignedDto)
      }),
      tap( () => this.calculateProgressOfAllSprintInProject(idProject, assignedUnassignedDto.idAdvance))
    )
  }

  doneUserStories(doneUserStoryDto: RequestDoneUserStoriesDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    let idProject = null
    return from(this.projectModel.findOne({  "status.value": { $ne: 3 }, "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        let sprintInProject = project.sprint.advance.find(x => String(x.idAdvance) == String(doneUserStoryDto.idAdvance))

        if(!sprintInProject)
          throw new ConflictException("No existe el sprint en este proyecto : ", project.name)

        let userHistory = sprintInProject.userStories.find(x => String(x.idUserStory) == String(doneUserStoryDto.idUserStory))
        if(!userHistory)
          throw new ConflictException(`No existe la HU en este proyecto - ${project.name}`)

        idProject = project._id
      }),
      concatMap(project => {
        return this.createDoneUserStories(project._id, doneUserStoryDto)
      }),
      catchError((error) => {
        this.loggerService.error(`### error done user stories [ message : `, error.message + " ] [ satatus: " + error.status + " ]");
        throw new HttpException(error.message, error.status);
      })
      //tap( () => this.calculateProgressOfAllSprintInProject(idProject, doneUserStoryDto.idAdvance))
    )
  }

  private calculateTransforProfress(percentageProgress: number, percentageProgressInSprint: number) {
    return (percentageProgress * percentageProgressInSprint) / 100
  }

  private calculateIndicators(percentageProgress: number, percentageProgressInSprint: number) {
    this.loggerService.warn(' - percentageProgress - ', percentageProgress, ' - percentageProgressInSprint - ', percentageProgressInSprint)
    let mediumUp = percentageProgress - 1
    let mediumDown = (percentageProgress/2) - 0.5
    let completed = (percentageProgress == percentageProgressInSprint)
    return {
      completed: completed,
      notCompleted: !completed,
      isHigh: (percentageProgressInSprint > mediumUp),
      isMedium: (percentageProgressInSprint >= mediumDown && percentageProgressInSprint <= mediumUp),
      isLow: (percentageProgressInSprint < mediumDown)
    }
  }

  private calculateProgressForProject(project: any, progress: number, idAdvance: string) {
    let total = 0;
    if (project.sprint.advance) {
      let advance = project.sprint.advance.filter((x: { idAdvance: any; }) => String(x.idAdvance) !== String(idAdvance))
      total = advance.map((a: { progress: any; }) => this.calculateTransforProfress(project.sprint.percentageProgress, a.progress)).reduce((a: any, b: any) => a + b, 0);
    }
    return total + progress
  }

  private createAssignedUnassignedUserStories(idProject: string, assignedUnassignedDto: RequestAssignedUnassignedUserStoriesDto ) {
    let query = (assignedUnassignedDto.isAssigned) ? [{
      updateOne: {
        filter: { _id: new ObjectId(idProject), "sprint.advance.idAdvance": new ObjectId(assignedUnassignedDto.idAdvance) },
        update: { $addToSet: { "sprint.advance.$[item].userStories" : {
          idUserStory: new ObjectId(),
          name: assignedUnassignedDto.name,
          observation: assignedUnassignedDto.observation,
          createdAt: moment.utc().toDate(),
          updatedAt: null,
          percentageOfSprint: 0,
          isCompleted: false,
          status: {
              value: ENUM_STATUS_VALUE.PROJECT_SPRINT_USER_STORY_CREATE,
              description: ENUM_STATUS_DESCRIPTION.PROJECT_SPRINT_USER_STORY_CREATE
          }
        } } },
        arrayFilters: [{ "item.idAdvance": new ObjectId(assignedUnassignedDto.idAdvance) }]
      },
    }] : [{
      updateOne: {
        filter: { _id: new ObjectId(idProject), "sprint.advance.idAdvance": new ObjectId(assignedUnassignedDto.idAdvance) },
        update: { $pull: { "sprint.advance.$[item].userStories.$[subItem].idUserStory": new ObjectId(assignedUnassignedDto.idAdvance) } },
        arrayFilters: [{ "item.idAdvance": new ObjectId(assignedUnassignedDto.idAdvance) }]
      },
    }]

    return from(this.projectModel.bulkWrite(query)).pipe(
      tap(() => this.loggerService.log(`### end process assigned unassigned collaborators : ${JSON.stringify(idProject)}`)),
    );
  }

  private createDoneUserStories(idProject: string, doneUserStoryDto: RequestDoneUserStoriesDto) {
    let operation = [{
      updateOne: {
        filter: { _id: new ObjectId(idProject) },
        update: {
          $set: {
            'sprint.advance.$[itemAdvance].userStories.$[itemUserStory].isCompleted': doneUserStoryDto.isCompleted,
            'sprint.advance.$[itemAdvance].userStories.$[itemUserStory].status': {
              value: ENUM_STATUS_VALUE.PROJECT_SPRINT_USER_STORY_INITIATED,
              description: ENUM_STATUS_DESCRIPTION.PROJECT_SPRINT_USER_STORY_INITIATED
            }
          }
        },
        arrayFilters: [ 
          { 'itemAdvance.idAdvance': new ObjectId(doneUserStoryDto.idAdvance) }, 
          { 'itemUserStory.idUserStory': new ObjectId(doneUserStoryDto.idUserStory) }
        ]
      }
    }]
    return from(this.projectModel.bulkWrite(operation)).pipe(
      tap( () => this.loggerService.log("### operation bulkwrite done user stories", JSON.stringify(operation)))
    )
  }

  private calculateProgressOfAllSprintInProject(idProject: string, idAdvancex: string) {
    from(this.projectModel.findById(idProject)).pipe(
      switchMap(project => project.sprint.advance),
      filter(advance => String(advance.idAdvance) == String(idAdvancex)),
      map(({ idAdvance, userStories }) => {
        let percentageOfSprint = (100 / (userStories.length))
        let operation = [{
          updateOne: {
            filter: { _id: new ObjectId(idProject) },
            update: {
              $set: {
                'sprint.advance.$[itemAdvance].userStories.$[].percentageOfSprint': percentageOfSprint
              }
            },
            arrayFilters: [ { 'itemAdvance.idAdvance': idAdvance }]
          }
        }]
        this.loggerService.log("### operation bulkwrite percetageOfSprint", JSON.stringify(operation))
        from(this.projectModel.bulkWrite(operation)).subscribe()
      })
    ).subscribe( () => {}, (error) => this.loggerService.error( `### error process find project`, error ));
  }
  
}