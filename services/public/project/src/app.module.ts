import { HttpModule, Module } from '@nestjs/common';
import { AppHttpController } from "./app.http.controller";
import { AppHttpService } from "./app.http.service";
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { CONST_MODULE } from './app.constants';
import { SecurityModule } from '@corp_mvp_crtl_project_nodejs_ws/client-security';
import { ProjectSchema } from './models/schemas/project.schema';
import { ExternalUserSchema } from './models/schemas/external-user.schema';
import { EnterpriseSchema } from './models/schemas/enterprise.schema';
import { SprintModule } from './modules/sprint.module';
import { LoggerModule } from '@corp_mvp_crtl_project_nodejs_ws/logger';
import { ScheduleModule } from '@nestjs/schedule';
@Module({
    imports: [
        HttpModule,
        SecurityModule,
        SprintModule,
        LoggerModule,
        MongoDbModule,
        MongoDbModule.forFeature([
            { name: CONST_MODULE.PROJECT, schema: ProjectSchema },
            { name: CONST_MODULE.EXTERNAL_USER, schema: ExternalUserSchema },
            { name: CONST_MODULE.ENTERPRISE, schema: EnterpriseSchema }
        ]),
        ScheduleModule.forRoot()
    ],
    controllers: [AppHttpController],
    providers: [AppHttpService]
})
export class AppModule { }