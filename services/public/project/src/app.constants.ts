export const CONST_MODULE = {
    PROJECT: 'Project',
    EXTERNAL_USER: 'ExternalUser',
    ENTERPRISE: 'Enterprise'
}

export const CONST_EVENT = {
  PROJECT_CREATE: 'Proyecto ha sido creado por el usuario REPLACE_EMAIL',
  PROJECT_ASIGNED_COLLABORATOR: 'Se asigno el siguiente usuario REPLACE_EMAIL con perfil REPLEACE_PROFILE',
  PROJECT_UNASIGNED_COLLABORATOR: 'Se desasigno el siguiente usuario REPLACE_EMAIL con perfil REPLEACE_PROFILE',
  PROJECT_ASIGNED_MATERIAL_RESOURCES: 'Se asigno el siguiente material REPLEACE_MATERIAL',
  PROJECT_UNASIGNED_MATERIAL_RESOURCES: 'Se desasigno el siguiente material REPLEACE_MATERIAL',
  PROJECT_INITIATED: 'El proyecto ha sido iniciado por el usuario con email REPLEACE_EMAIL',
  PROJECT_ASSIGNED_DURATION_SPRINT: 'Se asigno la duracion de REPLEACE_DURATION_SPRINT para los sprint',
  PROJECT_LEARDNED_LESSON: 'Se asignaron las lecciones aprendidas por el usuario REPLACE_EMAIL',
  PROJECT_TERMINATION: 'El proyecto aa sido terminado por el usuario REPLACE_EMAIL',
}

export const CONST_EVENT_BEFORE_INITIALIZING = {
    READY_TO_ASSIGNED_DURATION_SPRINT: 'READY_TO_ASSIGNED_DURATION_SPRINT',
    READY_TO_ASSIGNED_COLLABORATOR: 'READY_TO_ASSIGNED_COLLABORATOR',
    READY_TO_ASSIGNED_MATERIAL_RESOURCE: 'READY_TO_ASSIGNED_MATERIAL_RESOURCE',
    READY_TO_INITIALIZING_OF_THE_PROJECT: 'READY_TO_INITIALIZING_OF_THE_PROJECT'
}

export const CONST_DAYS_IN_MONTH = {
    VALUE : 30
}


export const CONST_PROFILE_ADMINISTRATOR = {
    ID: "6056a25a93e26f99efa6a3ef",
    NAME: "Administrador",
    CODE: "ADM",
    IS_VIEW_ADM: true,
  };
  
  export const CONST_PROFILE_HUMAN_RESOURCES = {
    ID: "605bb2132e0c8173b7d2031c",
    NAME: "Recursos Humanos",
    CODE: "RHH",
    IS_VIEW_ADM: true,
  };
  
  export const CONST_PROFILE_PROJECT_MANAGER = {
    ID: "605bb43655b54ee157c1d4d9",
    NAME: "Jefe de proyecto",
    CODE: "PJM",
    IS_VIEW_ADM: false,
  };
  
  export const CONST_PROFILE_COLLABORATORS = {
    ID: "605bb465a40ceafedc0f4445",
    NAME: "Colaboradores",
    CODE: "DEV",
    IS_VIEW_ADM: false,
  };
  
  export const CONST_ORDERING = {
    ASCENDING: 1,
    DESCENDING: -1
  }