import { ObjectId } from "mongodb";
import { Document } from "mongoose";

declare global {
  export interface ExternalUser extends Document {
    email: string;
    enterprise: {
      idEnterprise: ObjectId;
      businessName: string;
      ruc: string;
      isSuscribe: boolean;
    };
    cost: number;
    profile: {
      idProfile: ObjectId;
      name: string;
      code: string;
    };
    roles: [
      {
        idRole: ObjectId;
        code: string;
      }
    ];
    fullName: string;
    firstName: string;
    lastName: string;
    document: {
      code: string;
      value: string;
    };
    password: string;
    createdAt: Date;
    updatedAt: Date;
    isOnline: boolean;
    isInternal: boolean;
  }
}
