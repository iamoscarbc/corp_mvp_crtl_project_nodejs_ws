import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsBoolean } from "class-validator";

export class RequestAssignedUnassignedCollaboratorDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idProject: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idCollaborator: string;

  @IsBoolean()
  @IsNotEmpty()
  @ApiModelProperty()
  isAssigned: boolean;
}
