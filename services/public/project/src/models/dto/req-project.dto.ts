import { ApiModelProperty } from "@nestjs/swagger";
import {} from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class RequestCreateDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idProjectManager: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty()
  budget: number;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  name: string;
}
