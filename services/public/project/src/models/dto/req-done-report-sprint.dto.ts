import { ApiModelProperty } from "@nestjs/swagger";
import {} from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsNotEmpty, IsString } from "class-validator";

export class RequestDoneReportSprintDto {

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idAdvance: string;

}
