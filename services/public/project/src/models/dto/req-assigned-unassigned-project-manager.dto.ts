import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "@corp_mvp_crtl_project_nodejs_ws/setup";

export class RequestAssignedUnassignedProjectManagerDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idProject: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idProjectManager: string;
}
