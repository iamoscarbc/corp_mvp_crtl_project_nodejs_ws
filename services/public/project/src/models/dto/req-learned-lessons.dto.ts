import { ApiModelProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from '@corp_mvp_crtl_project_nodejs_ws/setup';

export class RequestLearnedLessonsDto {

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    idProject: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    commentary: string

}