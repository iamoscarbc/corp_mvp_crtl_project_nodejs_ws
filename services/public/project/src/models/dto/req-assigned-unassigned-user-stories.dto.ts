import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsBoolean } from "class-validator";

export class RequestAssignedUnassignedUserStoriesDto {
  
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idAdvance: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  observation: string;

  @IsBoolean()
  @IsNotEmpty()
  @ApiModelProperty()
  isAssigned: boolean;

}
