import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty, IsNumber } from "@corp_mvp_crtl_project_nodejs_ws/setup";

export class RequestAssignedEndDateDurationSprintDto {

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idProject: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  endDate: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty()
  durationSprint: number;
}
