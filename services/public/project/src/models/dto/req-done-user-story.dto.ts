import { ApiModelProperty } from "@nestjs/swagger";
import { IsString, IsNotEmpty } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsBoolean } from "class-validator";

export class RequestDoneUserStoriesDto {
  
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idAdvance: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idUserStory: string;

  @IsBoolean()
  @IsNotEmpty()
  @ApiModelProperty()
  isCompleted: boolean;

}
