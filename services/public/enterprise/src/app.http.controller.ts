import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  UseGuards,
  Get,
  Put,
  Param,
} from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";
import {
  ENUM_Roles,
  SecurityGuard,
  Token,
  RolesGuard,
  Roles,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { AppHttpService } from "./app.http.service";
import { RequestCreateDto } from "./models/dto/req-create.dto";
import { RequestSuscribeDto } from "./models/dto/req-create.dto copy";
import { RequestFindEnterpriseRucDto } from "./models/dto/req-find-enterprise-ruc.dto";
import { RequestFindOneByIdEnterpriseDto } from "./models/dto/req-find-one-by-identerprise.dto";
@ApiBearerAuth()
@UseGuards(SecurityGuard, RolesGuard)
@ApiUseTags("Responsible service for all enterprise in project")
@Controller("Enterprise")
export class AppHttpController {
  constructor(private readonly appService: AppHttpService) {}

  @ApiOperation({
    title: "Find enterprise by ruc",
    description: "Find all enterprise by ruc for using service external",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.CREATE_ENTERPRISE)
  @Get("find-enterprise-by-ruc/:ruc")
  findEnterprisesByRuc(
    @Param() { ruc }: RequestFindEnterpriseRucDto,
    @Token() token: string
  ) {
    return this.appService.findEnterprisesByRuc(ruc);
  }

  @ApiOperation({ title: "Create", description: "Create all enterprise" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.CREATE_ENTERPRISE)
  @Post("create")
  create(@Body() createDto: RequestCreateDto, @Token() token: string) {
    return this.appService.create(createDto, token);
  }

  @ApiOperation({ title: "Suscribe", description: "Suscribe all enterprise" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.UNSUSCRIBE_ENTERPRISE)
  @Put("suscribe")
  suscribe(@Body() suscribeDto: RequestSuscribeDto, @Token() token: string) {
    return this.appService.suscribe(suscribeDto, token);
  }

  @ApiOperation({ title: "Find-All", description: "FInd all enterprises" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_ENTERPRISE)
  @Get("find-all")
  findAll() {
    return this.appService.findAll();
  }

  @ApiOperation({
    title: "Find-one-by-identerprise",
    description: "Find one enterprise for idEnterprise",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_ENTERPRISE)
  @Get("find-one-by-idEnterprise/:idEnterprise")
  findOneByIdEnterprise(
    @Param() { idEnterprise }: RequestFindOneByIdEnterpriseDto
  ) {
    return this.appService.findOneByIdEnterprise(idEnterprise);
  }
}
