export const CONST_MODULE = {
    ENTERPRISE: 'Enterprise',
    INTERNAL_USER: 'InternalUser',
    EXTERNAL_USER: 'ExternalUser'
}

export const CONST_ORDERING = {
    ASCENDING: 1,
    DESCENDING: -1
}