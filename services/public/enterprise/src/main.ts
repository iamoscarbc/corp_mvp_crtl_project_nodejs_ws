import { create } from '@corp_mvp_crtl_project_nodejs_ws/setup'
import { TCP, HTTP } from '@corp_mvp_crtl_project_nodejs_ws/config'
import { AppModule } from './app.module'
import { Transport } from '@nestjs/microservices'

create(AppModule, app => {
  app.connectMicroservice({
    transport: Transport.TCP, options: { port: TCP.PORT, host: TCP.HOST }
  });
})