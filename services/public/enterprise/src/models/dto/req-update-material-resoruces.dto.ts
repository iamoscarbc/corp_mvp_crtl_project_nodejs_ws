import { ApiModelProperty } from "@nestjs/swagger";
import {
  IsString,
  IsNotEmpty
} from "@corp_mvp_crtl_project_nodejs_ws/setup";

export class RequestUpdateMaterialResourcesDto {

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idMaterialResource: string;  

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  type: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  description: string;

}
