import { ApiModelProperty } from "@nestjs/swagger";
import {
  IsString,
  IsNotEmpty,
  IsNumber,
} from "@corp_mvp_crtl_project_nodejs_ws/setup";

export class RequestAssignMaterialResourcesDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idMaterialResource: string;
}
