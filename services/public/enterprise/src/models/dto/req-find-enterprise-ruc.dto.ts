import { ApiModelProperty } from '@nestjs/swagger'
import { IsNotEmpty, Length, IsNumberString } from '@corp_mvp_crtl_project_nodejs_ws/setup';
export class RequestFindEnterpriseRucDto {

    @IsNumberString()
    @IsNotEmpty()
    @Length(11, 11)
    @ApiModelProperty()
    ruc: string

}