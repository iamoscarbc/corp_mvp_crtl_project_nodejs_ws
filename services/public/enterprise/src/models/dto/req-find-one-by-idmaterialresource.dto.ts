import { ApiModelProperty } from "@nestjs/swagger";
import {
  IsString,
  IsNotEmpty
} from "@corp_mvp_crtl_project_nodejs_ws/setup";

export class RequestFindOneByIdMaterialResourceDto {

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  idMaterialResource: string;

}
