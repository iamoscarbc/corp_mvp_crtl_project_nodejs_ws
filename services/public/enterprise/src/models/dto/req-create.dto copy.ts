import { ApiModelProperty } from '@nestjs/swagger'
import { IsString, IsNotEmpty } from '@corp_mvp_crtl_project_nodejs_ws/setup';
import { IsBoolean } from 'class-validator';

export class RequestSuscribeDto {

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    idEnterprise: string

    @IsBoolean()
    @IsNotEmpty()
    @ApiModelProperty()
    isSuscribe: boolean

}