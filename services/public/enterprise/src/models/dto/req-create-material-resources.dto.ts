import { ApiModelProperty } from "@nestjs/swagger";
import {
  IsString,
  IsNotEmpty,
  IsNumber,
} from "@corp_mvp_crtl_project_nodejs_ws/setup";

export class RequestCreateMaterialResourcesDto {
  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  type: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiModelProperty()
  description: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty()
  quantity: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty()
  yearCost: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty()
  monthCost: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiModelProperty()
  singleCost: number;
}
