import { ApiModelProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString } from '@corp_mvp_crtl_project_nodejs_ws/setup';

export class RequestFindOneByIdEnterpriseDto {

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    idEnterprise: string

}