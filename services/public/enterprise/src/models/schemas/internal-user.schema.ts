import { ObjectId } from "mongodb";
import * as mongoose from "mongoose";
import * as moment from "moment";

export const InternalUserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      unique: true,
      index: true,
    },
    profile: {
      idProfile: ObjectId,
      name: String,
      code: String,
    },
    roles: [
      {
        idRole: ObjectId,
        code: String,
      },
    ],
    password: String,
    fullName: String,
    firstName: String,
    lastName: String,
    document: {
      code: String,
      value: {
        type: String,
        unique: true,
        index: true,
      },
    },
    isOnline: {
      type: Boolean,
      index: true,
    },
    createdAt: {
      type: Date,
      default: () => moment.utc().clone(),
    },
    updatedAt: {
      type: Date,
      default: null,
    },
  },
  {
    collection: "InternalUsers",
  }
);

InternalUserSchema.pre("save", function (this: any, next) {
  this.fullName = `${this.firstName} ${this.lastName}`;
  next();
});
