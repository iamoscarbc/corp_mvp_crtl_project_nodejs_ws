import { HttpModule, Module } from '@nestjs/common';
import { AppHttpController } from "./app.http.controller";
import { AppHttpService } from "./app.http.service";
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { EnterpriseSchema } from './models/schemas/enterprise.schema';
import { InternalUserSchema } from './models/schemas/internal-user.schema';
import { CONST_MODULE } from './app.constants';
import { SecurityModule } from '@corp_mvp_crtl_project_nodejs_ws/client-security';
import { MaterialResourcesModule } from './modules/material-resources/material-resources.module';
@Module({
    imports: [
        HttpModule,
        SecurityModule,
        MaterialResourcesModule,
        MongoDbModule,
        MongoDbModule.forFeature([
            { name: CONST_MODULE.ENTERPRISE, schema: EnterpriseSchema },
            { name: CONST_MODULE.INTERNAL_USER, schema: InternalUserSchema }
        ])
    ],
    controllers: [AppHttpController],
    providers: [AppHttpService]
})
export class AppModule { }