/// <reference path="./models/interfaces/enterprise.interface.ts" />
/// <reference path="./models/interfaces/internal-user.interface.ts" />

import {
  ConflictException,
  HttpException,
  Injectable,
  HttpService,
} from "@nestjs/common";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { CONST_MODULE, CONST_ORDERING } from "./app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { RequestCreateDto } from "./models/dto/req-create.dto";
import { forkJoin, from, of } from "rxjs";
import { catchError, concatMap, map, tap, toArray } from "rxjs/operators";
import { ObjectId } from "mongodb";
import {
  decodeTokenInternalUser,
} from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { RequestSuscribeDto } from "./models/dto/req-create.dto copy";

@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseModel: Model<Enterprise>,
    @InjectModel(CONST_MODULE.INTERNAL_USER) private readonly internalUserModule: Model<InternalUser>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService,
    private readonly http: HttpService
  ) {}

  findEnterprisesByRuc(ruc: string) {
    return forkJoin([this.externalUriForRuc(ruc)]).pipe(
      concatMap((response) => {
        let externalServiceRuc = response[0].data;
        this.loggerService.log(
          `### externalServiceRuc : `,
          JSON.stringify(externalServiceRuc)
        );
        if (externalServiceRuc) return of(externalServiceRuc);

        of({});
      })
    );
  }

  create(createDto: RequestCreateDto, token: string) {
    this.loggerService.log(
      `### init process create with parameter : ${JSON.stringify(createDto)}`
    );

    const decodeTokenForInternalUser = decodeTokenInternalUser(token);
    const createdBy: any = {
      idUserCreator: new ObjectId(decodeTokenForInternalUser.idInternalUser),
      firstName: decodeTokenForInternalUser.firstName,
      lastName: decodeTokenForInternalUser.lastName,
      profileCode: decodeTokenForInternalUser.profileCode,
    };
    const validateRucObservable = from(
      this.enterpriseModel.findOne({
        ruc: String(createDto.ruc),
        isSubscribe: true,
      })
    );

    return forkJoin([
      this.externalUriForRuc(createDto.ruc),
      validateRucObservable,
    ]).pipe(
      concatMap((response) => {
        let externalServiceRuc = response[0].data;
        this.loggerService.log(
          `### externalServiceRuc : `,
          JSON.stringify(externalServiceRuc)
        );

        if (response[1]) {
          throw new ConflictException("El ruc ya se encuentra registrado");
        }

        return this.createEnterprise(externalServiceRuc, createDto, createdBy);
      })
    );
  }

  suscribe(suscribeDto: RequestSuscribeDto, token: string) {
    this.loggerService.log(
      `### init process suscribe with parameter : ${JSON.stringify(
        suscribeDto
      )}`
    );

    return from(
      this.enterpriseModel.findByIdAndUpdate(suscribeDto.idEnterprise, {
        isSubscribe: suscribeDto.isSuscribe,
      })
    ).pipe(
      tap(() =>
        this.loggerService.log(
          "Agregar todos los documentos que se veran afectado al dar de baja a una empresa"
        )
      ),
      tap(() =>
        this.loggerService.log(
          `### end process suscribe : ${JSON.stringify(
            suscribeDto.idEnterprise
          )}`
        )
      ),
      catchError((error) => {
        this.loggerService.error(
          `### error suscribe message : `,
          error.message + " - " + error.status
        );
        throw new HttpException(error.message, error.status);
      })
    );
  }

  findAll() {
    this.loggerService.log(`### init process find all enterprises`);
    return from(this.enterpriseModel.find({}).sort({ createdAt: CONST_ORDERING.DESCENDING })).pipe(
      tap((enterprise) =>
        this.loggerService.log(
          `### end process find all enterprises: ${JSON.stringify(
            enterprise.length
          )}`
        )
      ),
      concatMap((enterprise) => enterprise),
      map((enterprise) => {
        return {
          idEnterprise: enterprise._id,
          ruc: enterprise.ruc,
          businessName: enterprise.businessName,
          createdBy: enterprise.createdBy,
          humanResources: enterprise.humanResources,
          managers: enterprise.managers,
          projects: enterprise.projects,
          createdAt: enterprise.createdAt,
          updatedAt: enterprise.updatedAt,
          isSubscribe: enterprise.isSubscribe,
        };
      }),
      toArray()
    );
  }

  findOneByIdEnterprise(idEnterprise: string) {
    this.loggerService.log(`### init process find one enterprise`);
    return from(this.enterpriseModel.findById(idEnterprise)).pipe(
      tap((enterprise) =>
        this.loggerService.log(
          `### end process find one enterprise: ${JSON.stringify(enterprise)}`
        )
      ),
      map((enterprise) => {
        return {
          idEnterprise: enterprise._id,
          ruc: enterprise.ruc,
          businessName: enterprise.businessName,
          createdBy: enterprise.createdBy,
          humanResources: enterprise.humanResources,
          managers: enterprise.managers,
          projects: enterprise.projects,
          createdAt: enterprise.createdAt,
          updatedAt: enterprise.updatedAt,
          isSubscribe: enterprise.isSubscribe,
        };
      }),
      toArray()
    );
  }

  private createEnterprise(
    externalServiceRuc: any,
    createDto: RequestCreateDto,
    createdBy: any
  ) {
    const newCreateEnterprise = {
      ruc: externalServiceRuc.ruc,
      businessName: externalServiceRuc.nombre_o_razon_social,
      estado_del_contribuyente: externalServiceRuc.estado_del_contribuyente,
      condicion_de_domicilio: externalServiceRuc.condicion_de_domicilio,
      ubigeo: externalServiceRuc.ubigeo,
      tipo_de_via: externalServiceRuc.tipo_de_via,
      nombre_de_via: externalServiceRuc.nombre_de_via,
      codigo_de_zona: externalServiceRuc.codigo_de_zona,
      tipo_de_zona: externalServiceRuc.tipo_de_zona,
      numero: externalServiceRuc.numero,
      interior: externalServiceRuc.interior,
      lote: externalServiceRuc.lote,
      dpto: externalServiceRuc.dpto,
      manzana: externalServiceRuc.manzana,
      kilometro: externalServiceRuc.kilometro,
      departamento: externalServiceRuc.departamento,
      provincia: externalServiceRuc.provincia,
      distrito: externalServiceRuc.distrito,
      direccion: externalServiceRuc.direccion,
      direccion_completa: externalServiceRuc.direccion_completa,
      ultima_actualizacion: externalServiceRuc.ultima_actualizacion,
      managers: [],
      projects: [],
      createdBy: createdBy,
    };

    return from(new this.enterpriseModel(newCreateEnterprise).save()).pipe(
      tap((enterprise) =>
        this.loggerService.log(
          `### end process create : ${JSON.stringify(enterprise._id)}`
        )
      )
    );
  }

  private updateEnterprise() {}

  private externalUriForRuc(ruc: string) {
    const externalServiceUrl = process.env.EXTERNAL_SERVICE_RUC_URL;
    const externalServiceToken = process.env.EXTERNAL_SERVICE_RUC_TOKEN;
    return from(
      this.http.post(externalServiceUrl, {
        ruc: ruc,
        token: externalServiceToken,
      })
    );
  }
}
