/// <reference path="../../models/interfaces/enterprise.interface.ts" />
/// <reference path="../../models/interfaces/internal-user.interface.ts" />

import {
  ConflictException,
  HttpException,
  Injectable,
  HttpService,
  HttpStatus,
  BadRequestException,
} from "@nestjs/common";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { CONST_MODULE } from "../../app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { forkJoin, from, of } from "rxjs";
import { catchError, concatMap, filter, map, pluck, tap, toArray } from "rxjs/operators";
import moment = require("moment");
import { ObjectId } from "mongodb";
import {
  decodeTokenExternalUser,
  decodeTokenInternalUser,
} from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { RequestAssignMaterialResourcesDto } from "../../models/dto/req-assign-material-resoruces.dto";
import { RequestCreateMaterialResourcesDto } from "../../models/dto/req-create-material-resources.dto";
import { RequestUpdateMaterialResourcesDto } from "../../models/dto/req-update-material-resoruces.dto";

@Injectable()
export class MaterialResourcesService {
  constructor(
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseModel: Model<Enterprise>,
    @InjectModel(CONST_MODULE.INTERNAL_USER) private readonly internalUserModule: Model<InternalUser>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService
  ) {}

  create(createDto: RequestCreateMaterialResourcesDto, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token);
    return from(
      this.enterpriseModel.updateOne(
        { _id: new ObjectId(tokenExternalUser.idEnterprise) },
        {
          $push: { 
            materialResources: { 
              idMaterialResource: new ObjectId(),
              code: createDto.type,
              name: createDto.name,
              description: createDto.description,
              quantity: createDto.quantity,
              monthCost: createDto.monthCost,
              yearCost: createDto.yearCost,
              singleCost: createDto.singleCost,
              createdAt: moment.utc().toDate(),
            }
          },
        }
      )
    );
  }

  update(updateDto: RequestUpdateMaterialResourcesDto, token: string ) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.enterpriseModel.updateOne(
      { _id: new ObjectId(tokenExternalUser.idEnterprise), "materialResources.idMaterialResource": new ObjectId(updateDto.idMaterialResource) },
      { $set: {
          "materialResources.$[item].code": updateDto.type,
          "materialResources.$[item].name": updateDto.name,
          "materialResources.$[item].description": updateDto.description
      } },
      { arrayFilters: [ { "item.idMaterialResource": new ObjectId(updateDto.idMaterialResource) }], multi: true }))
  }

  findAll(token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.enterpriseModel.findById(tokenExternalUser.idEnterprise)).pipe(
      pluck('materialResources'),
      map(materialResources => materialResources)
    )
  }

  findOneByIdMaterialResource(idMaterialResource: string, token: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.enterpriseModel.findById(tokenExternalUser.idEnterprise)).pipe(
      pluck('materialResources'),
      concatMap(materialResources => materialResources),
      filter(materialResource => String(materialResource.idMaterialResource) == String(idMaterialResource))
    )
  }
}
