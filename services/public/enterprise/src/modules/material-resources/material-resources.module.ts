import { Module } from "@nestjs/common";
import { MongoDbModule } from "@corp_mvp_crtl_project_nodejs_ws/database";
import { SecurityModule } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { MaterialResourcesController } from "./material-resources.controller";
import { CONST_MODULE } from "../../app.constants";
import { EnterpriseSchema } from "../../models/schemas/enterprise.schema";
import { InternalUserSchema } from "../../models/schemas/internal-user.schema";
import { MaterialResourcesService } from "./material-resources.service";

@Module({
  imports: [
    SecurityModule,
    MongoDbModule,
    MongoDbModule.forFeature([
      { name: CONST_MODULE.ENTERPRISE, schema: EnterpriseSchema },
      { name: CONST_MODULE.INTERNAL_USER, schema: InternalUserSchema },
    ]),
  ],
  exports: [
    MaterialResourcesService
  ],
  controllers: [MaterialResourcesController],
  providers: [ MaterialResourcesService ],
})
export class MaterialResourcesModule {}
