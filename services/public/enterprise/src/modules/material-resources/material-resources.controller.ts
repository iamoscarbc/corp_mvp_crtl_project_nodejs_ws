import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  UseGuards,
  Put,
  Get,
  Param,
} from "@nestjs/common";
import {
  ApiUseTags,
  ApiOperation,
  ApiBearerAuth,
  ApiModelProperty,
} from "@nestjs/swagger";
import {
  ENUM_Roles,
  SecurityGuard,
  Token,
  RolesGuard,
  Roles,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { MaterialResourcesService } from "./material-resources.service";
import { RequestCreateMaterialResourcesDto } from "../../models/dto/req-create-material-resources.dto";
import { RequestUpdateMaterialResourcesDto } from "../../models/dto/req-update-material-resoruces.dto";
import { RequestFindOneByIdMaterialResourceDto } from "../../models/dto/req-find-one-by-idmaterialresource.dto";

@ApiBearerAuth()
@UseGuards(SecurityGuard, RolesGuard)
@ApiUseTags(
  "Responsible service for all material resources for enterprise in project"
)
@Controller("Material-Resources")
export class MaterialResourcesController {
  constructor(
    private readonly materialResourcesService: MaterialResourcesService
  ) {}

  @ApiOperation({
    title: "Create Material resources",
    description: "Create material resources in all enterprises",
  })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.CREATE_MATERIAL_RESOURCES)
  @Post("create")
  create(
    @Body() createDto: RequestCreateMaterialResourcesDto,
    @Token() token: string
  ) {
    return this.materialResourcesService.create(createDto, token);
  }

  @ApiOperation({
    title: "Update Material resources",
    description: "Update material resources in all enterprises",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.UPDATE_MATERIAL_RESOURCES)
  @Put("update")
  update(
    @Body() updateDto: RequestUpdateMaterialResourcesDto,
    @Token() token: string
  ) {
    return this.materialResourcesService.update(updateDto, token);
  }

  @ApiOperation({
    title: "Find all Material resources",
    description: "Find all material resources in all enterprises",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_MATERIAL_RESOURCES)
  @Get("find-all")
  findAll(
    @Token() token: string
  ) {
    return this.materialResourcesService.findAll(token);
  }

  @ApiOperation({
    title: "Find one Material resources",
    description: "Find one by id material resources in all enterprises",
  })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.TOLIST_MATERIAL_RESOURCES)
  @Get("find-one-by-idMaterialResource/:idMaterialResource")
  findOneByIdMaterialResource(
    @Param() { idMaterialResource } : RequestFindOneByIdMaterialResourceDto,
    @Token() token: string
  ) {
    return this.materialResourcesService.findOneByIdMaterialResource(idMaterialResource, token);
  }
}
