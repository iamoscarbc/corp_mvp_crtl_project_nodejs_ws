/// <reference path="./models/interfaces/internal-user.interface.ts" />

import { Injectable, ConflictException, HttpException } from "@nestjs/common";
import { RequestCreateDto } from "./models/dto/req-create.dto";
import { from, throwError, Observable, of, forkJoin, zip } from "rxjs";
import { concatMap, catchError, tap, map, switchMap, toArray } from "rxjs/operators"
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { CONST_MODULE, CONST_PROFILE, CONST_ROLE } from "./app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import moment = require("moment");
import { RequestUpdateDto } from "./models/dto/req-update.dto";
import { ObjectId } from "mongodb";

@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.INTERNAL_USER) private readonly internalUserModel: Model<InternalUser>,
    private readonly loggerService: LoggerService
  ) { }

  profiles(){
    return from(CONST_PROFILE).pipe(
      map(profiles => {
        return {
          idProfile: profiles.ID_PROFILE,
          name: profiles.NAME,
          code: profiles.CODE
        }
      }),
      toArray()
    )
  }

  create(createDto: RequestCreateDto){
    this.loggerService.log(`### init process create with parameter : ${JSON.stringify(createDto)}`)

    const validateEmailObservable = from(this.internalUserModel.findOne({ email: createDto.email }))
    const validateDocumentObservable = from(this.internalUserModel.findOne({ "document.value": createDto.documentNumber }))

    return forkJoin(validateEmailObservable, validateDocumentObservable).pipe(
      concatMap((response) => {
        if(response[0])
          throw new ConflictException('El email ya esta registrado')

        if(response[1])
          throw new ConflictException('El numero de documento ya esta registrado')

        return this.createUser(createDto)
      }),
      catchError(error => {
        this.loggerService.error(`### error in create message : `, error.message + ' - ' + error.status)
        throw new HttpException(error.message, error.status)
      })
    )  
  }

  update(updateDto: RequestUpdateDto){
    this.loggerService.log(`### init process update with parameter : ${JSON.stringify(updateDto)}`)
    return this.updateUser(updateDto).pipe(
      catchError(error => {
        this.loggerService.error(`### error in update message : `, error.message + ' - ' + error.status)
        throw new HttpException(error.message, error.status)
      })
    )
  }

  findAll() {
    this.loggerService.log(`### init process find all internal user`)
    return from(this.internalUserModel.find({  })).pipe(
      tap(internalUser => this.loggerService.log(`### end process find all internal user: ${JSON.stringify(internalUser.length)}`)),
      concatMap(internalUser => internalUser),
      map(internalUser => {
        return {
          idInternalUser: internalUser._id,
          firstName: internalUser.firstName,
          lastName: internalUser.lastName,
          fullName: internalUser.fullName,
          document: internalUser.document,
          email: internalUser.email,
          password: internalUser.password,
          profile: internalUser.profile,
          roles: internalUser.roles
        }
      }),
      toArray()
    )
  }

  findOneByIdInternalUser(idInternalUser: string) {
    return from(this.internalUserModel.findById(idInternalUser))
  }

  private createUser(createDto: RequestCreateDto): Observable<any> {
    const profile = CONST_PROFILE.find(x => x.ID_PROFILE == createDto.idProfile)
    const profileRoles = CONST_ROLE.find(x => x.ID_PROFILE == profile.ID_PROFILE).ROLES
    const newCreateUser : any = {
      firstName: createDto.firstName,
      lastName: createDto.lastName,
      email: createDto.email,
      password: createDto.password,
      document: {
        code: createDto.documentType,
        value: createDto.documentNumber
      },
      profile: {
        idProfile: new ObjectId(profile.ID_PROFILE),
        code: profile.CODE,
        name: profile.NAME
      },
      roles: profileRoles.map(x => {
        return {
          idRole: new ObjectId(x.ID_ROLE),
          code: x.CODE
        }
      }),
      createdAt: moment.utc().clone(),
      updatedAt: null,
      isOnline: false,
      isInternal: true
    }
    return from(new this.internalUserModel(newCreateUser).save()).pipe(
      tap( internalUser => this.loggerService.log(`### end process create : ${JSON.stringify(internalUser._id)}`))
    )
  }

  private updateUser(updateDto: RequestUpdateDto) {
    const profile = CONST_PROFILE.find(x => x.ID_PROFILE == updateDto.idProfile)
    const profileRoles = CONST_ROLE.find(x => x.ID_PROFILE == profile.ID_PROFILE).ROLES
    const newUpdateUser : any = { 
      firstName: updateDto.firstName,
      lastName: updateDto.lastName,
      profile: {
        idProfile: new ObjectId(profile.ID_PROFILE),
        code: profile.CODE,
        name: profile.NAME
      },
      roles: profileRoles.map(x => {
        return {
          idRole: new ObjectId(x.ID_ROLE),
          code: x.CODE
        }
      }),
      updatedAt: moment.utc().clone(),
      isOnline: false  
    }
    return from(this.internalUserModel.findByIdAndUpdate(updateDto.idInternalUser, newUpdateUser)).pipe(
      tap( () => this.loggerService.log(`### end process update : ${JSON.stringify(updateDto.idInternalUser)}`))
    )
  }

}