import { Module } from '@nestjs/common';
import { AppHttpController } from "./app.http.controller";
import { AppHttpService } from "./app.http.service";
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { InternalUserSchema } from './models/schemas/internal-user.schema';
import { CONST_MODULE } from './app.constants';

@Module({
    imports: [
        MongoDbModule,
        MongoDbModule.forFeature([
            { name: CONST_MODULE.INTERNAL_USER, schema: InternalUserSchema }
        ])
    ],
    controllers: [AppHttpController],
    providers: [AppHttpService]
})
export class AppModule { }