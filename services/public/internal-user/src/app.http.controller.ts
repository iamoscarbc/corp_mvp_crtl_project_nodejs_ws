import { Param } from "@nestjs/common";
import { Controller, HttpCode, HttpStatus, Post, Body, Get, Put} from "@nestjs/common";
import { ApiUseTags, ApiOperation } from "@nestjs/swagger";
import { AppHttpService } from "./app.http.service";
import { RequestCreateDto } from "./models/dto/req-create.dto";
import { RequestFindOneByIdInternalUserDto } from "./models/dto/req-find-one-by-idinternaluser.dto";
import { RequestUpdateDto } from "./models/dto/req-update.dto";

@ApiUseTags("Responsible service for all internal user in project")
@Controller("InternalUser")
export class AppHttpController { 
  
    constructor (
        private readonly appService: AppHttpService
    ) {
        
    }

    @ApiOperation({ title: "Profiles", description: "Find all profiles" })
    @HttpCode(HttpStatus.OK)
    @Get('profiles')
    profiles() { 
        return this.appService.profiles() 
    }

    @ApiOperation({ title: "Create", description: "Create all internal user" })
    @HttpCode(HttpStatus.CREATED)
    @Post('create')
    create(@Body() createDto: RequestCreateDto) { 
        return this.appService.create(createDto) 
    }

    @ApiOperation({ title: "Update", description: "Update all internal user" })
    @HttpCode(HttpStatus.OK)
    @Put('update')
    update(@Body() updateDto: RequestUpdateDto) { 
        return this.appService.update(updateDto)
    }

    @ApiOperation({ title: "Find-All", description: "Find all internal user" })
    @HttpCode(HttpStatus.OK)
    @Get('find-all')
    findAll() { 
        return this.appService.findAll()
    }

    @ApiOperation({ title: "Find-one-by-idInternalUser", description: "Find one internal user for idInternalUser" })
    @HttpCode(HttpStatus.OK)
    @Get('find-one-by-idInternalUser/:idInternalUser')
    findOneByIdInternalUser(@Param() { idInternalUser } : RequestFindOneByIdInternalUserDto) { 
        return this.appService.findOneByIdInternalUser(idInternalUser)
    }

}