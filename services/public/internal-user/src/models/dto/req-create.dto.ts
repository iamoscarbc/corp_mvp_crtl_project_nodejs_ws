import { ApiModelProperty } from '@nestjs/swagger'
import { IsString, IsEmail, IsNotEmpty } from '@corp_mvp_crtl_project_nodejs_ws/setup';
import { IsEnum, IsNumberString } from 'class-validator';
import { CONST_PROFILE_ADMINISTRATOR, CONST_PROFILE_SERVICE_BOARD } from '../../app.constants';

export class RequestCreateDto {

    @IsString()
    @IsNotEmpty()
    @IsEmail()
    @ApiModelProperty()
    email: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    password: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    firstName: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    lastName: string

    @IsString()
    @IsNotEmpty()
    @IsEnum(['DNI'])
    @ApiModelProperty()
    documentType: string

    @IsNumberString()
    @IsNotEmpty()
    @ApiModelProperty()
    documentNumber: string

    @IsString()
    @IsNotEmpty()
    @IsEnum([CONST_PROFILE_ADMINISTRATOR.ID, CONST_PROFILE_SERVICE_BOARD.ID])
    @ApiModelProperty()
    idProfile: string

}