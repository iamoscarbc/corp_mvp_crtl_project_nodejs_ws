import { ApiModelProperty } from '@nestjs/swagger'
import { IsString, IsNotEmpty } from '@corp_mvp_crtl_project_nodejs_ws/setup';
import { IsEnum } from 'class-validator';

export class RequestUpdateDto {

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    idInternalUser: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    firstName: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    lastName: string

    @IsString()
    @IsNotEmpty()
    @IsEnum(['6056a25a93e26f99efa6a3ef', '6056a262c935375b5067383d'])
    @ApiModelProperty()
    idProfile: string

}