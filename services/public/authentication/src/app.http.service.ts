/// <reference path="./models/interfaces/petition.interface.ts" />
/// <reference path="./models/interfaces/internal-user.interface.ts" />
/// <reference path="./models/interfaces/external-user.interface.ts"/>
/// <reference path="./models/interfaces/project.interface.ts"/>

import { Injectable, ConflictException } from "@nestjs/common";
import { RequestLoginDto } from "./models/dto/req-login.dto";
import { from, Observable, forkJoin } from "rxjs";
import { catchError, concatMap, tap } from "rxjs/operators";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { CONST_MODULE } from "./app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { decodeTokenExternalUser } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { HttpException } from "@nestjs/common";

@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.PETITION) private readonly petitionModule: Model<Petition>,
    @InjectModel(CONST_MODULE.EXTERNAL_USER) private readonly externalUserModule: Model<ExternalUser>,
    @InjectModel(CONST_MODULE.INTERNAL_USER) private readonly internalUserModule: Model<InternalUser>,
    @InjectModel(CONST_MODULE.PROJECT) private readonly projectModule: Model<Project>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService
  ) {}

  login(loginDto: RequestLoginDto) {
    this.loggerService.log(`### init process login with parameter : ${JSON.stringify(loginDto)}`);

    const internalUserObservable = from(this.internalUserModule.findOne({ email: loginDto.email }));
    const externalUserObservable = from(this.externalUserModule.findOne({ email: loginDto.email }));

    return forkJoin([internalUserObservable, externalUserObservable]).pipe(
      tap((response) => { this.beforeLoginValidateActive(response); }),
      tap((response) => { this.beforeLoginValidateSubscribe(response); }),
      concatMap((response) => {
        let internalUser = response[0];
        let externalUser = response[1];

        if ( internalUser && internalUser.password == loginDto.password && !externalUser )
          return this.afterLoginForInternalUser(internalUser);

        if (!internalUser && externalUser.password == loginDto.password && externalUser )
          return this.afterLoginForExternalUser(externalUser);

        throw new ConflictException("El email y/o password son incorrectos");
      }),
      catchError((error) => {
        this.loggerService.error(`### error login [ message : `, error.message + " ] [ satatus: " + error.status + " ]");
        throw new HttpException(error.message, error.status);
      })
    );
  }

  logout(token: string) {
    let decodeToken = decodeTokenExternalUser(token)
    this.loggerService.log(`### init process logout with token : ${JSON.stringify(token)}`);
    return from(this.securityService.logout(token)).pipe(
      tap((token) => this.loggerService.log(`### end process generate token external user: ${JSON.stringify(token)}`)),
      /*tap(() => this.afterLoginUpdateOnline(decodeToken.idExternalUser) ), //ExternalUser
      tap(() => this.afterLoginUpdateOnline(decodeToken.idExternalUser) ), //Project
      tap(() => this.afterLoginUpdateOnline(decodeToken.idExternalUser) ), //Enterprise*/
    );
  }

  decodetokenexternal(token: string) {
    return decodeTokenExternalUser(token);
  }

  private afterLoginForInternalUser(internalUser: any): Observable<any> {
    return from(
      this.securityService.generateTokenForInternalUser(
        String(internalUser._id),
        internalUser.profile.code,
        internalUser.roles,
        internalUser.fullName,
        internalUser.firstName,
        internalUser.lastName,
        internalUser.email,
        internalUser.isOnline
      )
    ).pipe(
      tap((token) =>this.loggerService.log(`### end process generate token internal user: ${JSON.stringify(token)}`)),
      tap(() => 
        from(this.internalUserModule.updateOne({ _id: internalUser._id },{ $set: { isOnline: true } })).subscribe(
          () => {}, (error) => this.loggerService.error("### error - update internal user login online",error))
        )
    );
  }

  private afterLoginForExternalUser(externalUser: any): Observable<any> {
    return from(
      this.securityService.generateTokenForExternalUser(
        externalUser._id,
        externalUser.profile,
        externalUser.roles,
        externalUser.firstName,
        externalUser.lastName,
        externalUser.email,
        true,
        externalUser.enterprise.idEnterprise
      )
    ).pipe(
      tap((token) => this.loggerService.log(`### end process generate token external user: ${JSON.stringify(token)}`)),
      /*tap(() => this.afterLoginUpdateOnline(externalUser._id) ), //ExternalUser
      tap(() => this.afterLoginUpdateOnline(externalUser._id) ), //Project
      tap(() => this.afterLoginUpdateOnline(externalUser._id) ), //Enterprise*/
    );
  }

  private beforeLoginValidateActive(response) {
    let internalUser = response[0];
    let externalUser = response[1];

    if(!internalUser && !externalUser)
      throw new ConflictException("EL usuario no existe");

    if (internalUser && !internalUser.active && !externalUser) {
      throw new ConflictException("El usuario no se encuentra activo");
    }

    if (externalUser && !externalUser.active && !internalUser) {
      throw new ConflictException("El usuario no se encuentra activo");
    }
  }

  private beforeLoginValidateSubscribe(response: any) {
    let internalUser = response[0];
    let externalUser = response[1];

    if(!internalUser && !externalUser)
      throw new ConflictException("EL usuario no existe");

    if (internalUser && !internalUser.isSubscribe && !externalUser)
      throw new ConflictException("El usuario fue dado de baja");

    if (externalUser && !externalUser.isSubscribe && !internalUser)
      throw new ConflictException("El usuario fue dado de baja");
  }

  private afterLoginUpdateOnline(idExternalUser: string) {
    from(this.externalUserModule.updateOne({ _id: idExternalUser },{ $set: { isOnline: true } })).subscribe(
      () => {},
      (error) => this.loggerService.error("### error - update external user login online", error)
    )
  }
}
