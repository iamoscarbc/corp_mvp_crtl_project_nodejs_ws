export const CONST_MODULE = {
    PETITION: 'Petition',
    EXTERNAL_USER: 'ExternalUser',
    INTERNAL_USER: 'InternalUser',
    PROJECT: 'Project'

}

export const CONST_CODE = {
    KEY:'0ABC1DEF2GHI3JKL4MNO5PQR6DTU7VWX8YZ9'
}