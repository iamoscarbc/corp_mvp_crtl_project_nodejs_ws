import { Module } from '@nestjs/common';
import { AppHttpController } from "./app.http.controller";
import { AppHttpService } from "./app.http.service";
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { SecurityModule } from '@corp_mvp_crtl_project_nodejs_ws/client-security';
import { PetitionSchema } from './models/schemas/petition.schema';
import { ExternalUserSchema } from './models/schemas/external-user.schema';
import { InternalUserSchema } from './models/schemas/internal-user-schema';
import { ProjectSchema } from './models/schemas/project.schema';
import { CONST_MODULE } from './app.constants';
@Module({
    imports: [
        SecurityModule,
        MongoDbModule,
        MongoDbModule.forFeature([
            { name: CONST_MODULE.PETITION, schema: PetitionSchema },
            { name: CONST_MODULE.EXTERNAL_USER, schema: ExternalUserSchema },
            { name: CONST_MODULE.INTERNAL_USER, schema: InternalUserSchema },
            { name: CONST_MODULE.PROJECT, schema: ProjectSchema }
        ])
    ],
    controllers: [AppHttpController],
    providers: [AppHttpService]
})
export class AppModule { }