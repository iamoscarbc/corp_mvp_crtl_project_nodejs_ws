import { SecurityGuard, Token } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { Controller, HttpCode, HttpStatus, Post, Body, Put, UseGuards, Get} from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";
import { AppHttpService } from "./app.http.service";
import { RequestLoginDto } from "./models/dto/req-login.dto";

@ApiUseTags("Responsible service for all authentication in project")
@Controller("Authentication")
export class AppHttpController { 
  
    constructor (
        private readonly appService: AppHttpService
    ) {
        
    }

    @ApiOperation({ title: "Login", description: "Login all user" })
    @HttpCode(HttpStatus.OK)
    @Post('login')
    login(@Body() loginDto: RequestLoginDto) { 
        return this.appService.login(loginDto) 
    }

    @ApiBearerAuth()
    @UseGuards(SecurityGuard)
    @ApiOperation({ title: "Logout", description: "Logout all user" })
    @HttpCode(HttpStatus.OK)
    @Put('logout')
    logout(@Token() token: string) { 
        return this.appService.logout(token) 
    }

    @ApiBearerAuth()
    @UseGuards(SecurityGuard)
    @ApiOperation({ title: "Decode", description: "Decode all user" })
    @HttpCode(HttpStatus.OK)
    @Get('decode-token-external')
    decodetokenexternal(@Token() token: string) { 
        return this.appService.decodetokenexternal(token) 
    }

}