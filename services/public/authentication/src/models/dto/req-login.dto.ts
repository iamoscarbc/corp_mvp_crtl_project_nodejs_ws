import { ApiModelProperty } from '@nestjs/swagger'
import { IsString, IsEmail, IsNotEmpty } from '@corp_mvp_crtl_project_nodejs_ws/setup';

export class RequestLoginDto {

    @IsString()
    @IsNotEmpty()
    @IsEmail()
    @ApiModelProperty()
    email: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    password: string

}