import { ObjectId } from 'mongodb';
import * as mongoose from 'mongoose';
import * as moment from 'moment';

export const ProjectSchema = new mongoose.Schema(
    {
        name: {
           type: String,
           index: true 
        },
        startDate: Date,
        startString: String,
        endDate: Date,
        endDateString: Date,
        startStoppedDate: Date,
        startStoppedString: String,
        endStoppedDate: Date,
        endStoppedString: String,
        advance: {
            type: Number,
            default: 0,
            index: true
        },
        projectManager: {
            idProjectManager: {
                type: ObjectId,
                index: true
            },
            firstName: String,
            lastName: String,
            isOnline: Boolean
        },
        collaborators: [{
            idCollaborator: ObjectId,
            firstName: String,
            lastName: String,
            email: String,
            isOnline: String
        }],
        createdBy: {
            idUserCreator: ObjectId,
            firstName: String,
            lastName: String,
            profile: {
                idProfile: ObjectId,
                name: String
            }
        },
        createdAt: {
            type: Date,
            default: () => moment.utc().clone(),
        },
        updatedAt: {
            type: Date,
            default: null,
        },
        status: {
            value: {
                type: Number,
                default: 1,
                index: true
            },
            description: {
                type: String,
                default: 'Creado'
            }
        },
        isSubscribe: {
            type: Boolean,
            default: true,
            index: true
        }

    },
    {
        collection: "Projects"
    }
);