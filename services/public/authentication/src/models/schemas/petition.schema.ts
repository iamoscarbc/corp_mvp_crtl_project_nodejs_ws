import { ObjectId } from 'mongodb';
import * as mongoose from 'mongoose';
import * as moment from 'moment';

export const PetitionSchema = new mongoose.Schema(
    {
        idUser: ObjectId,
        email: String,
        code: {
            type: String,
            unique: true
        },
        createdAt: {
            type: Date,
            default: () => moment.utc().clone(),
        },
        updatedAt: {
            type: Date,
            default: null,
        }
    },
    {
        collection: "Petitions"
    }
);