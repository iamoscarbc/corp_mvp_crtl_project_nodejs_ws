import { ObjectId } from 'mongodb';
import { Document } from 'mongoose';

declare global {
    export interface Petition extends Document {
        idUser: ObjectId
        email: string
        code: string
        createdAt: Date
        updatedAt: Date
    }
}