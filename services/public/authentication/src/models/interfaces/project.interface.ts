import { ObjectId } from 'mongodb';
import { Document } from 'mongoose';

declare global {
    export interface Project extends Document {
        name: string
        startDate: Date
        startString: String
        endDate: Date
        endDateString: Date
        startStoppedDate: Date
        startStoppedString: string
        endStoppedDate: Date
        endStoppedString: string
        advance: number
        projectManager: {
            idProjectManager: ObjectId
            firstName: string
            lastName: string
            isOnline: boolean
        },
        collaborators: [{
            idCollaborator: ObjectId
            firstName: string
            lastName: string
            email: string
            isOnline: boolean
        }],
        createdBy: {
            idUserCreator: ObjectId
            firstName: string
            lastName: string
            profile: {
                idProfile: ObjectId
                name: string
            }
        },
        createdAt: Date
        updatedAt: Date
        status: {
            value: number,
            description: string
        }
        isSubscribe: boolean
    }
}