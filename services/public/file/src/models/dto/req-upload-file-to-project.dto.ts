import { ApiModelProperty } from "@nestjs/swagger";
import {} from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { IsNotEmpty, IsString } from "class-validator";

export class RequestUploadFileToProjectDto {
  
    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    idProject: string

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    type: string

}
