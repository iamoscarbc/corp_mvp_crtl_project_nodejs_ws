import { ObjectId } from "mongodb";
import * as mongoose from "mongoose";

export const TypeFileSchema = new mongoose.Schema(
    {
      idTypeFile: {
        type: ObjectId,
        index: true,
      },
      name: String,
      description: String,
      status: {
          code: Number,
          value: String
      },
      createdAt: Date,
      updatedAt: Date
    },
    {
        collection: "TypeFiles",      
    }
)