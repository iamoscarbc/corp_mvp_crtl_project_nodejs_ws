import * as mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

export const ProjectAdvanceSchema = new mongoose.Schema(
    {
        idAdvance: {
            type: ObjectId,
            required: true
        }, 
        name: String,
        startDate: String,
        endDate: String,
        description: String,
        observations: String,
        status: {
            value: Number,
            description: String
        },
        progress: Number,
        documents: [{
            idDocument: ObjectId,
            code: String,
            fileName: String,
            fileKey: String,
            url: String,
            createdAt: Date
        }],
        userStories: [{
            idUserStory: ObjectId,
            name: String,
            observation: String,
            createdAt: Date,
            updatedAt: Date,
            percentageOfSprint: Number,
            isCompleted: Boolean,
            status: {
                value: Number,
                description: String
            }
        }],
        indicators: {
            completed: Boolean,
            notCompleted: Boolean,
            isMedium: Boolean,
            isLow: Boolean,
            isHigh: Boolean
        },
        createdAt: Date,
        updatedAt: Date
    },
    {
        _id: false
    }
);