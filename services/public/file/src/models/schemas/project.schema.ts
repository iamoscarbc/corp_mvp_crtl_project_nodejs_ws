import { ObjectId } from "mongodb";
import * as mongoose from "mongoose";
import * as moment from "moment";
import { ENUM_STATUS_DESCRIPTION, ENUM_STATUS_VALUE } from "../../app.enums";
import { ProjectAdvanceSchema } from "./project-advance.schema";

export const ProjectSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      index: true,
    },
    startDate: Date,
    startString: String,
    endDate: Date,
    endDateString: String,
    startStoppedDate: Date,
    startStoppedString: String,
    endStoppedDate: Date,
    endStoppedString: String,
    learnedLessons: String,
    budget: Number,
    cost: Number,
    costReal: Number,
    progress: Number,
    sprint: {
      quantityMonth: Number,
      quantityDays: Number,
      percentageProgress: Number,
      advance: [ProjectAdvanceSchema]
    },
    enterprise: {
      idEnterprise: ObjectId,
      businessName: String,
      ruc: String,
      isSuscribe: Boolean,
    },
    documents: [{
      idDocument: ObjectId,
      code: String,
      fileName: String,
      fileKey: String,
      url: String,
      extension: String,
      createdAt: Date
    }],
    eventBeforeInitializing: [],
    eventBeforeMaterialResources: [],
    detailsCostRealsPerMonth: [{
      idDetailCostRealPerMonth: ObjectId,
      year: String,
      month: String,
      startDate: Date,
      startString: String,
      endDate: Date,
      endString: String,
      createdAt: Date,
      updatedAt: Date,
      costs: [{
        idCost: ObjectId,
        charge: String,
        cost: Number,
        createdAt: Date,
        updatedAt: Date
      }]
    }],
    projectManager: {
      idProjectManager: {
        type: ObjectId,
        index: true,
      },
      firstName: String,
      lastName: String,
      createdAt: Date,
      cost: Number,
      isOnline: Boolean,
    },
    materialResources: [
      {
        idMaterialResource: {
          type: ObjectId,
          index: true,
        },
        code: String,
        name: String,
        description: String,
        quantity: Number,
        singleCost: Number,
        monthCost: Number,
        yearCost: Number,
        createdAt: Date,
        isActive: Boolean
      },
    ],
    collaborators: [
      {
        idCollaborator: ObjectId,
        firstName: String,
        lastName: String,
        email: String,
        cost: Number,
        isOnline: String,
        createdAt: Date,
        isActive: Boolean
      },
    ],
    createdBy: {
      idUserCreator: ObjectId,
      firstName: String,
      lastName: String,
      profile: {
        idProfile: ObjectId,
        name: String,
      },
    },
    createdAt: {
      type: Date,
      default: () => moment.utc().clone(),
    },
    updatedAt: {
      type: Date,
      default: null,
    },
    status: {
      value: {
        type: Number,
        default: ENUM_STATUS_VALUE.PROJECT_CREATE,
        index: true,
      },
      description: {
        type: String,
        default: ENUM_STATUS_DESCRIPTION.PROJECT_CREATE,
      },
    },
    isSubscribe: {
      type: Boolean,
      default: true,
      index: true,
    },
  },
  {
    collection: "Projects",
  }
);
