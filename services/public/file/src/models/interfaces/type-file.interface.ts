import { ObjectId } from "mongodb";
import { Document } from "mongoose";

declare global {
  export interface TypeFile extends Document {
    idTypeFile: ObjectId;
    name: string;
    description: string;
    status: { 
        code: number;
        value: string; 
    }
    createdAt: Date;
    updatedAt: Date;
 }
}