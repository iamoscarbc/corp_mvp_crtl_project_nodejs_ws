import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Transport } from '@nestjs/microservices';
import { HTTP, PACKAGE_JSON, SWAGGER, TCP } from '@corp_mvp_crtl_project_nodejs_ws/config'
import { LoggerService, LoggerModule } from '@corp_mvp_crtl_project_nodejs_ws/logger'

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const swaggerBasePath = '/swagger';

  const httpBasePath = '/api'

  const loggerSevice = app.select(LoggerModule).get(LoggerService)

  //Configuracion para Swagger
  const documentBuilder = new DocumentBuilder()
    .setTitle('File')
    .setDescription(PACKAGE_JSON)
    .setBasePath(httpBasePath)
    .setVersion('1.0')
    .setSchemes('http', 'https')
    .addBearerAuth()
    .setHost(SWAGGER.HOST);

  const document = SwaggerModule.createDocument(app, documentBuilder.build());
  SwaggerModule.setup(swaggerBasePath, app, document);

  app.enableCors();

  app.setGlobalPrefix(httpBasePath);

  await app.startAllMicroservicesAsync();
  await app.listenAsync(HTTP.PORT, HTTP.HOST);

  loggerSevice.log(`listening at http://${HTTP.HOST}:${HTTP.PORT}`)
  loggerSevice.log(`swagger documentation at http://${HTTP.HOST}:${HTTP.PORT}/swagger `)
  loggerSevice.log(`listening at tcp://${TCP.HOST}:${TCP.PORT}`)

}
bootstrap();