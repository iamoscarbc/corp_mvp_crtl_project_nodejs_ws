import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { ConflictException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CONST_MODULE, CONST_ORDERING, CONST_PROFILE_ADMINISTRATOR } from "./app.constants";
import * as AWS from 'aws-sdk';
import { RequestUploadFileToProjectDto } from "./models/dto/req-upload-file-to-project.dto";
import { RequestUploadFileToSprintDto } from "./models/dto/req-upload-file-to-sprint.dto";
import { ObjectId } from "mongodb";
import moment = require("moment");
import { from } from "rxjs";
import { concatMap, map, tap, toArray } from "rxjs/operators";
import { decodeTokenExternalUser } from "@corp_mvp_crtl_project_nodejs_ws/setup";
import { GenerateEfficiencyLevelForExternal, GenerateEfficiencyLevelForInternal } from "./excel/generate-efficiency-level";
import { GenerateCostPerformanceForExternal, GenerateCostPerformanceForInternal } from "./excel/generate-cost-performance";
import { GenerateStatusEventForExternal, GenerateStatusEventForInternal } from "./excel/generate-status-event";
import { Response } from "express";
import { S3 } from '@corp_mvp_crtl_project_nodejs_ws/config'

const BUCKET = 'freelance-project-temp'
const s3 = new AWS.S3({
    accessKeyId: S3.SECRETKEY,
    secretAccessKey: S3.SECURITYKEY
});
@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.PROJECT) private readonly projectModel: Model<Project>,
    @InjectModel(CONST_MODULE.EXTERNAL_USER) private readonly externalUserModel: Model<ExternalUser>,
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseMode: Model<Enterprise>,
    @InjectModel(CONST_MODULE.TYPE_FILE) private readonly typeFileModel: Model<TypeFile>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService
  ) {} 

  public async uploadToProject(token: string, file: any, uploadToProjectDto: RequestUploadFileToProjectDto) {
    this.loggerService.log(` ### init process upload file to project `, file.originalname, uploadToProjectDto);
    let splitFileOriginalName = file.originalname.split('.')
    let extension = splitFileOriginalName[splitFileOriginalName.length - 1];
    let validationExtension = this.validateExtensionFile(extension);

    if(!validationExtension)
      throw new ConflictException(`No se puede subir archivos de tipo ${extension}`)

    let fileKey = this.generateFileKey(file.originalname);
    const urlKey = `crtl-project/project/${uploadToProjectDto.idProject}/${fileKey}`;
    const params = {
        Body: file.buffer,
        Bucket: BUCKET,
        Key: urlKey
    }
    await this.uploadFileInS3(params);
    
    const createDocument = {
      idDocument: new ObjectId(),
      code: uploadToProjectDto.type,
      fileName: file.originalname,
      fileKey: urlKey,
      url: `https://${BUCKET}.s3.amazonaws.com/${urlKey}`,
      extension: extension,
      createdAt: moment().utc().toDate()
    }

    let uploadFileInProject = await this.projectModel.updateOne({ _id: new ObjectId(uploadToProjectDto.idProject)}, { $push: { documents: createDocument } })
    this.loggerService.log(" ### upload file in project :", uploadFileInProject)
    return createDocument;
  }

  public async uploadToSprint(token: string, file: any, uploadToSprintDto: RequestUploadFileToSprintDto) {
    this.loggerService.log(` ### init process upload file to sprint `, file.originalname)
    let splitFileOriginalName = file.originalname.split('.')
    let extension = splitFileOriginalName[splitFileOriginalName.length - 1];
    let validationExtension = this.validateExtensionFile(extension);

    if(!validationExtension)
      throw new ConflictException(`No se puede subir archivos de tipo ${extension}`)

    let fileKey = this.generateFileKey(file.originalname);
    const urlKey = `crtl-project/project/${uploadToSprintDto.idProject}/sprint/${uploadToSprintDto.idAdvance}/${fileKey}`;
    const params = {
        Body: file.buffer,
        Bucket: BUCKET,
        Key: urlKey
    }
    await this.uploadFileInS3(params)

    const createDocument = {
      idDocument: new ObjectId(),
      code: uploadToSprintDto.type,
      fileName: file.originalname,
      fileKey: urlKey,
      url: `https://${BUCKET}.s3.amazonaws.com/${urlKey}`,
      extension: extension,
      createdAt: moment().utc().toDate()
    }

    let operation = [{
      updateOne: {
        filter: { _id: new ObjectId(uploadToSprintDto.idProject), "sprint.advance.idAdvance": new ObjectId(uploadToSprintDto.idAdvance) },
        update: {   
          $push: {
            "sprint.advance.$[item].documents": createDocument,
           }
        },
        arrayFilters: [ { 'item.idAdvance': new ObjectId(uploadToSprintDto.idAdvance) } ]
      }
    }]
    
    let uploadFileInTheSprint = await this.projectModel.bulkWrite(operation);
    this.loggerService.log(" ### upload file in the sprint :", uploadFileInTheSprint)
    return createDocument;
  }

  deleteToProject(token: string, idProject: string, idDocument: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.projectModel.findOne({ "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe un proyecto para este usuario", tokenExternalUser.email)

        if(String(project._id) !== String(idProject))
          throw new ConflictException("No puedes quitar documentos a un projecto que no eres el jefe del proyecto")
      }),
      concatMap(project => {
        let operation = [{
          updateOne: {
            filter: { _id: project._id, "documents.idDocument": new ObjectId(idDocument) },
            update: {   
              $pull: { documents: { idDocument: new ObjectId(idDocument) } },
            }
          }
        }]
        return from(this.projectModel.bulkWrite(operation))
      })
    )
  }

  deleteToSprint(token: string, idProject: string, idAdvance: string, idDocument: string) {
    let tokenExternalUser = decodeTokenExternalUser(token)
    return from(this.projectModel.findOne({ "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) })).pipe(
      tap(project => {
        if(!project)
          throw new ConflictException("No existe el proyecto para este usuario", tokenExternalUser.email)

        if(String(project._id) !== String(idProject))
          throw new ConflictException("No puedes quitar documentos a un projecto que no eres el jefe del proyecto")
      }),
      concatMap(project => {
        let operation = [{
          updateOne: {
            filter: { _id: project._id, "sprint.advance.idAdvance":  new ObjectId(idAdvance), "sprint.advance.documents.idDocument" : new ObjectId(idDocument) },
            update: {   
              $pull: { "sprint.advance.$.documents" : { idDocument:  new ObjectId(idDocument) } }
            }
          }
        }]
        return from(this.projectModel.bulkWrite(operation))
      })
    )
  }

  async downloadAllFile(fileKey: string, token: string, res: Response): Promise<any> {

		try {
      let downloadRequest;
      
      let params = {
        Bucket: BUCKET,
        Key: fileKey
      }

			downloadRequest = await s3.getObject(params);

			downloadRequest.on('httpHeaders', async (statusCode: number, headers, response) => {
				if (statusCode !== HttpStatus.OK) {
					console.log('Ocurri\u00f3 un error obteniendo el archivo desde AWS S3');
					res.status(statusCode).json({ message: 'Ocurri\u00f3 un error obteniendo el archivo' });
				}
				this.loggerService.log(`Enviando stream AWS de descarga con fileKey=${fileKey}`);
				res.set('Content-Length', headers['content-length']);
				res.set('Content-Type', headers['content-type']);
				const responseStream = response.httpResponse.createUnbufferedStream();
				responseStream.pipe(res);
			});

			downloadRequest.on('httpError', function (error, response) {
				this.loggerService.error('Error al descargar archivo\n', error);
			});

			downloadRequest.on('httpDone', function (response) {
				this.logger.log(`Descarga del archivo finalizada (fileKey = ${fileKey})`);
			});

			downloadRequest.send();

		} catch (error) {
			return error
		}
  }

  findAllFileInTheProject(token: string, idProject: string) {
    this.loggerService.log(`### init process find all file in the project`);
    let decodeToken = decodeTokenExternalUser(token);

    return from(this.projectModel.findById(idProject)).pipe(
      map(project => {
        return {
          idProject: project._id,
          CODE01: project.documents.filter(x => x.code === 'CODE01'),
          CODE02: project.documents.filter(x => x.code === 'CODE02'),
          CODE03: project.documents.filter(x => x.code === 'CODE03')
        };
      })
    );
  }

  findAllFileInTheSprint(token: string, idProject: string, idAdvance: string) {
    this.loggerService.log(`### init process find all file in the sprint`);
    let decodeToken = decodeTokenExternalUser(token);
    return from(this.projectModel.findById(idProject)).pipe(
      map((project) => {
        let advanceDocuments = project.sprint.advance.find(x => String(x.idAdvance) == String(idAdvance)).documents
        return {
          idProject: project._id,
          documents: advanceDocuments
        };
      }),
      toArray()
    );
  }

  findAllTypeFile() {
    return from(this.typeFileModel.find({ "status.code": 1 }));
  }

  async generateEfficiencyLevel(token: string) {
    this.loggerService.log(`### init process find all efficiency level in project`);
    let tokenExternalUser = decodeTokenExternalUser(token);
    let query = !tokenExternalUser.isInternal
    ? (tokenExternalUser.profile.idProfile == CONST_PROFILE_ADMINISTRATOR.ID) ? {  "enterprise.idEnterprise": new ObjectId(tokenExternalUser.idEnterprise) } : { "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) }
      : {};
    let projects = await this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })

    if(!projects.length) {
      return new ConflictException('No puedes generar reporte ya que no existe ningun proyecto asociado a la empresa.')
    }

    let enterprise = await this.enterpriseMode.find().sort({ createdAt: CONST_ORDERING.DESCENDING })

    let pushProjectForInternal : any[] = []
    enterprise.forEach(x => {
      let projectInEnterprise  = projects.filter(y => String(y.enterprise.idEnterprise) == String(x._id))

      if(projectInEnterprise) {
        pushProjectForInternal.push({
          enterprise: x.businessName,
          projects : projectInEnterprise.map(project => {
            return {
              name: project.name,
              sprint: project.sprint
            }
          })
        })
      }
    })

    let buffer = !tokenExternalUser.isInternal ? await GenerateEfficiencyLevelForExternal(projects) : await GenerateEfficiencyLevelForInternal(pushProjectForInternal)
    const urlKey = `crtl-project/project/`;
    const params = {
        Body: buffer,
        Bucket: BUCKET,
        Key: urlKey,
        ContentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    }
    const url = await this.uploadFileInS3(params)
    return {
      url
    }
  }

  async generateCostPerformance(token: string) {
    this.loggerService.log(`### init process generate cost performance`);
    let tokenExternalUser = decodeTokenExternalUser(token);
    let query = !tokenExternalUser.isInternal
    ? (tokenExternalUser.profile.idProfile == CONST_PROFILE_ADMINISTRATOR.ID) ? {  "enterprise.idEnterprise": new ObjectId(tokenExternalUser.idEnterprise) } : { "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) }
      : {};
    let projects = await this.projectModel.find(query)

    if(!projects.length) {
      return new ConflictException('No puedes generar reporte ya que no existe ningun proyecto asociado a la empresa.')
    }

    let enterprise = await this.enterpriseMode.find()

    let pushProjectForInternal : any[] = []
    enterprise.forEach(x => {
      let projectInEnterprise  = projects.filter(y => String(y.enterprise.idEnterprise) == String(x._id))

      if(projectInEnterprise) {
        pushProjectForInternal.push({
          enterprise: x.businessName,
          projects : projectInEnterprise.map(project => {
            return {
              budget: project.budget,
              costReal: project.costReal,
              name: project.name,
              sprint: project.sprint
            }
          })
        })
      }
    })
    let buffer = !tokenExternalUser.isInternal ? await GenerateCostPerformanceForExternal(projects) : await GenerateCostPerformanceForInternal(pushProjectForInternal)
    const urlKey = `crtl-project/project/`;
    const params = {
        Body: buffer,
        Bucket: BUCKET,
        Key: urlKey,
        ContentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    }
    const url = await this.uploadFileInS3(params)
    return {
      url
    }
  }

  async generateStatusEvent(token: string) {
    this.loggerService.log(`### init process generate status event`);
    let tokenExternalUser = decodeTokenExternalUser(token);
    let query = !tokenExternalUser.isInternal
    ? (tokenExternalUser.profile.idProfile == CONST_PROFILE_ADMINISTRATOR.ID) ? {  "enterprise.idEnterprise": new ObjectId(tokenExternalUser.idEnterprise) } : { "projectManager.idProjectManager": new ObjectId(tokenExternalUser.idExternalUser) }
      : {};
    let projects = await this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })
    let enterprise = await this.enterpriseMode.find().sort({ createdAt: CONST_ORDERING.DESCENDING })

    if(!projects.length) {
      return new ConflictException('No puedes generar reporte ya que no existe ningun proyecto asociado a la empresa.')
    }

    let pushProjectForInternal : any[] = []
    enterprise.forEach(x => {
      let projectInEnterprise  = projects.filter(y => String(y.enterprise.idEnterprise) == String(x._id))

      if(projectInEnterprise) {
        pushProjectForInternal.push(projectInEnterprise)
      }
    })
    let buffer = !tokenExternalUser.isInternal ? await GenerateStatusEventForExternal(projects) : await GenerateStatusEventForInternal(pushProjectForInternal)
    const urlKey = `crtl-project/project/`;
    const params = {
        Body: buffer,
        Bucket: BUCKET,
        Key: urlKey,
        ContentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    }
    const url = await this.uploadFileInS3(params)
    return {
      url
    }
  }

  private async uploadFileInS3(params) {
    try {
      await s3.putObject(params).promise().then(
        data => {
            this.loggerService.log(" ##### upload file data : ", data.ETag);
            return params.Key
        },
        err => {
            this.loggerService.error(" #### Error file data : ", err);
            return err;
        }
      );

      return s3.getSignedUrl('getObject', { Bucket: params.Bucket, Key: params.Key });

    } catch (error) {
      this.loggerService.error(" #### Error upload file in s3 : ", error);
    }
  }

  private validateExtensionFile (extension: string) {
    let extensions = ['xlsx', 'xls', 'ppt', 'pptx', 'doc', 'docx', 'txt']
    return (extensions.includes(extension))
  }

  private generateFileKey(fileName: string) {
    return `${new ObjectId()}-${fileName}`
  }

}