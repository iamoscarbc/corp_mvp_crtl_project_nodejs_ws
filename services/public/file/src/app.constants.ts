export const CONST_MODULE = {
    PROJECT: 'Project',
    EXTERNAL_USER: 'ExternalUser',
    ENTERPRISE: 'Enterprise',
    TYPE_FILE: 'TypeFile'
}

export const CONST_EVENT_BEFORE_INITIALIZING = {
    READY_TO_ASSIGNED_DURATION_SPRINT: 'READY_TO_ASSIGNED_DURATION_SPRINT',
    READY_TO_ASSIGNED_COLLABORATOR: 'READY_TO_ASSIGNED_COLLABORATOR',
    READY_TO_ASSIGNED_MATERIAL_RESOURCE: 'READY_TO_ASSIGNED_MATERIAL_RESOURCE',
    READY_TO_INITIALIZING_OF_THE_PROJECT: 'READY_TO_INITIALIZING_OF_THE_PROJECT'
}

export const CONST_DAYS_IN_MONTH = {
    VALUE : 30
}

export const CONST_PROFILE_ADMINISTRATOR = {
    ID: "6056a25a93e26f99efa6a3ef",
    NAME: "Administrador",
    CODE: "ADM",
    IS_VIEW_ADM: true,
  };
  
  export const CONST_PROFILE_HUMAN_RESOURCES = {
    ID: "605bb2132e0c8173b7d2031c",
    NAME: "Recursos Humanos",
    CODE: "RHH",
    IS_VIEW_ADM: true,
  };
  
  export const CONST_PROFILE_PROJECT_MANAGER = {
    ID: "605bb43655b54ee157c1d4d9",
    NAME: "Jefe de proyecto",
    CODE: "PJM",
    IS_VIEW_ADM: false,
  };
  
  export const CONST_PROFILE_COLLABORATORS = {
    ID: "605bb465a40ceafedc0f4445",
    NAME: "Colaboradores",
    CODE: "DEV",
    IS_VIEW_ADM: false,
  };
  
  export const CONST_ORDERING = {
    ASCENDING: 1,
    DESCENDING: -1
  }