import {
  ENUM_Roles,
  Roles,
  RolesGuard,
  SecurityGuard,
  Token,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Param,
  Delete,
  Get,
  Response,
  Query
} from "@nestjs/common";
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiUseTags, ApiBearerAuth, ApiOperation, ApiConsumes, ApiImplicitFile } from "@nestjs/swagger";
import { RequestUploadFileToProjectDto } from "./models/dto/req-upload-file-to-project.dto";
import { AppHttpService } from "./app.http.service";
import { RequestUploadFileToSprintDto } from "./models/dto/req-upload-file-to-sprint.dto";
import { RequestDownloadAllFileDto } from "./models/dto/req-download-all-file.dto";

@ApiBearerAuth()
@UseGuards(SecurityGuard, RolesGuard)
@ApiUseTags("Responsible service for all upload files in project or sprint")
@Controller("File")
export class AppHttpController {
  constructor(private readonly appHttpService: AppHttpService) {}

  @ApiOperation({ title: "Upload file - project", description: "Upload file in the projet" })
  @HttpCode(HttpStatus.OK)
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true })
  @UseInterceptors(FileInterceptor('file'))
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Post("upload-to-project")
  async uploadToProject(
      @UploadedFile() file: any,
      @Body() uploadToProjectDto : RequestUploadFileToProjectDto,
      @Token() token: string
  ) {
      return await this.appHttpService.uploadToProject(token, file, uploadToProjectDto);
  }

  @ApiOperation({ title: "Report - sprint", description: "Report sprint in the projet" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Delete("delete-to-project/:idProject/:idDocument")
  deleteToProject(
      @Param('idProject') idProject: string,
      @Param('idDocument') idDocument: string,
      @Token() token: string
  ) {
      return this.appHttpService.deleteToProject(token, idProject, idDocument);
  }

  @ApiOperation({ title: "Upload file - sprint", description: "Upload file in the sprint" })
  @HttpCode(HttpStatus.OK)
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true })
  @UseInterceptors(FileInterceptor('file'))
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Post("upload-to-sprint")
  async uploadToSprint(
      @UploadedFile() file: any,
      @Body() uploadToSprintDto: RequestUploadFileToSprintDto,
      @Token() token: string
  ) {
      return await this.appHttpService.uploadToSprint(token, file, uploadToSprintDto);
  }

  @ApiOperation({ title: "Report - sprint", description: "Report sprint in the projet" })
  @HttpCode(HttpStatus.OK)
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Delete("delete-to-sprint/:idProject/:idAdvance/:idDocument")
  deleteToSprint(
      @Param('idProject') idProject: string,
      @Param('idAdvance') idAdvance: string,
      @Param('idDocument') idDocument: string,
      @Token() token: string
  ) {
      return this.appHttpService.deleteToSprint(token, idProject, idAdvance, idDocument);
  }

  @ApiOperation({ title: "Download file" })
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Post("/download-all-file")
  async downloadAllFile( 
    @Body() uploadToSprintDto: RequestDownloadAllFileDto,
    @Token() token: string, 
    @Response() res
  ): Promise<any> {
    return await this.appHttpService.downloadAllFile(uploadToSprintDto.fileKey, token, res);
  }
  
  @ApiOperation({ title: "List all file in the project", description: "List all file in the project" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("all-file-in-the-project/:idProject")
  findAllFileInTheProject(
    @Param('idProject') idProject: string,
    @Token() token: string) {
    return this.appHttpService.findAllFileInTheProject(token, idProject);
  }

  @ApiOperation({ title: "Montly cost performance project", description: "Montly cost performance in all projet" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("all-file-in-the-sprint/:idProject/:idAdvance")
  findAllFileInTheSprint(
    @Param('idProject') idProject: string,
    @Param('idAdvance') idAdvance: string,
    @Token() token: string
  ) {
    return this.appHttpService.findAllFileInTheSprint(token, idProject, idAdvance);
  }

  @ApiOperation({ title: "Montly cost performance project", description: "Montly cost performance in all projet" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.REPORT_SPRINT_PROJECT)
  @Get("all-type-file")
  findAllTypeFile(@Token() token: string
  ) {
    return this.appHttpService.findAllTypeFile();
  }

  @ApiOperation({ title: "Generate Efficiency level", description: "Generate Efficiency level" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("generate-efficiency-level")
  async generateEfficiencyLevel(@Token() token: string
  ) {
    return await this.appHttpService.generateEfficiencyLevel(token);
  }

  @ApiOperation({ title: "Generate Cost Performance", description: "Generate Cost Performance" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("generate-cost-performance")
  async generateCostPerformance(@Token() token: string
  ) {
    return await this.appHttpService.generateCostPerformance(token);
  }

  @ApiOperation({ title: "Generate Status Event", description: "Generate Status Event" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.TOLIST_PROJECT)
  @Get("generate-status-event")
  async generateStatusEvent(@Token() token: string
  ) {
    return await this.appHttpService.generateStatusEvent(token);
  }

}
