import { HttpException } from '@nestjs/common';
import { worker } from 'cluster';
import ExcelJS = require('exceljs');
import moment = require('moment');

export const GenerateStatusEventForInternal = async (pushProjectForInternal: any[]) => {
    const workBook = new ExcelJS.Workbook();
    console.log(pushProjectForInternal.length)
    for (let index = 0; index < pushProjectForInternal.length; index++) {
        const element = pushProjectForInternal[index];
        console.log("element",  element)
        const workSheet = workBook.addWorksheet(element[0].enterprise.businessName);
        await loadHeader(workSheet);
        await loadBody(element, workSheet);
    }
    return await workBook.xlsx.writeBuffer();
}

export const GenerateStatusEventForExternal = async (projects: any[]) => {
    const workBook = new ExcelJS.Workbook();
    const workSheet = workBook.addWorksheet(projects[0].enterprise.businessName);
    await loadHeader(workSheet);
    await loadBody(projects, workSheet);
    return await workBook.xlsx.writeBuffer();
}

const loadHeader = async (workSheet: any) => {

    await loadHeaderWidth(workSheet);
    
    workSheet.getCell(`A1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('A1').value = 'Item';

    workSheet.getCell(`B1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('B1').value = 'Proyecto';

    workSheet.getCell(`C1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('C1').value = 'Creado por';

    workSheet.getCell(`D1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('D1').value = 'Fecha creación';

    workSheet.getCell(`E1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('E1').value = 'Jefe proyecto';

    workSheet.getCell(`F1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('F1').value = 'Fecha inicial';

    workSheet.getCell(`G1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('G1').value = 'Fecha final';

    workSheet.getCell(`H1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('H1').value = 'Estado';

    workSheet.getCell(`I1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('I1').value = 'Costo Planificado';

    workSheet.getCell(`J1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('J1').value = 'Costo Real';

    workSheet.getCell(`K1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('K1').value = 'Presupuesto';

    workSheet.getCell(`L1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('L1').value = 'Duracion del sprint';

    workSheet.getCell(`M1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('M1').value = 'Cantidad de Sprint';

    workSheet.getCell(`N1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('N1').value = 'Cantidad de Colaboradores';

    workSheet.getCell(`O1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('O1').value = 'Cantidad de Materiales';

    workSheet.getCell(`P1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('P1').value = 'Porcentaje de avance';

    workSheet.getCell(`Q1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('Q1').value = 'Lecciones aprendidas';
}

const loadBody = async (projects: any[], workSheet: any) => {
    let initDateDetail = 1
    for (let indexProject = 0; indexProject < projects.length; indexProject++) {
        const project = projects[indexProject];
        initDateDetail = initDateDetail + 1
        const row = workSheet.getRow(initDateDetail);
        row.values = [
            indexProject + 1,
            project.name,
            project.createdBy.firstName + ' ' + project.createdBy.lastName,
            moment(project.createdAt).format('DD/MM/YYYY'),
            project.projectManager.firstName + ' ' + project.projectManager.lastName,
            moment(project.startDate).format('DD/MM/YYYY'),
            moment(project.endDate).format('DD/MM/YYYY'),
            project.status.description,
            project.cost.toFixed(2),
            project.costReal.toFixed(2),
            project.budget.toFixed(2),
            project.sprint.quantityDays,
            project.sprint.advance.length,
            project.collaborators.length,
            project.materialResources.length,
            project.progress.toFixed(2),
            project.learnedLessons
        ]
        workSheet.addRow(row)
    }
}

const loadHeaderWidth = async (workSheet: any) => {
    workSheet.columns = [ 
        { key: 'A', width: 25 }, 
        { key: 'B', width: 25 }, 
        { key: 'C', width: 25 }, 
        { key: 'D', width: 25 }, 
        { key: 'E', width: 25 }, 
        { key: 'F', width: 25 }, 
        { key: 'G', width: 25 }, 
        { key: 'H', width: 25 }, 
        { key: 'I', width: 25 }, 
        { key: 'J', width: 25 },
        { key: 'K', width: 25 },
        { key: 'L', width: 25 },
        { key: 'M', width: 25 },
        { key: 'N', width: 25 },
        { key: 'O', width: 25 },
        { key: 'P', width: 25 }
    ];
}