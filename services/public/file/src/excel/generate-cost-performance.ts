import { worker } from 'cluster';
import ExcelJS = require('exceljs');
import moment = require('moment');

export const GenerateCostPerformanceForInternal = async (pushProjectForInternal: any[]) => {
    const workBook = new ExcelJS.Workbook();
    for (let index = 0; index < pushProjectForInternal.length; index++) {
        const element = pushProjectForInternal[index];
        const workSheet = workBook.addWorksheet(element.enterprise);
        await loadHeader(workSheet);
        await loadBody(element.projects, workSheet);
    }
    return await workBook.xlsx.writeBuffer();
}

export const GenerateCostPerformanceForExternal = async (projects: any[]) => {
    const workBook = new ExcelJS.Workbook();
    const workSheet = workBook.addWorksheet(projects[0].enterprise.businessName);
    await loadHeader(workSheet);
    await loadBody(projects, workSheet);
    return await workBook.xlsx.writeBuffer();
}

const loadHeader = async (workSheet: any) => {

    await loadHeaderWidth(workSheet);

    workSheet.mergeCells(`B1:D1`);
    workSheet.getCell(`B1:D1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('B1:D1').value = 'Variable';
    workSheet.getCell('B2:D2').value = 'Control de Proyectos';

    workSheet.mergeCells(`E1:H1`);
    workSheet.getCell(`E1:H1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('E1:H1').value = 'Indicador';
    workSheet.getCell('E2:H2').value = 'Porcentaje de Indice de Desempeño de Costo';
    
    workSheet.getCell(`I1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('I1').value = 'Medida';
    workSheet.getCell('I2').value = 'Proyectos';

    workSheet.getCell(`J1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('J1').value = 'Fórmula';
    workSheet.getCell('J2').value = 'CPI = EV/AC';

    workSheet.mergeCells(`A4:A5`);
    workSheet.getCell(`A4:A5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('A4:A5').value = 'Proyecto';
    
    workSheet.mergeCells(`B4:B5`);
    workSheet.getCell(`B4:B5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('B4:B5').value = 'Item';
    
    workSheet.mergeCells(`C4:C5`);
    workSheet.getCell(`C4:C5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('C4:C5').value = 'Tareas';
    
    workSheet.mergeCells(`D4:D5`);
    workSheet.getCell(`D4:D5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('D4:D5').value = 'Fecha Inicio';
    
    workSheet.mergeCells(`E4:E5`);
    workSheet.getCell(`E4:E5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('E4:E5').value = 'Fecha Fin';
    
    workSheet.mergeCells(`F4:H4`);
    workSheet.getCell(`F4:H4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('F4:H4').value = 'Valor Ganado (EV)  (EV = PV* %Progreso)';
    
    workSheet.getCell(`F5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('F5').value = 'PV (Valor Planificado)';
   
    workSheet.getCell(`G5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('G5').value = 'Progreso (%)';
   
    workSheet.getCell(`H5`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('H5').value = 'EV (Valor Ganado)';
    
    workSheet.getCell('I4:I5').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('I4:I5').value = 'Costo Actual (AC)';
    workSheet.getCell('J4:J5').fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('J4:J5').value = 'Porcentaje de Índice de Desempeño de Costo';
}

const loadBody = async (projects: any[], workSheet: any) => {
    let initDateDetail = 5
    for (let indexProject = 0; indexProject < projects.length; indexProject++) {
        const project = projects[indexProject];
        let pv = project.budget / project.sprint.advance.length
        let projectAdvance = project.sprint.advance
        let totalUserStory = 0
        projectAdvance.forEach(element => {
            let userStoryCompleted = element.userStories.filter(x => x.isCompleted)
            totalUserStory = totalUserStory + userStoryCompleted.length
        });
        let costRealUserStory = project.costReal / totalUserStory

        for (let indexAdvance = 0; indexAdvance < project.sprint.advance.length; indexAdvance++) {
            const advance = project.sprint.advance[indexAdvance];
            let actualyCost = costRealUserStory * advance.userStories.filter(x => x.isCompleted).length;
            let ev = (pv * advance.progress) * 0.01
            let indiceCost = (actualyCost == 0) ? actualyCost : ev/actualyCost
            initDateDetail = initDateDetail + 1
            const row = workSheet.getRow(initDateDetail);
            row.values = [
                project.name,
                indexAdvance + 1,
                advance.name,
                advance.startDate,
                advance.endDate,
                pv.toFixed(2),
                advance.progress.toFixed(2),
                ev.toFixed(2),
                actualyCost.toFixed(2),
                indiceCost.toFixed(2)
            ]
            workSheet.addRow(row)
        }
    }
}

const loadHeaderWidth = async (workSheet: any) => {
    workSheet.columns = [ 
        { key: 'A', width: 25 }, 
        { key: 'B', width: 25 }, 
        { key: 'C', width: 25 }, 
        { key: 'D', width: 25 }, 
        { key: 'E', width: 25 }, 
        { key: 'F', width: 25 }, 
        { key: 'G', width: 25 }, 
        { key: 'H', width: 25 }, 
        { key: 'I', width: 25 }, 
        { key: 'J', width: 25 } 
    ];
}