import ExcelJS = require('exceljs')
import moment = require('moment')

export const GenerateEfficiencyLevelForInternal = async (pushProjectForInternal: any[]) => {
    const workBook = new ExcelJS.Workbook();
    for (let index = 0; index < pushProjectForInternal.length; index++) {
        const element = pushProjectForInternal[index];
        const workSheet = workBook.addWorksheet(element.enterprise);
        await loadHeader(workSheet);
        await loadBody(element.projects, workSheet);
    }
    return await workBook.xlsx.writeBuffer();
}

export const GenerateEfficiencyLevelForExternal = async (projects: any[]) => {
    const workBook = new ExcelJS.Workbook();
    const workSheet = workBook.addWorksheet(projects[0].enterprise.businessName);
    await loadHeader(workSheet);
    await loadBody(projects, workSheet);
    return await workBook.xlsx.writeBuffer();
}

const loadBody = async (data : any[], workSheet: any) => {

    let initDateDetail = 4;

    for (let indexProject = 0; indexProject < data.length; indexProject++) {
        const project = data[indexProject];
        
        if(project.sprint.advance.length > 0) {
            for (let indexAdvance = 0; indexAdvance < project.sprint.advance.length; indexAdvance++) {
                const advance = project.sprint.advance[indexAdvance];
                const userStoriesTotal = advance.userStories.length
                const userStoriesIsCompleted = advance.userStories.filter(x => x.isCompleted).length
                const efficiencyLevel = userStoriesTotal == 0 ? userStoriesTotal : userStoriesIsCompleted/userStoriesTotal
                initDateDetail = initDateDetail + 1
                const row = workSheet.getRow(initDateDetail);
                row.values = [
                    project.name,
                    'Jefe de Proyecto',
                    indexAdvance + 1,
                    moment(advance.startDate).format('DD/MM/YYYY'),
                    moment(advance.endDate).format('DD/MM/YYYY'),
                    userStoriesIsCompleted,
                    userStoriesTotal,
                    efficiencyLevel
                ]
                workSheet.addRow(row)
            }
        } else {
            initDateDetail = initDateDetail + 1
            const row = workSheet.getRow(initDateDetail);
            row.values = [
                project.name,
                'Jefe de Proyecto'
            ]
            workSheet.addRow(row)
        }
    }
}

const loadHeader = async (workSheet: any) => {

    await loadHeaderWidth(workSheet);
    
    workSheet.mergeCells(`C1:E1`);
    workSheet.getCell(`C1:E1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('C1:E1').value = 'Variable';
    workSheet.mergeCells(`C2:E2`);
    workSheet.getCell('C2:E2').value = 'Control de Proyectos';

    workSheet.getCell(`F1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('F1').value = 'Indicador';
    workSheet.getCell('F2').value = 'Nivel de eficacia';

    workSheet.getCell(`G1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('G1').value = 'Medida';
    workSheet.getCell('G2').value = 'Proyectos';

    workSheet.getCell(`H1`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell('H1').value = 'Fórmula';
    workSheet.getCell('H2').value = 'RA/RE';

    workSheet.getCell('A4').value = 'Proyecto';
    workSheet.getCell('B4').value = 'Cargo de Personal';
    workSheet.getCell('C4').value = 'Item';
    workSheet.getCell('D4').value = 'Fecha Inicio';
    workSheet.getCell('E4').value = 'Fecha Final';
    workSheet.getCell('F4').value = 'Actividades Alcanzadas (RA)';
    workSheet.getCell('G4').value = 'Actividades Esperadas (RE)';
    workSheet.getCell('H4').value = 'Nivel de eficacia';

    workSheet.getCell(`A4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`B4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`C4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`D4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`E4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`F4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`G4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
    workSheet.getCell(`H4`).fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: '9BC2E6' } };
}

const loadHeaderWidth = async (workSheet: any) => {
    workSheet.columns = [ 
        { key: 'A', width: 25 }, 
        { key: 'B', width: 25 }, 
        { key: 'C', width: 25 }, 
        { key: 'D', width: 25 }, 
        { key: 'E', width: 25 }, 
        { key: 'F', width: 25 }, 
        { key: 'G', width: 25 }, 
        { key: 'H', width: 25 }, 
        { key: 'I', width: 25 }, 
        { key: 'J', width: 25 } 
    ];
}