import { Module } from '@nestjs/common';
import { MongoDbModule } from '@corp_mvp_crtl_project_nodejs_ws/database';
import { CONST_MODULE } from './app.constants';
import { SecurityModule } from '@corp_mvp_crtl_project_nodejs_ws/client-security';
import { ProjectSchema } from './models/schemas/project.schema';
import { ExternalUserSchema } from './models/schemas/external-user.schema';
import { EnterpriseSchema } from './models/schemas/enterprise.schema';
import { LoggerModule } from '@corp_mvp_crtl_project_nodejs_ws/logger';
import { AppHttpController } from './app.http.controller';
import { AppHttpService } from './app.http.service';
import { TypeFileSchema } from './models/schemas/type-file.schema';

@Module({
    imports: [
        SecurityModule,
        MongoDbModule,
        LoggerModule,
        MongoDbModule.forFeature([
            { name: CONST_MODULE.PROJECT, schema: ProjectSchema },
            { name: CONST_MODULE.EXTERNAL_USER, schema: ExternalUserSchema },
            { name: CONST_MODULE.ENTERPRISE, schema: EnterpriseSchema },
            { name: CONST_MODULE.TYPE_FILE, schema: TypeFileSchema }
        ])
    ],
    exports: [AppHttpService],
    controllers: [AppHttpController],
    providers: [AppHttpService]
})
export class AppModule { }