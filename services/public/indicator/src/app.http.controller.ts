import {
  ENUM_Roles,
  Roles,
  RolesGuard,
  SecurityGuard,
  Token,
} from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Body,
  UseGuards,
  Get,
  Param,
  Put,
} from "@nestjs/common";
import { ApiUseTags, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";
import { AppHttpService } from "./app.http.service";

@ApiBearerAuth()
@UseGuards(SecurityGuard)
@ApiUseTags("Responsible service for all indicator in project")
@Controller("Indicator")
export class AppHttpController {
  constructor(private readonly appService: AppHttpService) {}

  @ApiOperation({ title: "Montly cost performance project", description: "Montly cost performance in all projet" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.INDICATOR_MONTLY_COST_PERFORMANCE_PROJECT)
  @Get("montly-cost-performance-project")
  montlyCostPerformanceProject(@Token() token: string) {
    return this.appService.montlyCostPerformanceProject(token);
  }

  @ApiOperation({ title: "Total cost performance project", description: "Total cost performance in all project" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.INDICATOR_TOTAL_COST_PERFORMANCE_PROJET)
  @Get("total-cost-performance-project")
  totalCostPerformanceProject(@Token() token: string) {
    return this.appService.totalCostPerformanceProject(token);
  }

  @ApiOperation({ title: "Percentage progress all project", description: "Percentage progress all projet" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.INDICATOR_PERCENTAGE_PROGRESS_ALL_PROJECT)
  @Get("percentage-progress-all-project")
  percentageProgressAllProject(@Token() token: string) {
    return this.appService.percentageProgressAllProject(token);
  }

  @ApiOperation({ title: "Percentage progress sprint all project", description: "Percentage progress sprint all project" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.INDICATOR_PERCENTAGE_PROGRESS_SPRINT_ALL_PROJECT)
  @Get("percentage-progress-sprint-all-project")
  percentageProgressSprintAllProject(@Token() token: string) {
    return this.appService.percentageProgressSprintAllProject(token);
  }

  @ApiOperation({ title: "History event project", description: "History event in all project" })
  @HttpCode(HttpStatus.CREATED)
  @Roles(ENUM_Roles.INDICATOR_PERCENTAGE_PROGRESS_SPRINT_ALL_PROJECT) // Se debe crear un nuevo rol
  @Get("history-event")
  historyEvent(@Token() token: string) {
    return this.appService.historyEvent(token);
  }

}
