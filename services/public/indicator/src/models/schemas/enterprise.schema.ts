import { ObjectId } from "mongodb";
import * as mongoose from "mongoose";
import * as moment from "moment";

export const EnterpriseSchema = new mongoose.Schema(
  {
    idEnterprise: {
      type: ObjectId,
      index: true,
    },
    ruc: {
      type: String,
      unique: true,
      index: true,
    },
    businessName: {
      type: String,
      unique: true,
      index: true,
    },

    estado_del_contribuyente: String,
    condicion_de_domicilio: String,
    ubigeo: String,
    tipo_de_via: String,
    nombre_de_via: String,
    codigo_de_zona: String,
    tipo_de_zona: String,
    numero: String,
    interior: String,
    lote: String,
    dpto: String,
    manzana: String,
    kilometro: String,
    departamento: String,
    provincia: String,
    distrito: String,
    direccion: String,
    direccion_completa: String,
    ultima_actualizacion: String,
    materialResources: [
      {
        idMaterialResource: {
          type: ObjectId,
          index: true,
        },
        code: String,
        name: String,
        description: String,
        quantity: Number,
        singleCost: Number,
        monthCost: Number,
        yearCost: Number,
        createdAt: Date,
      },
    ],
    humanResources: [
      {
        idHumanResource: {
          type: ObjectId,
          index: true,
        },
        firstName: {
          type: String,
          index: true,
        },
        lastName: {
          type: String,
          index: true,
        },
        email: {
          type: String,
          index: true,
        },
        createdAt: Date,
      },
    ],
    managers: [
      {
        idManager: {
          type: ObjectId,
          index: true,
        },
        firstName: {
          type: String,
          index: true,
        },
        lastName: {
          type: String,
          index: true,
        },
        email: {
          type: String,
          index: true,
        },
        createdAt: Date,
      },
    ],
    projects: [
      {
        idProject: {
          type: ObjectId,
          index: true,
        },
        name: String,
        boss: [
          {
            idBoss: {
              type: ObjectId,
              index: true,
            },
            fullName: {
              type: String,
              index: true,
            },
            email: {
              type: ObjectId,
              index: true,
            },
          },
        ],
        createdAt: Date,
      },
    ],
    collaborators: [
      {
        idCollaborator: ObjectId,
        firstName: String,
        lastName: String,
        createdAt: Date,
        isOnline: Boolean,
      },
    ],
    createdBy: {
      idUserCreator: {
        type: ObjectId,
        index: true,
      },
      firstName: String,
      lastName: String,
      profileCode: String,
    },
    createdAt: {
      type: Date,
      default: () => moment.utc().clone(),
    },
    updatedAt: {
      type: Date,
      default: null,
    },
    status: {
      value: {
        type: Number,
        default: 1,
        index: true,
      },
      description: {
        type: String,
        default: 'Creado',
      },
    },
    isSubscribe: {
      type: Boolean,
      default: true,
      index: true,
    },
  },
  {
    collection: "Enterprises",
  }
);
