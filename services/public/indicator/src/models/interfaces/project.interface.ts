import { ObjectId } from "mongodb";
import { Document } from "mongoose";


declare global {
  export interface ProjectAdvance extends Document {
    idAdvance: ObjectId,
    name: string,
    startDate: string,
    endDate: string,
    description: string,
    observations: string,
    status: {
      value: number,
      description: string
    },
    progress: number,
    userStories: [{
      idUserStory: ObjectId,
      name: string,
      observation: string,
      createdAt: Date,
      updatedAt: Date,
      percentageOfSprint: number;
      isCompleted: boolean;
      status: {
          value: number,
          description: number
      }
    }],
    indicators: {
      completed: boolean;
      notCompleted: boolean;
      isMedium: boolean;
      isLow: boolean;
      isHigh: boolean;
    },

    createdAt: Date;
    updatedAt: Date;
  }
}
declare global {
  export interface Project extends Document {
    name: string;
    startDate: Date;
    startString: string;
    endDate: Date;
    endDateString: string;
    startStoppedDate: Date;
    startStoppedString: string;
    endStoppedDate: Date;
    endStoppedString: string;
    learnedLessons: string;
    budget: number;
    cost: number;
    costReal: number;
    progress: number;
    sprint: {
      quantityMonth: number;
      quantityDays: number;
      percentageProgress: number;
      advance: Array<ProjectAdvance>
    },
    enterprise: {
      idEnterprise: ObjectId;
      businessName: string;
      ruc: string;
      isSuscribe: boolean;
    };
    event: [{
      idEvent: ObjectId;
      description: string;
      createdAt: Date
    }]
    eventBeforeInitializing: string[];
    eventBeforeMaterialResources: string[];
    detailsCostRealsPerMonth: [{
      idDetailCostRealPerMonth: ObjectId;
      year: String,
      month: string;
      startDate: Date;
      startString: string;
      endDate: Date;
      endString: string;
      createdAt: Date;
      updatedAt: Date;
      costs: [{
        idCost: ObjectId;
        charge: string;
        cost: number;
        createdAt: Date;
        updatedAt: Date;
      }]
    }]
    projectManager: {
      idProjectManager: ObjectId;
      firstName: string;
      lastName: string;
      createdAt: Date;
      cost: number;
      isOnline: boolean;
    };
    materialResources: [
      {
        idMaterialResource: ObjectId;
        code: string;
        name: string;
        description: string;
        quantity: number;
        singleCost: number;
        monthCost: number;
        yearCost: number;
        createdAt: Date;
        updatedAt: Date;
        isActive: boolean;
      }
    ];
    collaborators: [
      {
        idCollaborator: ObjectId;
        firstName: string;
        lastName: string;
        email: string;
        createdAt: Date;
        updatedAt: Date;
        cost: number;
        isOnline: boolean;
        isActive: boolean;
      }
    ];
    createdBy: {
      idUserCreator: ObjectId;
      firstName: string;
      lastName: string;
      profile: {
        idProfile: ObjectId;
        name: string;
      };
    };
    createdAt: Date;
    updatedAt: Date;
    status: {
      value: number;
      description: string;
    };
    isSubscribe: boolean;
  }
}
