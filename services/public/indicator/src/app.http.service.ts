/// <reference path="./models/interfaces/project.interface.ts" />
/// <reference path="./models/interfaces/external-user.interface.ts" />
/// <reference path="./models/interfaces/enterprise.interface.ts" />

import { ConflictException, HttpException, Injectable } from "@nestjs/common";
import { from, throwError, Observable, of, forkJoin, concat } from "rxjs";
import {
  concatMap,
  catchError,
  tap,
  map,
  switchMap,
  toArray,
  pluck,
  filter,
} from "rxjs/operators";
import { LoggerService } from "@corp_mvp_crtl_project_nodejs_ws/logger";
import { CONST_MODULE, CONST_ORDERING, CONST_PROFILE_ADMINISTRATOR } from "./app.constants";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { SecurityService } from "@corp_mvp_crtl_project_nodejs_ws/client-security";
import { ObjectId } from "mongodb";
import moment = require("moment");
import { decodeTokenExternalUser } from "@corp_mvp_crtl_project_nodejs_ws/setup";

@Injectable()
export class AppHttpService {
  constructor(
    @InjectModel(CONST_MODULE.PROJECT) private readonly projectModel: Model<Project>,
    @InjectModel(CONST_MODULE.EXTERNAL_USER) private readonly externalUserModel: Model<ExternalUser>,
    @InjectModel(CONST_MODULE.ENTERPRISE) private readonly enterpriseMode: Model<Enterprise>,
    private readonly loggerService: LoggerService,
    private readonly securityService: SecurityService
  ) {}

  montlyCostPerformanceProject(token: string) {
    let query = this.getQuery(token)
    return from(this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })).pipe(
      switchMap(projects => projects),
      map(project  => {
        let budgetPerMonth = project.budget / project.sprint.quantityMonth
        return {
          idProject: project._id,
          name: project.name,
          detailsCostRealsPerMonth: project.detailsCostRealsPerMonth.map(x => {
            return {
              month: x.month,
              year: x.year,
              totalCost: x.costs.map(y => y.cost).reduce((a: any, b: any) => a + b, 0),
              budget: budgetPerMonth,
              costs: x.costs.map(y => {
                return {
                  cost: y.cost,
                  charge: y.charge,
                }
              })
            }
          })
        }
      }),
      toArray()
    )
  }

  totalCostPerformanceProject(token: string) {
    let query = this.getQuery(token)
    return from(this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })).pipe(
      switchMap(projects => projects),
      map(project  => {
        return {
          idProject: project._id,
          name: project.name,
          cost: project.cost,
          costReal: project.costReal,
          budget: project.budget
        }
      }),
      toArray()
    )
  }

  percentageProgressAllProject(token: string) {
    let query = this.getQuery(token)
    return from(this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })).pipe(
      switchMap(projects => projects),
      map(project  => {
        return {
          idProject: project._id,
          name: project.name,
          progress: project.progress
        }
      }),
      toArray()
    )
  }
   
  percentageProgressSprintAllProject(token: string) {
    let query = this.getQuery(token)
    return from(this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })).pipe(
      switchMap(projects => projects),
      map(project  => {
        return {
          idProject: project._id,
          name: project.name,
          sprint: project.sprint.advance.map(x => {
            return {
              name: x.name,
              progress: x.progress
            }
          })
        }
      }),
      toArray()
    )
  }

  historyEvent(token: string) {
    let decodeToken = decodeTokenExternalUser(token);

    let query = !decodeToken.isInternal
    ? (decodeToken.profile.idProfile == CONST_PROFILE_ADMINISTRATOR.ID) ? 
    { "enterprise.idEnterprise": new ObjectId(decodeToken.idEnterprise) } : 
    { "projectManager.idProjectManager": new ObjectId(decodeToken.idExternalUser) }
    : { };

    return from(this.projectModel.find(query).sort({ startDate: CONST_ORDERING.ASCENDING })).pipe(
      switchMap(projects => projects),
      map(project  => {
        return {
          idProject: project._id,
          name: project.name,
          event: project.event.map(x => {
            return {
              created: moment(x.createdAt).format('DD-MM-YYYY'),
              event: x.description
            }
          })
        }
      }),
      toArray()
    )
  }

  
  protected getQuery(token: string) {
    let decodeToken = decodeTokenExternalUser(token);

    let query = !decodeToken.isInternal
      ? (decodeToken.profile.idProfile == CONST_PROFILE_ADMINISTRATOR.ID) ? 
      { "enterprise.idEnterprise": new ObjectId(decodeToken.idEnterprise), "status.value": { $ne: 1 } } : 
      { "projectManager.idProjectManager": new ObjectId(decodeToken.idExternalUser), "status.value": { $ne: 1 } }
      : { "status.value": { $ne: 1 } };

    return query
  }
}
