#!/bin/bash

echo 'Init npm i production for authentication-service'
cd ./authentication-service/ && npm i --production
cd ..

echo 'Init npm i production for enterprise-service'
cd ./enterprise-service/ && npm i --production
cd ..

echo 'Init npm i production for internal-user-service'
cd ./internal-user-service/ && npm i --production
cd ..

echo 'Init npm i production for external-user-service'
cd ./external-user-service/ && npm i --production
cd ..

echo 'Init npm i production for project-service'
cd ./project-service/ && npm i --production
cd ..

echo 'Init npm i production for security-service'
cd ./security-service/ && npm i --production
cd ..

echo 'Init npm i production for indicator-service'
cd ./indicator-service/ && npm i --production
cd ..

echo 'Init npm i production for file-service'
cd ./file-service/ && npm i --production
cd ..

pm2 start pm2.development.yml