import { Injectable } from "@nestjs/common";
import { Client, ClientTCP, Transport } from "@nestjs/microservices";
import { CLIENTS } from "@corp_mvp_crtl_project_nodejs_ws/config";
import { Observable } from "rxjs";

@Injectable()
export class ProjectService {
  @Client({
    transport: Transport.TCP,
    options: { host: CLIENTS.PROJECT.HOST, port: CLIENTS.PROJECT.PORT },
  })
  private readonly client: ClientTCP;

  afterLoginForExternalUser() {

  }

}