import { Injectable } from "@nestjs/common";
import { Client, ClientTCP, Transport } from "@nestjs/microservices";
import { CLIENTS } from "@corp_mvp_crtl_project_nodejs_ws/config";
import { Observable } from "rxjs";

@Injectable()
export class SecurityService {
  @Client({
    transport: Transport.TCP,
    options: { host: CLIENTS.SECURITY.HOST, port: CLIENTS.SECURITY.PORT },
  })
  private readonly client: ClientTCP;

  validateExistToken(token: string): Observable<boolean> {
    return this.client.send<boolean>(
      { action: "validate-exist-token", type: "validate" },
      { token }
    );
  }

  generateTokenForInternalUser(
    idInternalUser: string,
    profileCode: string,
    roles: any,
    fullName: string,
    firstName: string,
    lastName: string,
    email: string,
    isOnline: true
  ): Observable<any> {
    return this.client.send<Observable<any>>(
      { action: "generate-token-for-internal-user", type: "insert-update" },
      {
        idInternalUser,
        profileCode,
        roles,
        fullName,
        firstName,
        lastName,
        email,
        isOnline,
      }
    );
  }

  generateTokenForExternalUser(
    idExternalUser: string,
    profile: any,
    roles: any,
    firstName: string,
    lastName: string,
    email: string,
    isOnline: true,
    idEnterprise: string
  ): Observable<any> {
    return this.client.send<Observable<any>>(
      { action: "generate-token-for-external-user", type: "insert-update" },
      {
        idExternalUser,
        profile,
        roles,
        firstName,
        lastName,
        email,
        isOnline,
        idEnterprise,
      }
    );
  }

  logout(token: string): Observable<void> {
    return this.client.send<void>(
      { action: "logout", type: "update" },
      { token }
    );
  }
}
