import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { decode } from 'jsonwebtoken'
import { ROLES_KEY } from '../decorators/use-roles-guard.decorator';
import { ENUM_Roles } from '../enums/enum.role';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
        const requiredRoles = this.reflector.getAllAndOverride<ENUM_Roles[]>(ROLES_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        const req  = context.switchToHttp().getRequest()
        let parts = req.headers.authorization.split(' ')
        let decodeToken = Buffer.from(decode(parts[1]).data, 'base64').toString('utf8')
        let parseToken = this.parseJsonInternalUser(decodeToken)
        requiredRoles.forEach(x => {
            if(!parseToken.roles.includes(x))
                throw new UnauthorizedException(`El rol ${x} no esta configurado para el email ${parseToken.email}`)
        })
        return true
    }

    private parseJsonInternalUser(token) {
        var objectParse = JSON.parse(token)
        return <any>{
            idInternalUser: objectParse['idInternalUser'],
            profileCode: objectParse['profileCode'],
            roles: objectParse['roles'],
            fullName: objectParse['fullName'],
            firstName: objectParse['firstName'],
            lastName: objectParse['lastName'],
            email: objectParse['email'],
            isOnline: objectParse['isOnline']
        }
    }
}