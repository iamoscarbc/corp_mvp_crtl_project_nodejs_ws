import { Injectable, CanActivate, ExecutionContext, UnauthorizedException, Logger } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { SecurityService } from '../security.service'
import { Observable, of } from 'rxjs';
import { decode } from 'jsonwebtoken'

@Injectable()
export class SecurityGuard implements CanActivate {
    constructor (
        private readonly securityService: SecurityService
    ) {}

    async canActivate(context: ExecutionContext) : Promise<boolean> {
        const req  = context.switchToHttp().getRequest()
        if(req.headers && req.headers.authorization){
            let parts = req.headers.authorization.split(' ')
            if (parts.length === 2) {
                let scheme = parts[0], token = parts[1]
                if (!token || !((scheme || '').toLowerCase() === 'bearer'))
                    throw new UnauthorizedException()

                let validateToken = await this.securityService.validateExistToken(token).toPromise()
                if(!validateToken) {
                    req.token = Buffer.from(decode(token).data, 'base64').toString('utf8');
                    return true
                }
                throw new UnauthorizedException()
            }
        }
        throw new UnauthorizedException()
    }
}