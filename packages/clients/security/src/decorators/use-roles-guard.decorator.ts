import { SetMetadata } from '@nestjs/common';
import { ENUM_Roles } from '../enums/enum.role';

export const ROLES_KEY = 'roles';
export const Roles = (...roles: ENUM_Roles[]) => SetMetadata(ROLES_KEY, roles);