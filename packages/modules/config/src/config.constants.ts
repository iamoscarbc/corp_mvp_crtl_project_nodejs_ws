import { ConfigService } from './config.service'
import { join } from 'path'

export const TCP = {
  HOST: ConfigService.get<string>('tcp.host', 'localhost'),
  PORT: ConfigService.get<number>('tcp.port', 30000)
}

export const HTTP = {
  HOST: ConfigService.get<string>('http.host', 'localhost'),
  PORT: ConfigService.get<number>('http.port', 3000)
}

export const PACKAGE_JSON = require(join(process.cwd(), 'package.json'))
export const MONGO = { URI: ConfigService.get<string>('mongouri', '') }
export const SWAGGER = { HOST: ConfigService.get<string>('swagger.host', 'localhost') }

export const CLIENTS = {
  SECURITY: {
    HOST: ConfigService.get<string>('client.security.host', 'localhost'),
    PORT: ConfigService.get<number>('client.security.port', 40001)
  },
  PROJECT: {
    HOST: ConfigService.get<string>('client.project.host', 'localhost'),
    PORT: ConfigService.get<number>('client.project.port', 40001)
  },
  ENTERPRISE: {
    HOST: ConfigService.get<string>('client.enterprise.host', 'localhost'),
    PORT: ConfigService.get<number>('client.enterprise.port', 40001)
  },
}

export const S3 = {
  SECURITYKEY : ConfigService.get<string>('aws.securitykey', 'securitykey'),
  SECRETKEY: ConfigService.get<string>('aws.secretkey', 'secretkey')
}