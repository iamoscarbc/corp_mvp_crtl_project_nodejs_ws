import { Module } from '@nestjs/common'
import { LoggerModule } from '@corp_mvp_crtl_project_nodejs_ws/logger'
import { setupProviders } from './setup.providers'

@Module({
    imports: [LoggerModule],
    providers: [...setupProviders],
    exports: [...setupProviders]
})

export class SetupModule {}