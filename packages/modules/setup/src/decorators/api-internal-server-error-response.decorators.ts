import * as swagger from '@nestjs/swagger'

export const ApiInternalServerErrorResponse = () => swagger.ApiInternalServerErrorResponse({ description: 'ocurrio un error en la petición' })