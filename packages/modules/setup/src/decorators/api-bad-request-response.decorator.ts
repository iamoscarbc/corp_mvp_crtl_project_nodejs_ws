import * as swagger from '@nestjs/swagger'

export const ApiBadRequestResponse = () => swagger.ApiBadRequestResponse({ description: 'parámetros inválidos' })