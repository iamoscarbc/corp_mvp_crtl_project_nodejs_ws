/// <reference path="./models/interfaces/token-internal-user-paylod.ts" />
/// <reference path="./models/interfaces/token-external-user-paylod.ts" />

export const decodeTokenInternalUser = (token: string) => {
  var objectParse = JSON.parse(token);
  return <JwtTokenInternalUserPayloadData>{
    idInternalUser: objectParse["idInternalUser"],
    profileCode: objectParse["profileCode"],
    roles: objectParse["roles"],
    fullName: objectParse["fullName"],
    firstName: objectParse["firstName"],
    lastName: objectParse["lastName"],
    email: objectParse["email"],
    isOnline: objectParse["isOnline"],
    isInternal: objectParse["isInternal"],
  };
};

export const decodeTokenExternalUser = (token: string) => {
  var objectParse = JSON.parse(token);
  return <JwtTokenExternalUserPayloadData>{
    idExternalUser: objectParse["idExternalUser"],
    profile: objectParse["profile"],
    roles: objectParse["roles"],
    fullName: objectParse["fullName"],
    firstName: objectParse["firstName"],
    lastName: objectParse["lastName"],
    email: objectParse["email"],
    isOnline: objectParse["isOnline"],
    isInternal: objectParse["isInternal"],
    idEnterprise: objectParse["idEnterprise"],
  };
};
