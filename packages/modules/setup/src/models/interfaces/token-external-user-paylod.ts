declare interface JwtTokenExternalUserPayloadData {
  idExternalUser: string;
  profile: any;
  roles: string[];
  fullName: string;
  firstName: string;
  lastName: string;
  email: string;
  isOnline: boolean;
  isInternal: false;
  idEnterprise: string;
}
