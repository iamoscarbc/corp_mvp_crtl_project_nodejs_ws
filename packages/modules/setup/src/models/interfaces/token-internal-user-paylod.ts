declare interface JwtTokenInternalUserPayloadData {
    idInternalUser: string
    profileCode: string
    roles: string[]
    fullName: string
    firstName: string
    lastName: string
    email: string
    isOnline: boolean
    isInternal: boolean
}