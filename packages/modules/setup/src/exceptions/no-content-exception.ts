import { HttpException, HttpStatus } from '@nestjs/common'

export class NoContentException extends HttpException {
    constructor(schema) {
		super({ statusCode: HttpStatus.NO_CONTENT, error: `No Content ${schema}` }, HttpStatus.NO_CONTENT)
	}
}