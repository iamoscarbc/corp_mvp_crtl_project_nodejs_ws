import { NestFactory } from '@nestjs/core'
import { FastifyAdapter } from '@nestjs/platform-fastify'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { DynamicModule, INestApplication, ValidationPipe } from '@nestjs/common'
import { NestApplicationOptions } from '@nestjs/common/interfaces/nest-application-options.interface'
import { LoggerService, LoggerModule } from '@corp_mvp_crtl_project_nodejs_ws/logger'
import { loggerServiceInstance } from '@corp_mvp_crtl_project_nodejs_ws/logger/dist/logger.providers'
import { HTTP, PACKAGE_JSON, SWAGGER, TCP } from '@corp_mvp_crtl_project_nodejs_ws/config'
import { SetupModule } from './setup.module'
import { ErrorFilter } from './filters/error.filter'

export const create = async (appModule: any, builder?: (app: INestApplication, documentBuilder: DocumentBuilder) => void | Promise<void>) => {
    const app = await NestFactory.create(
        <DynamicModule>{
            module: SetupModule,
            imports: [appModule]
        },
        new FastifyAdapter()
        ,
        <NestApplicationOptions>{
            logger: loggerServiceInstance
        }
    )
    app.enableCors()
    app.setGlobalPrefix('api')
    app.useGlobalFilters(
        app.get(ErrorFilter)
    )
    app.useGlobalPipes(new ValidationPipe({ transform: true }))

    const loggerSevice = app.select(LoggerModule).get(LoggerService)

    const documentBuilder = new DocumentBuilder()
        .setTitle(PACKAGE_JSON.name)
        .setDescription(PACKAGE_JSON.description)
        .setBasePath('/api')
        .setSchemes('http', 'https')
        .addBearerAuth()
        .setHost(SWAGGER.HOST)

    if (builder)
        await builder(app, documentBuilder)

    SwaggerModule.setup('/swagger', app, SwaggerModule.createDocument(app, documentBuilder.build()))

    await app.startAllMicroservicesAsync()
    await app.listenAsync(HTTP.PORT, HTTP.HOST)
    loggerSevice.log(`listening at http://${HTTP.HOST}:${HTTP.PORT}`)
    loggerSevice.log(`swagger documentation at http://${HTTP.HOST}:${HTTP.PORT}/swagger/ `)
    loggerSevice.log(`listening at tcp://${TCP.HOST}:${TCP.PORT}`)
}