import { Module, DynamicModule } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { MONGO } from '@corp_mvp_crtl_project_nodejs_ws/config'

const mongooseModule = MongooseModule.forRoot(MONGO.URI, { useCreateIndex: true, useNewUrlParser: true })

@Module({
    imports: [mongooseModule],
    exports: [mongooseModule]
})
export class MongoDbModule {
    
    static forFeature(models?: {
        name: string;
        schema: any;
        collection?: string;
    }[], connectionName?: string): DynamicModule { return MongooseModule.forFeature(models, connectionName) }
}
